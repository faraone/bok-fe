import {useEffect, useMemo} from 'react'
import {
    FETCH_ALL_BANK_TRANSACTION_ID,
    FETCH_BANK_ACCOUNT_INFO_ID,
    fetchAllBankTransactionAction,
    fetchBankAccountInfoAction,
    isFetchAllBankTransactionPending,
    isFetchBankAccountInfoPending
} from './redux/bank-account.fetch'
import {
    FETCH_ALL_USER_CARDS_HOME_ID,
    fetchAllUserCardsAction,
    isFetchAllUserCardsHomePending
} from '../card/redux/card.fetch'
import {useDispatch, useSelector} from 'react-redux'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../core/components/utils'

/**
 * Call specific bank account container (account info and transactions)
 * */
export const useBankAccountHook = () => {

    const dispatch = useDispatch()

    const isBankAccountInfoPending = useSelector(isFetchBankAccountInfoPending)
    const isBankTransactionPending = useSelector(isFetchAllBankTransactionPending)
    const isAllCardPending = useSelector(isFetchAllUserCardsHomePending)

    useEffect(() => {
        const token = Cookies.get(USER_TOKEN)

        // Bank account info
        dispatch(fetchBankAccountInfoAction.build(null, FETCH_BANK_ACCOUNT_INFO_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
        // Fetch profile info
        dispatch(fetchAllBankTransactionAction.build(null, FETCH_ALL_BANK_TRANSACTION_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
        // Fetch account card list
        dispatch(fetchAllUserCardsAction.build(null, FETCH_ALL_USER_CARDS_HOME_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
    }, [dispatch])

    const isBankAccountRequestsPending = useMemo(() =>
        isBankAccountInfoPending ||
        isBankTransactionPending ||
        isAllCardPending, [isAllCardPending, isBankAccountInfoPending, isBankTransactionPending])

    return {isBankAccountRequestsPending}
}
