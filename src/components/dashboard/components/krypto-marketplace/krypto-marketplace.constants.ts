import {ITableStructure} from '../../../shared/visualization-table/VisualizationTable'

// Krypto marketplace list table structure
export const KRYPTO_TABLE_STRUCTURE: ITableStructure[] = [
    {key: 'name', label: 'Name'},
    {key: 'price', label: 'Price ($)'},
    {key: 'symbol', label: 'Symbol'},
    {key: 'updateTimestamp', label: 'Update'}
]


