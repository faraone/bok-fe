import * as React from 'react'
import {useCallback} from 'react'
import {Button, DialogActions, DialogContent, DialogTitle} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'

interface ISendMoneyDialog {
}

const SendMoneyDialog: React.FC<ISendMoneyDialog> = () => {

    const dispatch = useDispatch()

    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    return (
        <>
            <DialogTitle>
                <Typography> Send money </Typography>
            </DialogTitle>

            <DialogContent>
                <Typography> DialogContent </Typography>
            </DialogContent>

            <DialogActions>
                <div className="d-flex justify-content-between w-100 p-3">
                    <Button variant="outlined" color="default" onClick={closeDialog}> Close </Button>
                    <Button variant="contained" color="primary" type='submit'> Send </Button>
                </div>
            </DialogActions>
        </>
    )
}

export default SendMoneyDialog
