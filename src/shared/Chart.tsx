import React from 'react'
import {useTheme} from '@material-ui/core/styles'
import {Label, Line, LineChart, ResponsiveContainer, Tooltip, XAxis, YAxis} from 'recharts'
import {Typography} from '@material-ui/core'
import {createChartData} from '../core/components/utils'

interface IChartProps {
    title: string | React.ReactElement
    leftLabelText?: string
    firstDescriptionString?: string
    secondDescriptionString?: string
    data: any[]
    noTitleTypography?: boolean
}


const mockData = [
    createChartData('00:00', undefined),
    createChartData('24:00', undefined)
]

/**
 * Shared component to render Chart with data from props
 * */
const Chart: React.FC<IChartProps> = (props) => {

    const {
        title,
        data = mockData,
        firstDescriptionString,
        secondDescriptionString,
        noTitleTypography = false,
        leftLabelText = 'Prices ($)'
    } = props

    const theme = useTheme()

    return (
        <React.Fragment>
            {noTitleTypography ? title : <Typography component="h2" variant="h6" color="primary" gutterBottom>
                {title}
            </Typography>}

            {
                firstDescriptionString && secondDescriptionString && <div className="row">
                    <div className="col-12 flex-column mb-2">
                        <Typography variant="caption" color="textSecondary"> {firstDescriptionString} </Typography>
                        <br/>
                        <Typography variant="caption" color="textSecondary"> {secondDescriptionString} </Typography>
                    </div>
                </div>
            }

            <ResponsiveContainer height="75%">
                <LineChart
                    data={data.length > 0 ? data : mockData}
                    margin={{
                        top: 16,
                        right: 16,
                        bottom: 0,
                        left: 24
                    }}
                >
                    <XAxis dataKey="time"
                           stroke={theme.palette.text.secondary}/>

                    <YAxis stroke={theme.palette.text.secondary}
                           domain={['auto', 'auto']}>
                        <Label
                            angle={270}
                            position="left"
                            style={{textAnchor: 'middle', fill: theme.palette.text.primary}}>
                            {leftLabelText}
                        </Label>
                    </YAxis>
                    <Tooltip/>
                    <Line type="linear"
                          stroke={theme.palette.primary.main}
                          dataKey="amount"
                          animationDuration={500}
                          dot={false}
                    />
                </LineChart>
            </ResponsiveContainer>
        </React.Fragment>
    )
}

export default Chart
