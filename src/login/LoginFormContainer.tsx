import {Field, Form, useFormikContext} from 'formik'
import {makeStyles} from '@material-ui/core/styles'
import {TextField} from 'formik-material-ui'
import {LOGIN_FORM_NAMES} from './login.constants'
import InputAdornment from '@material-ui/core/InputAdornment'
import {Email, Visibility, VisibilityOff} from '@material-ui/icons'
import React, {useCallback, useEffect, useState} from 'react'
import {Button, Checkbox, CircularProgress, FormControlLabel, Grid, Link} from '@material-ui/core'
import {useSelector} from 'react-redux'
import {isFetchLoginPending} from './redux/login.fetch'
import {REGISTRATION_PATH, RESET_PASSWORD_PATH} from '../config/routing/router.constants'
import {Validators} from '../shared/form-validators'
import {EMAIL_REMEMBER_ME_KEY} from '../core/components/utils'
import {ILogin} from './login.types'

interface ILoginFormContainer {
}

const useStyles = makeStyles((theme) => ({
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}))

const LoginFormContainer: React.FC<ILoginFormContainer> = () => {

    const classes = useStyles()

    // Formik
    const {values, setFieldValue} = useFormikContext<ILogin>()

    // Selector
    const isLoginPending = useSelector(isFetchLoginPending)

    // Local states
    // Remember me
    const [rememberMe, setRememberMe] = useState<boolean>(false)
    // Show password
    const [showPassword, setShowPassword] = useState<boolean>(false)

    // Handle change callback - Remember me
    // set email in localStorage on rememberMe value change
    const handleChangeRememberMe = useCallback(() => {
        // inverted boolean logic
        if (!rememberMe)
            localStorage.setItem(EMAIL_REMEMBER_ME_KEY, values?.email)
        else
            localStorage.removeItem(EMAIL_REMEMBER_ME_KEY)

        setRememberMe(!rememberMe)
    }, [rememberMe, values?.email])

    // Did mount - check if there is email saved in localStorage and set it
    useEffect(() => {
        const savedEmail = localStorage.getItem(EMAIL_REMEMBER_ME_KEY)
        savedEmail && setFieldValue(LOGIN_FORM_NAMES.email, savedEmail)
    }, [setFieldValue])

    return (
        <Form className={classes.form} noValidate>

            {/* Email */}
            <Field
                component={TextField}
                label="Email..."
                id="email"
                fullWidth
                name={LOGIN_FORM_NAMES.email}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <Email/>
                        </InputAdornment>
                    )
                }}
                disabled={isLoginPending}
                validate={Validators.required}
            />

            {/* Password */}
            <Field
                component={TextField}
                label="Password..."
                id="password"
                fullWidth
                name={LOGIN_FORM_NAMES.password}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end"
                                        className="c-pointer"
                                        onClick={() => setShowPassword(!showPassword)}>
                            {showPassword ? <Visibility fontSize="small"/> :
                                <VisibilityOff fontSize="small"/>}
                        </InputAdornment>
                    )
                }}
                type={showPassword ? 'text' : 'password'}
                disabled={isLoginPending}
                validate={Validators.required}
            />

            <FormControlLabel
                control={<Checkbox checked={rememberMe}
                                   onChange={handleChangeRememberMe}
                                   color="primary"/>}
                label="Remember me"
            />

            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                disabled={isLoginPending}
                className={classes.submit}
            >
                {
                    isLoginPending &&
                    <div className="mr-2" color="white"><CircularProgress color="inherit" size={20}/></div>
                }
                Sign In
            </Button>
            <Grid container>
                <Grid item xs>
                    <Link href={RESET_PASSWORD_PATH} variant="body2">
                        Forgot password?
                    </Link>
                </Grid>
                <Grid item>
                    <Link href={REGISTRATION_PATH} variant="body2">
                        Don't have an account? Sign Up
                    </Link>
                </Grid>
            </Grid>
        </Form>
    )
}

export default LoginFormContainer
