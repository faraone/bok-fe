import * as React from 'react'
import {useCallback, useEffect} from 'react'
import {Avatar, Grid, Paper, Typography} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import {LOGIN_FORM_INIT_VALUES} from './login.constants'
import {Formik} from 'formik'
import LoginFormContainer from './LoginFormContainer'
import {ILogin} from './login.types'
import {useDispatch, useSelector} from 'react-redux'
import {FETCH_LOGIN_ID, fetchLoginAction} from './redux/login.fetch'
import {baseRedirectingPathSelector} from '../core/components/redux/redirect.reducer'
import {useHistory} from 'react-router-dom'
import {clearRedirectPathAction} from '../core/components/utils'
import {isAuthenticatedSelector} from '../core/profile/redux/profile.reducer'
import {BANK_ACCOUNT_PATH, HOME_PAGE_PATH} from '../config/routing/router.constants'
import {sha256} from 'js-sha256'

interface ILoginContainerProps {
}

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh'
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/DeMmn2iaFLI/1920x1277)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        opacity: '0.4'
    }
}))

const LoginContainer: React.FC<ILoginContainerProps> = () => {

    // Style
    const classes = useStyles()

    const dispatch = useDispatch()
    const history = useHistory()

    const pathRedirecting = useSelector(baseRedirectingPathSelector)?.path
    const isAuthenticated = useSelector(isAuthenticatedSelector)

    // redirecting
    useEffect(() => {
        if (pathRedirecting) {
            history.push(pathRedirecting)
            dispatch(clearRedirectPathAction())
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pathRedirecting])

    useEffect(() => {
        if (isAuthenticated) {
            history.push(`${HOME_PAGE_PATH}${BANK_ACCOUNT_PATH}`)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const handleLogin = useCallback((values: ILogin) => {

        // Encrypt password
        const encryptedPassword = sha256(values?.password)

        // fetch login
        dispatch(fetchLoginAction.build({
                email: values?.email,
                password: encryptedPassword
            }, FETCH_LOGIN_ID
        ))
    }, [dispatch])

    return (
        <>
            <Grid container component="main" className={classes.root}>

                <Grid xs={false} sm={4} md={7} className={classes.image}/>

                <Grid xs={12} sm={8} md={5} component={Paper} elevation={6} square>

                    <div className={'col ' + classes.paper}>
                        <Avatar className={classes.avatar}>
                            <LockOutlinedIcon/>
                        </Avatar>
                        <Typography component="h1" variant="h5">
                            BOK - Sign in
                        </Typography>

                        <Formik
                            initialValues={LOGIN_FORM_INIT_VALUES}
                            onSubmit={handleLogin}
                            component={LoginFormContainer}
                            validateOnBlur={false}
                        />

                    </div>
                </Grid>
            </Grid>
        </>
    )
}

export default LoginContainer

