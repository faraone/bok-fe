import React, {useCallback} from 'react'
import {Card, CardContent, CircularProgress, createStyles, GridListTile, Tooltip, Typography} from '@material-ui/core'
import GridList from '@material-ui/core/GridList'
import {makeStyles} from '@material-ui/core/styles'
import VisibilityIcon from '@material-ui/icons/Visibility'
import {useDispatch, useSelector} from 'react-redux'
import {isFetchGetPlainPanCardPending} from '../../card/redux/card.fetch'
import {MAP_CARD_STATUS, MAP_CARD_STATUS_COLOR, MAP_CARD_TYPE} from '../../../../../core/components/utils'
import {clearPlainPANCardAction} from '../../card/redux/card.actions'
import {cardListSelector, currentPlainPANCardsSelector} from '../../card/redux/card.selectors'
import {openDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {DIALOG_NAMES} from '../../../../shared/dialog/redux/dialog.reducer'

const useStyles = makeStyles((theme) => createStyles({
    tile: {
        height: 160
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        color: 'black'
    },
    root: {
        maxHeight: 170,
        minWidth: 270,
        maxWidth: 270,
        marginRight: 10
    },
    tileBar: {
        background: 'white'
    },
    titleWrap: {
        color: theme.palette.primary.main
    },
    card: {
        minWidth: 230,
        height: 'inherit',
        background: 'linear-gradient(135deg, #a7e858 0%,#2f7f9f 100%)',
        borderRadius: 12,
        border: 0
    }
}))

export const BankCards: React.FC<any> = () => {

    // Style
    const classes = useStyles()
    const dispatch = useDispatch()

    // Selector
    const cardList = useSelector(cardListSelector)
    const currentPlainPAN = useSelector(currentPlainPANCardsSelector)
    const isFetchCurrentPlainPANPending = useSelector(isFetchGetPlainPanCardPending)

    const getPlainPAN = useCallback((cardId: string) => {

        // clear plain PAN card state
        dispatch(clearPlainPANCardAction())

        if (!currentPlainPAN) {
            // open dialog to insert card pin
            dispatch(openDialogAction({
                name: DIALOG_NAMES.INSERT_CARD_PIN_FOR_PAN,
                meta: {
                    cardId: cardId
                }
            }))
        }

    }, [currentPlainPAN, dispatch])

    const openCardDetailDialog = useCallback((currentCard) => {
        dispatch(openDialogAction({
            name: DIALOG_NAMES.CARD_DETAIL,
            meta: {
                card: currentCard
            },
            maxWidth: 'xs'
        }))
    }, [dispatch])

    return <>
        <div className="row">
            <div className="col-12 d-flex flex-column ">
                <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Cards
                </Typography>
            </div>
        </div>

        <div className="row">
            <div className="col-12 justify-content-start">

                <GridList cols={2.5} className={classes.gridList}>

                    {    //@ts-ignore
                        Array.isArray(cardList) && cardList?.length > 0 ?
                            cardList?.map((card, index) => {

                                return <GridListTile key={index}
                                                     classes={{
                                                         root: classes.root,
                                                         tile: classes.tile
                                                     }}>

                                    <Card classes={{root: classes.card}} variant="outlined">
                                        <CardContent>

                                            {/*Card status circle*/}
                                            <div className="row">
                                                <div className="col mb-1">
                                                    <Tooltip title={`status: ${MAP_CARD_STATUS[card.cardStatus]}`}>
                                                            <span
                                                                style={{
                                                                    height: '15px',
                                                                    width: '15px',
                                                                    backgroundColor: MAP_CARD_STATUS_COLOR[card.cardStatus],
                                                                    borderRadius: '50%',
                                                                    display: 'inline-block'
                                                                }}/>
                                                    </Tooltip>
                                                </div>
                                            </div>

                                            {/*Card PAN*/}
                                            <div className="row">

                                                <div
                                                    className="col-12 d-flex justify-content-between align-items-center">

                                                    <Typography variant="h6"
                                                                onClick={() =>
                                                                    openCardDetailDialog(card)
                                                                }
                                                                className="flex-grow-1 c-pointer">
                                                        {currentPlainPAN?.[card?.cardId] || card?.maskedPan}
                                                    </Typography>

                                                    {isFetchCurrentPlainPANPending ? <CircularProgress size="20"/> :
                                                        <VisibilityIcon fontSize="small"
                                                                        style={{cursor: 'pointer'}}
                                                                        onClick={() =>
                                                                            getPlainPAN(card.cardId + '')
                                                                        }/>}
                                                </div>
                                            </div>

                                            <div className="row">

                                                {/*Card label and status circle*/}
                                                <div className="col-12 mt-2 c-pointer"
                                                     onClick={() => openCardDetailDialog(card)}>
                                                    <Typography
                                                        color="textSecondary"
                                                        variant="caption"
                                                        gutterBottom>
                                                        {card?.name} - {card?.label}
                                                    </Typography> <br/>

                                                    {/*Card type*/}
                                                    <Typography variant="caption"
                                                                color="textSecondary">
                                                        type: {MAP_CARD_TYPE[card.type]}
                                                    </Typography>
                                                </div>
                                            </div>

                                        </CardContent>
                                    </Card>
                                </GridListTile>

                            }) : <Typography>
                                No card available. Create first one
                            </Typography>
                    }

                </GridList>
            </div>
        </div>


    </>
}
