import * as React from 'react'
import {useCallback} from 'react'
import {Button, CircularProgress, DialogContent, FormHelperText, InputLabel, MenuItem} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {NEW_CARD_INIT_VALUES} from '../bank-account.constants'
import {INewCardRequest} from '../../card/redux/card.types'
import {ErrorMessage, Field, Form, Formik} from 'formik'
import {Select, TextField} from 'formik-material-ui'
import {Validators} from '../../../../../shared/form-validators'
import Grid from '@material-ui/core/Grid'
import {FETCH_CREATE_CARD_ID, fetchCreateCardAction, isFetchCreateCardPending} from '../../card/redux/card.fetch'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'
import {FormikHelpers} from 'formik/dist/types'
import {CARD_TYPE_LABEL} from '../bank-account.contants'

interface ICreateNewCardDialog {
}

const CreateNewCardDialog: React.FC<ICreateNewCardDialog> = () => {

    const dispatch = useDispatch()
    const isCreateCardPending = useSelector(isFetchCreateCardPending)

    // Close dialogs callback
    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    // Submit new card
    const handleSubmit = useCallback((values: INewCardRequest, formikHelpers: FormikHelpers<INewCardRequest>) => {
        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchCreateCardAction.build(values, FETCH_CREATE_CARD_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

        formikHelpers.setSubmitting(false)
    }, [dispatch])

    return (
        <>
            <DialogContent>

                <Typography variant="h5" color="primary"> Create card </Typography>

                <Formik
                    initialValues={NEW_CARD_INIT_VALUES}
                    onSubmit={handleSubmit}
                    validateOnBlur={false}
                >
                    {({errors, touched}) => <Form>
                        {/*Card label*/}
                        <Grid item xs={12}>
                            <Field
                                component={TextField}
                                name="label"
                                label="Card label"
                                fullWidth
                                validate={Validators.required}
                            />
                        </Grid>

                        {/*Card name*/}
                        <Grid item xs={12}>
                            <Field
                                component={TextField}
                                name="name"
                                label="Name"
                                fullWidth
                                validate={Validators.required}
                            />
                        </Grid>

                        {/*Card type*/}
                        <Grid item xs={12} className="mt-2">
                            <InputLabel htmlFor="card-type"
                                        error={!!errors.type && !!touched.type}
                            >Card type</InputLabel>
                            <Field
                                name="type"
                                id="card-type"
                                component={Select}
                                fullWidth
                                displayEmpty
                                validate={Validators.required}>

                                <MenuItem value="" disabled>Select card type</MenuItem>

                                {Object.keys(CARD_TYPE_LABEL)?.map((cardType, index) =>
                                    <MenuItem value={cardType} key={index}>
                                        {CARD_TYPE_LABEL[cardType]}
                                    </MenuItem>)}
                            </Field>
                            <ErrorMessage
                                name="type"
                                render={(message) => <FormHelperText
                                    error> {message} </FormHelperText>}
                            />
                        </Grid>

                        <div className="d-flex justify-content-between w-100 p-3">
                            <Button variant="outlined" color="default" onClick={closeDialog}> Close </Button>
                            <Button variant="contained" color="primary" type="submit" disabled={isCreateCardPending}>
                                {
                                    isCreateCardPending &&
                                    <CircularProgress color="inherit" size={20} className="mr-2"/>
                                }
                                Create
                            </Button>
                        </div>

                    </Form>}

                </Formik>
            </DialogContent>

        </>
    )
}

export default CreateNewCardDialog
