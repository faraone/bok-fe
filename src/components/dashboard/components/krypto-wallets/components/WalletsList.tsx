import React, {useCallback} from 'react'
import GridList from '@material-ui/core/GridList'
import {createStyles, GridListTile, GridListTileBar, useMediaQuery} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'
import {useDispatch, useSelector} from 'react-redux'
import {kryptoSymbolWalletListSelector} from '../redux/krypto-wallets.selectors'
import Typography from '@material-ui/core/Typography'
import {changeSelectedWalletSymbol} from '../redux/krypto-wallets.reducers'

const useStyles = makeStyles((theme) => createStyles({
    tile: {
        height: 60
    },
    gridList: {
        flexWrap: 'nowrap',
        // Promote the list into his own layer on Chrome. This cost memory but helps keeping high FPS.
        transform: 'translateZ(0)',
        cursor: 'pointer',
        color: 'black'
    },
    root: {
        maxHeight: 80
    },
    tileBar: {
        background: 'white'
    },
    titleWrap: {
        color: theme.palette.primary.main,
        marginRight: 0
    }
}))

/**
 * Display wallets krypto symbol
 * */
const WalletsList: React.FC<any> = () => {

    const classes = useStyles()
    const dispatch = useDispatch()

    const kryptoList = useSelector(kryptoSymbolWalletListSelector)

    // Match current component width with specified media query
    // Needed to specify number of krypto symbol showed in list
    const matchesMD = useMediaQuery('(min-width:600px)')

    const changeWallet = useCallback((kryptoLabel: string) => {
        // set krypto label
        dispatch(changeSelectedWalletSymbol(kryptoLabel))
    }, [dispatch])

    return <div className="row flex-wrap justify-content-around overflow-hidden">

        <div className="col-12 justify-content-start">

            {Array.isArray(kryptoList) && kryptoList?.length > 0 ?
                <>
                    <Typography variant="subtitle2">
                        Select krypto:
                    </Typography>
                    <GridList cols={matchesMD ? 7 : 3} className={classes.gridList}>

                        {kryptoList.map((kryptoLabel, index) => {
                            return <GridListTile key={index}
                                                 onClick={() => changeWallet(kryptoLabel)}
                                                 classes={{
                                                     root: classes.root,
                                                     tile: classes.tile
                                                 }}>
                                <GridListTileBar
                                    title={kryptoLabel}
                                    classes={{
                                        root: classes.tileBar,
                                        titleWrap: classes.titleWrap
                                    }}
                                />
                            </GridListTile>

                        })}

                    </GridList>
                </>
                : <Typography>No wallets ,what are you waiting for to buy in the market?</Typography>}
        </div>
    </div>
}

export default WalletsList
