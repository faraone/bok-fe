import {IAction} from "fetch-with-redux-observable/dist/types"
import {Selector} from "reselect"
import {IRootState} from "../../../reducer"
import {CLEAR_REDIRECT_PATH_ACTION, REDIRECT_PATH_ACTION} from "../utils"

export interface IRedirectState {
    path: string | null
}

// handle changing drawer action
export const redirectPathReducer = (state: IRedirectState = {path: null}, action: IAction<string>): IRedirectState => {
    switch (action.type) {
        case REDIRECT_PATH_ACTION:
            const path = action.payload ? action.payload : '/'
            return {
                path
            }
        case CLEAR_REDIRECT_PATH_ACTION:
            return {
                path: null
            }
        default:
            return state
    }
}

// path to redirect base selector
export const baseRedirectingPathSelector: Selector<IRootState, IRedirectState> = (state: IRootState): IRedirectState => state.redirectPath

