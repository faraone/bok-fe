import {Observable} from 'rxjs'
import {IFailureFetchAction, TYPE_FETCH_CONST} from 'fetch-with-redux-observable'
import {filter, mergeMap} from 'rxjs/operators'
import {addError} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {DEFAULT_ERROR_MESSAGE} from './fetch.utils'
import {HTTP_ERROR_CLIENT_MESSAGE} from './user-message-errors'
import {redirectPathAction, removeTokenCookies} from '../components/utils'
import {logoutAction} from '../profile/redux/profile.fetch'
import {LOGIN_PATH} from '../../config/routing/router.constants'

/**
 * epic take failure fetch and dispatch snackbar error
 */
export const failureFetchActionEpic = (action$: Observable<IFailureFetchAction>) => {
    return action$.pipe(
        filter((action: IFailureFetchAction) => action.type.includes('FAILURE') && action.type.includes(TYPE_FETCH_CONST)),
        mergeMap(
            (failureFetchAction: any) => {

                // get error code status
                const errorCode = failureFetchAction?.payload?.status || 500

                // get current error message or set one
                const currentErrorMessage = errorCode === 400 || (errorCode === 500 && failureFetchAction?.payload?.response?.message) ?
                    failureFetchAction?.payload?.response?.message :
                    // Unauthorized error response message
                    errorCode === 401 ? 'Invalid session. Please log in again' : DEFAULT_ERROR_MESSAGE

                // check if current failure fetch has custom error message
                const httpErrorClientMessage = {...HTTP_ERROR_CLIENT_MESSAGE}

                const errorMessage = httpErrorClientMessage[failureFetchAction.type] ?
                    httpErrorClientMessage[failureFetchAction.type][errorCode] ?
                        httpErrorClientMessage[failureFetchAction.type][errorCode] :
                        httpErrorClientMessage[failureFetchAction.type][500] :
                    currentErrorMessage

                return [addError({
                    userMessage: errorMessage,
                    error: failureFetchAction.payload
                })]
            }
        )
    )

}


/**
 * This epic takes failure fetch action with 401 status (unauthorized) and log out user.
 * This control prevent user to use application logged in with and invalid token in the cookies,
 * if it not refresh page and verifyTokenIfPresent() function didn't execute
 */
export const logoutOnTokenExpiredEpic = (action$: Observable<IFailureFetchAction>) => {
    return action$.pipe(
        filter((action: IFailureFetchAction) => action.type.includes('FAILURE') &&
            action.type.includes(TYPE_FETCH_CONST) &&
            action?.payload?.status === 401),
        mergeMap(() => {
                removeTokenCookies()

                return [
                    // Clear state
                    logoutAction(),
                    // Redirect to login page
                    redirectPathAction(LOGIN_PATH)
                ]
            }
        )
    )

}
