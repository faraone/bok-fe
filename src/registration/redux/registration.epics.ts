import {NEVER, Observable} from 'rxjs'
import {IAction} from 'fetch-with-redux-observable'
import {ofType} from 'redux-observable'
import {fetchRegistrationAction} from './registration.fetch'
import {flatMap} from 'rxjs/internal/operators'
import {addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {SUCCESS_REGISTRATION_MESSAGE} from '../registration.constants'
import {redirectPathAction} from '../../core/components/utils'
import {LOGIN_PATH} from '../../config/routing/router.constants'

export const successRegistrationEpic = (action$: Observable<IAction>) =>
    action$.pipe(
        ofType(fetchRegistrationAction.successActionType),
        flatMap((response: any) => {
            return response.payload?.status === 'registered' ? [
                // Success snackbar
                addSuccess({userMessage: SUCCESS_REGISTRATION_MESSAGE}),
                // Redirect to login
                redirectPathAction(LOGIN_PATH)
            ] : NEVER
        })
    )
