import {
    BANK_ACCOUNT_PATH,
    HOME_PAGE_PATH,
    KRYPTO_MARKETPLACE_PATH,
    PROFILE_INFO_PATH,
    WALLETS_PATH
} from '../../../config/routing/router.constants'

// Drawer item keys for redirecting
export const BANK_ACCOUNT_ITEM = 'BANK_ACCOUNT_ITEM'
export const WALLET_ITEM = 'WALLET_ITEM'
export const MARKETPLACE_ITEM = 'MARKETPLACE_ITEM'

// Map keys to redirecting path for drawer items
export const MAP_ITEM_TO_PATH: { [key: string]: string } = {
    [BANK_ACCOUNT_ITEM]: `${HOME_PAGE_PATH}${BANK_ACCOUNT_PATH}`,
    [WALLET_ITEM]: `${HOME_PAGE_PATH}${WALLETS_PATH}`,
    [MARKETPLACE_ITEM]: `${HOME_PAGE_PATH}${KRYPTO_MARKETPLACE_PATH}`,
    [PROFILE_INFO_PATH]: `${HOME_PAGE_PATH}${PROFILE_INFO_PATH}`
}
