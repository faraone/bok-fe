import * as React from 'react'
import {Typography} from '@material-ui/core'
import ChangePasswordFormContainer from './ChangePasswordFormContainer'

interface IChangePasswordContainer {
}

/**
 * Shared container with change password form
 * */
const ChangePasswordContainer: React.FC<IChangePasswordContainer> = () => {

    return <>
        <div className="col flex-grow-1 overflow-auto mt-3">

            <Typography variant="h5" color="primary" gutterBottom>
                Change password
            </Typography>

            <Typography variant="subtitle1" gutterBottom>
                To continue in Bok, please set a new password
            </Typography>


            <div className="col flex-grow-1 overflow-auto mt-3">
                <ChangePasswordFormContainer/>
            </div>

        </div>
    </>
}

export default ChangePasswordContainer
