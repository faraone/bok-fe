import {ILoginInfo} from '../../login/login.types'

export interface IProfileInfoResponse {
    email: string
    fullName: string
    icc: string
    mobile: string
    status: string
    type: string
}

export interface IProfile {
    isAuthenticated: boolean
    loginInfo: ILoginInfo | null
    profileInfo: IProfileInfoResponse | null
    passwordResetNeeded: boolean
}
