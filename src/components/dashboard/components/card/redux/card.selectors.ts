import {createSelector} from 'reselect'
import {baseBankSelector} from '../../bank-account/redux/bank-account.reducers'
import {IBaseBankState} from '../../bank-account/redux/bank-account.types'
import {ILabelValueOption} from '../../../dashboard.types'
import {ECardStatus, ICardInfo} from './card.types'
import {MAP_CARD_TYPE} from '../../../../../core/components/utils'

/**
 * card list selector
 * replace char 'XXXX' with '****' to display card better
 * */
export const cardListSelector = createSelector(baseBankSelector,
    (bankAccount) => bankAccount?.cardList?.sort((firstCard, secondCard) => {
        // sort card list by ECardStatus order
        return Object.keys(ECardStatus).indexOf(firstCard.cardStatus) - Object.keys(ECardStatus).indexOf(secondCard.cardStatus)
    })
        .map((card) => ({
                ...card,
                maskedPan: card?.maskedPan?.match(/.{0,4}/g)?.join(' ').replaceAll('X', '*')
            })
        )
)

export const cardListSourceOptionsSelector = createSelector(baseBankSelector,
    (bankAccount): ILabelValueOption<ICardInfo>[] => bankAccount?.cardList ?
        bankAccount?.cardList?.filter(card => card.cardStatus === ECardStatus.ACTIVE)
            .map((card) => ({
                    label: card.label + ' - ' + MAP_CARD_TYPE[card.type],
                    value: card
                })
            ) : []
)

export const currentPlainPANCardsSelector = createSelector(baseBankSelector,
    (bankAccount: IBaseBankState) => bankAccount?.cardsCurrentPlainPAN
)

export const currentCvvCardSelector = createSelector(baseBankSelector,
    (bankAccount: IBaseBankState) => bankAccount?.cardDialogDetail?.currentCardCVV
)

export const currentPanCardDialogSelector = createSelector(baseBankSelector,
    (bankAccount: IBaseBankState) => bankAccount?.cardDialogDetail?.currentPanCard
)

export const cardPinDialogSelector = createSelector(baseBankSelector,
    (bankAccount: IBaseBankState) => bankAccount?.cardDialogDetail?.cardPin
)

