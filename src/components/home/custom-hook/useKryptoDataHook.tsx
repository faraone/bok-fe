import {useEffect, useMemo} from 'react'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../core/components/utils'
import {useDispatch, useSelector} from 'react-redux'
import {
    FETCH_ALL_USER_CARDS_ID,
    fetchAllUserCardsAction,
    isFetchAllUserCardsPending
} from '../../dashboard/components/card/redux/card.fetch'
import {
    FETCH_KRYPTO_MARKET_LIST_ID,
    fetchKryptoMarketListAction,
    isFetchKryptoMarketListPending
} from '../../dashboard/components/krypto-marketplace/redux/krypto-marketplace.fetch'
import {
    FETCH_WALLETS_LIST_ID,
    fetchWalletsListInfoAction,
    isFetchWalletsListPending
} from '../../dashboard/components/krypto-wallets/redux/krypto-wallets.fetch'

/**
 * Custom hook with shared fetch between dashboard page.
 * Get user cards, user wallets, marketplace krypto available, before every render page
 * in order to have updated data
 * */
export const useKryptoDataHook = () => {

    const dispatch = useDispatch()

    // Selector
    const isKryptoMarketplacePending = useSelector(isFetchKryptoMarketListPending)
    const isWalletListPending = useSelector(isFetchWalletsListPending)
    const isAllCardPending = useSelector(isFetchAllUserCardsPending)

    // did mount - Fetch profile info
    useEffect(() => {

        const token = Cookies.get(USER_TOKEN)

        /** SHARED DATA FETCH ACTION */
        // Fetch account card list
        dispatch(fetchAllUserCardsAction.build(null, FETCH_ALL_USER_CARDS_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
        // Fetch krypto wallet list
        dispatch(fetchWalletsListInfoAction.build(null, FETCH_WALLETS_LIST_ID,
            undefined, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))
        // Fetch krypto market list
        dispatch(fetchKryptoMarketListAction.build(null, FETCH_KRYPTO_MARKET_LIST_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
    }, [dispatch])

    const isRequestsPending = useMemo(() =>
        isKryptoMarketplacePending ||
        isWalletListPending ||
        isAllCardPending
        , [isAllCardPending, isKryptoMarketplacePending, isWalletListPending])

    return {isRequestsPending}
}
