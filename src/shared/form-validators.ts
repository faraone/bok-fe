// check if form field is present
import moment from 'moment'

const required = (value: any): string | undefined => !!value ? undefined : 'Required'

// validate email regex
const email = (value: string) =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
        'Please enter a valid email' : undefined

// check if person is adult
const isAdult = (value: string) => {
    const formattedDate = moment(value)
    return value && moment().diff(formattedDate, 'years') >= 18 ? undefined : 'User should be of age'
}

// check postal code
const postalCode = (value: string) => value && (value.length === 4 || value.length === 5) ? undefined : 'Please enter a valid postal code'


// Formik Validators
export const Validators = {
    required,
    email,
    isAdult,
    postalCode
}
