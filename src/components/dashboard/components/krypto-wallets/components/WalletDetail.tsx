import React, {useCallback, useEffect, useMemo, useState} from 'react'
import Typography from '@material-ui/core/Typography'
import {currentWalletDetailSelector, selectedWalletSymbolSelector} from '../redux/krypto-wallets.selectors'
import {useDispatch, useSelector} from 'react-redux'
import InfoOutlinedIcon from '@material-ui/icons/InfoOutlined'
import {IconButton, Tooltip} from '@material-ui/core'
import Chart from '../../../../../shared/Chart'
import {useKryptoWalletsHook} from '../useKryptoWalletsHook'
import {marketKryptoListSelector} from '../../krypto-marketplace/redux/krypto-marketplace.selectors'
import moment from 'moment'
import {FileCopy} from '@material-ui/icons'
import {copyToClipboard} from 'fetch-with-redux-observable/dist/utils'
import {addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {KeyboardDatePicker} from '@material-ui/pickers'
import {makeStyles} from '@material-ui/core/styles'
import Cookies from 'js-cookie'
import {DATE_TIME_CUSTOM_FORMAT, KRYPTO_SYMBOL_PATH_VARIABLE, USER_TOKEN} from '../../../../../core/components/utils'
import {FETCH_WALLET_INFO_ID, fetchWalletInfoAction} from '../redux/krypto-wallets.fetch'

interface IWalletDetail {
}

const useStyles = makeStyles(() => ({
    datePicker: {padding: 0}
}))

/**
 * Wallet detail component, with current amount and delete button
 * */
const WalletDetail: React.FC<IWalletDetail> = () => {

    const {
        chartData
    } = useKryptoWalletsHook()

    const dispatch = useDispatch()
    const classes = useStyles()

    // Selector
    const currentWalletDetail = useSelector(currentWalletDetailSelector)
    const selectedWalletSymbol = useSelector(selectedWalletSymbolSelector)
    const kryptoMarketplaceList = useSelector(marketKryptoListSelector)

    // Local state
    const [startDateChart, setStartDateChart] = useState(moment().subtract(1, 'days').startOf('day'))
    const [endDateChart, setEndDateChart] = useState(moment().startOf('day'))

    // Calcolate current krypto wallet converted amount
    const convertedWalletAmount = useMemo(() => {
        const currentKryptoPrice = kryptoMarketplaceList?.find(krypto => krypto?.symbol === selectedWalletSymbol)?.price
        const convertedValue = (Number(currentKryptoPrice) * Number(currentWalletDetail?.availableAmount || 0))?.toLocaleString('it-IT', {maximumFractionDigits: 2})
        return convertedValue ? convertedValue : '0'
    }, [kryptoMarketplaceList, currentWalletDetail?.availableAmount, selectedWalletSymbol])

    // Normalize date
    const creationDate = useMemo(() => moment(currentWalletDetail?.creationTimestamp).isValid() ?
        moment(currentWalletDetail?.creationTimestamp).format(DATE_TIME_CUSTOM_FORMAT) : '', [currentWalletDetail?.creationTimestamp])

    const lastUpdateDate = useMemo(() => moment(currentWalletDetail?.updateTimestamp).isValid() ?
        moment(currentWalletDetail?.updateTimestamp).format(DATE_TIME_CUSTOM_FORMAT) : '', [currentWalletDetail?.updateTimestamp])

    // Copy wallet address
    const copyWalletAddress = useCallback(() => {
        copyToClipboard(currentWalletDetail?.address)
        dispatch(addSuccess({userMessage: 'Wallet address copied'}))
    }, [currentWalletDetail?.address, dispatch])


    useEffect(() => {
        const token = Cookies.get(USER_TOKEN)

        selectedWalletSymbol && dispatch(fetchWalletInfoAction.build(null, FETCH_WALLET_INFO_ID, {
            [KRYPTO_SYMBOL_PATH_VARIABLE]: selectedWalletSymbol
        }, {
            startDate: startDateChart.startOf('day').toISOString(),
            endDate: endDateChart.endOf('day').toISOString()
        }, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
    }, [dispatch, endDateChart, selectedWalletSymbol, startDateChart])

    return <div className="row">
        <div className="col-12 col-md-4">
            <div className="row">
                <div className="col-12 d-flex flex-column align-items-start">
                    {
                        currentWalletDetail ? <>

                                {/* Wallet available amount */}
                                <Typography variant="h6">
                                    <Tooltip title={
                                        <div>
                                            {/* Wallet update and creation datetime */}
                                            {'last update: ' + lastUpdateDate} <br/>
                                            {'creation date: ' + creationDate}
                                        </div>}>
                                        <InfoOutlinedIcon className="mr-2 c-pointer" color="primary" fontSize="small"/>
                                    </Tooltip>
                                    {currentWalletDetail.symbol + ' ' + currentWalletDetail.availableAmount}
                                </Typography>

                                <Typography variant="subtitle2" className="mt-1">
                                    converted amount: {convertedWalletAmount + ' $'}
                                </Typography>

                            </> :
                            <>
                                <Typography variant="subtitle1">Please, select one of the wallet above </Typography>
                            </>
                    }
                </div>
            </div>

            <div className="row mt-1">
                <div className="col">
                    <Typography variant="subtitle2" className="overflow-auto">
                        <IconButton size="small"
                                    onClick={() => copyWalletAddress()}>
                            <FileCopy color="primary"/>
                        </IconButton>
                        wallet public address: {currentWalletDetail?.address || ''}
                    </Typography>
                    <Typography variant="caption"
                                color="textSecondary">last update : {lastUpdateDate}</Typography>
                </div>
            </div>

        </div>

        {/*Wallet chart*/}
        {currentWalletDetail && <div className="col-12 col-md-8 my-2" style={{height: 200}}>
            <Chart
                data={chartData}
                noTitleTypography
                leftLabelText="Amount"
                title={
                    <>
                        <div className="d-flex flex-column">
                            <Typography variant="h6" color="primary"> History </Typography>
                        </div>

                        {/*Wallet chart filter*/}
                        <div className="row justify-content-end">
                            {/*Start date*/}
                            <div className="col-6 col-sm-4 col-lg-3">
                                <KeyboardDatePicker
                                    size="small"
                                    variant="inline"
                                    format="DD/MM/YYYY"
                                    margin="normal"
                                    className="my-0"
                                    label="Start date"
                                    value={startDateChart}
                                    onChange={newStartDate => newStartDate && setStartDateChart(newStartDate)}
                                    disableFuture
                                    autoOk
                                    KeyboardButtonProps={{className: classes.datePicker}}/>
                            </div>

                            {/*End date*/}
                            <div className="col-6 col-sm-4 col-lg-3">
                                <KeyboardDatePicker
                                    size="small"
                                    variant="inline"
                                    className="my-0"
                                    format="DD/MM/YYYY"
                                    margin="normal"
                                    label="End date"
                                    value={endDateChart}
                                    onChange={newEndDate => newEndDate && setEndDateChart(newEndDate)}
                                    disableFuture
                                    autoOk
                                    KeyboardButtonProps={{className: classes.datePicker}}/>
                            </div>
                        </div>
                    </>
                }
            />
        </div>}
    </div>
}

export default WalletDetail
