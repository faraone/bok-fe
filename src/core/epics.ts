import {combineEpics, Epic} from 'redux-observable'
import {generalSpinnerEpics, IAction, IFetchAction, switchMapFetchEpics} from 'fetch-with-redux-observable'
import {fetchChangePasswordAction, fetchLoginAction, fetchLogoutAction} from '../login/redux/login.fetch'
import {failureFetchActionEpic, logoutOnTokenExpiredEpic} from './fetch/fetch.epics'
import {fetchRegistrationAction} from '../registration/redux/registration.fetch'
import {userSessionEpics} from '../login/redux/login.epics'
import {fetchCloseAccountAction, fetchLastAccessInfoAction, fetchProfileInfoAction} from './profile/redux/profile.fetch'
import {
    fetchAllBankTransactionAction,
    fetchBankAccountInfoAction,
    fetchWireTransferAction
} from '../components/dashboard/components/bank-account/redux/bank-account.fetch'
import {
    fetchActivateCardAction,
    fetchAllUserCardsAction,
    fetchCardPINAction,
    fetchChangeCardStatusAction,
    fetchCreateCardAction,
    fetchDeleteCardAction,
    fetchGetCardTokenAction,
    fetchGetCvvCardAction,
    fetchGetPlainPanCardAction
} from '../components/dashboard/components/card/redux/card.fetch'
import {
    fetchCreateWalletAction,
    fetchDeleteWalletAction,
    fetchTransferKryptoAction,
    fetchValidateAddressAction,
    fetchWalletInfoAction,
    fetchWalletsListInfoAction
} from '../components/dashboard/components/krypto-wallets/redux/krypto-wallets.fetch'
import {cardEpics} from '../components/dashboard/components/card/redux/card.epics'
import {
    fetchBuyKryptoAction,
    fetchKryptoMarketListAction,
    fetchKryptoPriceBySymbolAction,
    fetchKyptoHistorticalDataAction,
    fetchKyptoInfoBySymbolAction,
    fetchKyptoMarketInfosAction,
    fetchKyptoMarketPricesAction,
    fetchSellKyptoAction
} from '../components/dashboard/components/krypto-marketplace/redux/krypto-marketplace.fetch'
import {dashboardEpics} from '../components/dashboard/redux/dashboard.epics'
import {kryptoWalletsEpics} from '../components/dashboard/components/krypto-wallets/redux/krypto-wallets.epics'
import {fetchResetPasswordAction, fetchResetPasswordNeededAction} from '../reset-password/redux/reset-password.fetch'
import {requestResetPasswordSuccessEpic} from '../reset-password/redux/reset-password.epics'

//@ts-ignore
export const rootEpics: Epic = combineEpics<Epic<IFetchAction | IAction>>(
    generalSpinnerEpics,
    failureFetchActionEpic,
    logoutOnTokenExpiredEpic,
    // Login
    switchMapFetchEpics(fetchLoginAction.pendingActionTypeWithSpinner),
    // Logout
    switchMapFetchEpics(fetchLogoutAction.pendingActionTypeWithSpinner),
    // Reset password
    switchMapFetchEpics(fetchResetPasswordAction.pendingActionTypeWithSpinner),
    // Reset password needed
    switchMapFetchEpics(fetchResetPasswordNeededAction.pendingActionTypeWithSpinner),
    // Registration
    switchMapFetchEpics(fetchRegistrationAction.pendingActionTypeWithSpinner),
    // Change password
    switchMapFetchEpics(fetchChangePasswordAction.pendingActionTypeWithSpinner),
    // Profile info
    switchMapFetchEpics(fetchProfileInfoAction.pendingActionTypeWithSpinner),
    // Close account
    switchMapFetchEpics(fetchCloseAccountAction.pendingActionTypeWithSpinner),
    // Last access action
    switchMapFetchEpics(fetchLastAccessInfoAction.pendingActionTypeWithSpinner),
    // Bank account action
    switchMapFetchEpics(fetchBankAccountInfoAction.pendingActionTypeWithSpinner),
    // Bank card list action
    switchMapFetchEpics(fetchAllUserCardsAction.pendingActionTypeWithSpinner),
    // Card plain PAN action
    switchMapFetchEpics(fetchGetPlainPanCardAction.pendingActionTypeWithSpinner),
    // Create new bank card
    switchMapFetchEpics(fetchCreateCardAction.pendingActionTypeWithSpinner),
    // Create delete card
    switchMapFetchEpics(fetchDeleteCardAction.pendingActionTypeWithSpinner),
    // Get card token
    switchMapFetchEpics(fetchGetCardTokenAction.pendingActionTypeWithSpinner),
    // Get card cvv
    switchMapFetchEpics(fetchGetCvvCardAction.pendingActionTypeWithSpinner),
    // All bank transaction
    switchMapFetchEpics(fetchAllBankTransactionAction.pendingActionTypeWithSpinner),
    // Change card status
    switchMapFetchEpics(fetchChangeCardStatusAction.pendingActionTypeWithSpinner),
    // Activate card
    switchMapFetchEpics(fetchActivateCardAction.pendingActionTypeWithSpinner),
    // Get card PIN
    switchMapFetchEpics(fetchCardPINAction.pendingActionTypeWithSpinner),
    // Wire transfer
    switchMapFetchEpics(fetchWireTransferAction.pendingActionTypeWithSpinner),

    // Wallet list
    switchMapFetchEpics(fetchWalletsListInfoAction.pendingActionTypeWithSpinner),
    // Wallet info
    switchMapFetchEpics(fetchWalletInfoAction.pendingActionTypeWithSpinner),
    // Delete wallet
    switchMapFetchEpics(fetchDeleteWalletAction.pendingActionTypeWithSpinner),
    // Create wallet
    switchMapFetchEpics(fetchCreateWalletAction.pendingActionTypeWithSpinner),
    // Transfer krypto
    switchMapFetchEpics(fetchTransferKryptoAction.pendingActionTypeWithSpinner),
    // Validate wallet address before transfer
    switchMapFetchEpics(fetchValidateAddressAction.pendingActionTypeWithSpinner),

    // Market krypto list
    switchMapFetchEpics(fetchKryptoMarketListAction.pendingActionTypeWithSpinner),
    // Market krypto price by symbol
    switchMapFetchEpics(fetchKryptoPriceBySymbolAction.pendingActionTypeWithSpinner),
    // Market buy krypto
    switchMapFetchEpics(fetchBuyKryptoAction.pendingActionTypeWithSpinner),
    // Market krypto historical data
    switchMapFetchEpics(fetchKyptoHistorticalDataAction.pendingActionTypeWithSpinner),
    // Market krypto infos
    switchMapFetchEpics(fetchKyptoMarketInfosAction.pendingActionTypeWithSpinner),
    // Market krypto prices
    switchMapFetchEpics(fetchKyptoMarketPricesAction.pendingActionTypeWithSpinner),
    // Market sell krypto
    switchMapFetchEpics(fetchSellKyptoAction.pendingActionTypeWithSpinner),
    // Market krypto info by symbol
    switchMapFetchEpics(fetchKyptoInfoBySymbolAction.pendingActionTypeWithSpinner),

    // Reset password epic
    requestResetPasswordSuccessEpic,

    // Login/logout epics
    userSessionEpics,
    // dashboard epics
    dashboardEpics,
    // wallet epics
    kryptoWalletsEpics,
    // card epics
    cardEpics
)
