import React, {useCallback, useMemo} from 'react'
import 'react-phone-number-input/style.css'
import PropTypes from 'prop-types'
import {ErrorMessage, getIn} from 'formik'
import {FieldAttributes} from 'formik/dist/Field'
import PhoneInput from 'react-phone-number-input'
import {FormHelperText, InputLabel} from '@material-ui/core'

/**
 * Custom component to combine PhoneInput from react-phone-number-input to Formik
 * */
const PhoneInputField = (props: FieldAttributes<any>) => {

    // Field props
    const {
        className,
        field: {name, value},
        form: {
            errors, setFieldValue, touched, handleBlur
        },
        label,
        country,
        onChange,
        disabled
    } = props

    const isError = getIn(touched, name) && getIn(errors, name)
    const errorStyle = useMemo(() => isError ? 'error' : '', [isError])
    const filledStyle = useMemo(() => value ? 'filled' : '', [value])
    const disabledStyle = useMemo(() => disabled ? 'disabled' : '', [disabled])

    const onValueChange = useCallback((phoneNumber: string) => {
        setFieldValue(name, phoneNumber)

        if (onChange !== null) {
            onChange(phoneNumber)
        }
    }, [name, onChange, setFieldValue])

    return (
        <div className={`${className} ${errorStyle} ${filledStyle} ${disabledStyle} text-input-group`}>

            <InputLabel className="transition ml-10"
                        htmlFor={name}
                        error={!!errors[name] && !!touched[name]}>
                {label}
            </InputLabel>

            <PhoneInput
                placeholder="Enter value"
                name={name}
                value={value}
                onChange={onValueChange}
                country={country}
                defaultCountry="IT"
                limitMaxLength
                onBlur={handleBlur(name)}
            />
            {/*<div className="flex h-5 items-end text-red-100 text-xs">
                {isError && getIn(errors, name)}
            </div>*/}
            <ErrorMessage name={name}
                          render={(message) => <FormHelperText
                              error>
                              {message}
                          </FormHelperText>}/>
        </div>
    )
}

PhoneInputField.propTypes = {
    className: PropTypes.string,
    form: PropTypes.any.isRequired,
    field: PropTypes.any.isRequired,
    onChange: PropTypes.func,
    label: PropTypes.string,
    country: PropTypes.string,
    disabled: PropTypes.bool,
    formik: PropTypes.shape({
        handleChange: PropTypes.func.isRequired,
        handleBlur: PropTypes.func.isRequired
    }).isRequired
}

PhoneInputField.defaultProps = {
    className: '',
    label: '',
    onChange: null,
    country: 'IT',
    disabled: false
}

export default PhoneInputField
