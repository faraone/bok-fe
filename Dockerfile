# build environment
FROM node:14.16 as build
WORKDIR /app
ENV PATH /app/node_modules/.bin:$PATH
# copy the package.json to install dependencies
COPY package.json ./
COPY package-lock.json ./
# install dependecies
RUN npm install
COPY . ./
RUN npm run build

# production environment
FROM nginx:stable-alpine
COPY --from=build /app/build /usr/share/nginx/html
COPY nginx/nginx.conf /etc/nginx/conf.d/default.conf
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
