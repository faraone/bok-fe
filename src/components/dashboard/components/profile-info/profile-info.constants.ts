export const PROFILE_INFO_FORM_NAMES = {
    email: 'email',
    fullName: 'fullName',
    icc: 'icc',
    mobile: 'mobile'
}
