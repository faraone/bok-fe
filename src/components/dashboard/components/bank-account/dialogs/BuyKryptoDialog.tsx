import * as React from 'react'
import {useCallback, useMemo, useState} from 'react'
import {
    Button,
    CircularProgress,
    DialogActions,
    DialogContent,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import NumberFormat, {NumberFormatValues} from 'react-number-format'
import {CARD_ID_PATH_VARIABLE, USER_TOKEN} from '../../../../../core/components/utils'
import Cookies from 'js-cookie'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {marketKryptoListSelector} from '../../krypto-marketplace/redux/krypto-marketplace.selectors'
import {cardListSourceOptionsSelector} from '../../card/redux/card.selectors'
import {ICardInfo} from '../../card/redux/card.types'
import {FETCH_GET_CARD_TOKEN_ID, fetchGetCardTokenAction, isFetchGetCardTokenPending} from '../../card/redux/card.fetch'
import {isFetchBuyKryptoPending} from '../../krypto-marketplace/redux/krypto-marketplace.fetch'

interface IBuyKryptoDialog {
    kryptoSymbol: string
}

const BuyKryptoDialog: React.FC<IBuyKryptoDialog> = (props) => {

    const dispatch = useDispatch()

    // props
    const {kryptoSymbol} = props

    // Selector
    const kryptoMarketList = useSelector(marketKryptoListSelector)
    const cardListOptions = useSelector(cardListSourceOptionsSelector)
    const isGetTokenCardPending = useSelector(isFetchGetCardTokenPending)
    const isBuyKryptoPending = useSelector(isFetchBuyKryptoPending)

    const isRequestsPending = useMemo(() => isBuyKryptoPending || isGetTokenCardPending, [isBuyKryptoPending, isGetTokenCardPending])
    const currentPrice = useMemo(() => kryptoMarketList?.find(kyptoInfo => kyptoInfo?.symbol === kryptoSymbol)?.price, [kryptoMarketList, kryptoSymbol])

    // krypto amount local state
    const [cashToInvest, setCashToInvest] = useState(0)
    const [selectedSourceCard, setSelectedSourceCard] = useState<ICardInfo | string>('')

    // Handle change source card
    const handleChange = useCallback((event) => {
        setSelectedSourceCard(event.target.value)
    }, [])

    const kryptoAmountBuying = useMemo(() => currentPrice && cashToInvest ?
        Number(cashToInvest) / Number(currentPrice) + '' : '0', [currentPrice, cashToInvest])

    const buyKrypto = useCallback(() => {

        // fetch request only if cashToInvest > 0
        if (cashToInvest > 0 && !!selectedSourceCard) {

            const token = Cookies.get(USER_TOKEN)

            //@ts-ignore
            selectedSourceCard?.cardId && dispatch(fetchGetCardTokenAction.build(null, FETCH_GET_CARD_TOKEN_ID, {
                [CARD_ID_PATH_VARIABLE]: (selectedSourceCard as ICardInfo)?.cardId
            }, undefined, {
                symbol: kryptoSymbol,
                amount: cashToInvest,
                currencyCode: 'EUR'
            }, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))

            dispatch(closeDialogAction())
        } else {
            dispatch(addWarning({userMessage: 'Please, enter all required fields'}))
        }

    }, [cashToInvest, selectedSourceCard, dispatch, kryptoSymbol])

    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    return (
        <>
            <DialogContent>
                <Typography variant="h6" color="primary" gutterBottom> Buy {kryptoSymbol || ''} </Typography>

                <div className="row mt-2">

                    {/*Buy krypto amount*/}
                    <div className="col-12">

                        <Typography variant="caption">

                            Current price: {`${Number(currentPrice).toLocaleString()} $`}
                        </Typography> <br/>

                        <Typography variant="caption">
                            Converted value: {`${kryptoAmountBuying} ${kryptoSymbol}`}
                        </Typography>

                        <Typography variant="subtitle1" className="mt-2"> Cash to invest </Typography>

                    </div>

                    {/*Amount*/}
                    <div className="col-12">

                        <NumberFormat customInput={TextField}
                                      label="Amount *"
                                      className="mt-2"
                                      fullWidth
                                      value={cashToInvest}
                                      onValueChange={(values: NumberFormatValues) => setCashToInvest(values.floatValue || 0)}
                                      displayType="input"
                                      decimalSeparator=","
                                      fixedDecimalScale
                                      decimalScale={2}
                                      thousandSeparator="."
                                      allowNegative={false}
                                      InputProps={{
                                          endAdornment: '€'
                                      }}
                        />
                    </div>

                    {/*Source card*/}
                    <div className="col-12">
                        <FormControl className="w-100">
                            <InputLabel htmlFor="sourceCard"> Card *</InputLabel>

                            <Select id="sourceCard"
                                    value={selectedSourceCard}
                                    onChange={handleChange}>

                                <MenuItem key="default" value="" disabled>Select symbol</MenuItem>

                                {cardListOptions.map((card, index: number) =>
                                    // @ts-ignore
                                    <MenuItem key={index} value={card.value}>{card.label}</MenuItem>)}

                            </Select>

                        </FormControl>
                    </div>

                </div>
            </DialogContent>

            <DialogActions>
                <div className="d-flex justify-content-between w-100 p-3">
                    <Button variant="outlined"
                            color="default"
                            onClick={closeDialog}> Close </Button>

                    <Button variant="contained"
                            color="primary"
                            disabled={isRequestsPending}
                            onClick={buyKrypto}>
                        {isRequestsPending && <div>
                            <CircularProgress className="ml-2" color="inherit" size={20}/>
                        </div>}
                        BUY
                    </Button>
                </div>
            </DialogActions>
        </>
    )
}

export default BuyKryptoDialog
