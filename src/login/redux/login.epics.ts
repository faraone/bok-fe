import {combineEpics, ofType} from 'redux-observable'
import {FETCH_LOGOUT_ID, fetchChangePasswordAction, fetchLoginAction, fetchLogoutAction} from './login.fetch'
import {Observable} from 'rxjs'
import {mergeMap} from 'rxjs/operators'
import {redirectPathAction, removeTokenCookies, setTokenCookies, USER_TOKEN} from '../../core/components/utils'
import {ILoginResponse} from '../login.types'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {HOME_PAGE_PATH, LOGIN_PATH} from '../../config/routing/router.constants'
import {addError, addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {
    fetchCloseAccountAction,
    logoutAction,
    setAuthStateAction,
    setPasswordResetNeededAction
} from '../../core/profile/redux/profile.fetch'
import Cookies from 'js-cookie'
import {closeDrawerAciont} from '../../components/shared/drawer-home/redux/drawer-home.actions'
import {successRegistrationEpic} from '../../registration/redux/registration.epics'
import {IFailureFetchAction} from 'fetch-with-redux-observable'
import {closeDialogAction} from '../../components/shared/dialog/redux/dialog.actions'

export const successLoginEpic = (action$: Observable<ISuccessFetchAction<ILoginResponse>>) =>
    action$.pipe(
        ofType(fetchLoginAction.successActionType),
        mergeMap((response: ISuccessFetchAction<ILoginResponse>) => {

            // payload value
            const {token, passwordResetNeeded} = response.payload

            // Set cookie
            setTokenCookies(token)

            return [
                // set auth -> true
                setAuthStateAction(true),
                // set passwordResetNeeded
                setPasswordResetNeededAction(passwordResetNeeded),
                // Display snackbar
                addSuccess({userMessage: 'Login success'}),
                // Redirect to home bank page
                redirectPathAction(HOME_PAGE_PATH)
            ]
        })
    )


export const successLogoutEpic = (action$: Observable<ISuccessFetchAction<{ loggedOut: boolean }>>) =>
    action$.pipe(
        ofType(fetchLogoutAction.successActionType),
        mergeMap((response: ISuccessFetchAction<{ loggedOut: boolean }>) => {

            // payload value
            const {loggedOut} = response.payload

            if (loggedOut) {
                removeTokenCookies()
            }

            return loggedOut ? [
                // Clear state
                logoutAction(),
                // Close dialog
                closeDialogAction(),
                // Redirect to login page
                redirectPathAction(LOGIN_PATH)
            ] : [addError({userMessage: 'Error while logging out. Please contact admin'})]
        })
    )

// todo remove
export const failureLogoutEpic = (action$: Observable<IFailureFetchAction>) =>
    action$.pipe(ofType(fetchLogoutAction.failureActionType),
        mergeMap(() => {
            removeTokenCookies()
            return [
                // Clear state
                logoutAction(),
                // Close drawer
                closeDrawerAciont(),
                // Redirect to login page
                redirectPathAction(LOGIN_PATH)]
        })
    )

export const closeAccountSuccessEpic = (action$: Observable<ISuccessFetchAction<any>>) =>
    action$.pipe(ofType(fetchCloseAccountAction.successActionType),
        mergeMap(() => {
            removeTokenCookies()
            return [
                // Clear state
                logoutAction(),
                // Close drawer
                closeDrawerAciont(),
                // Redirect to login page
                redirectPathAction(LOGIN_PATH)]
        })
    )

export const successChangePasswordEpic = (action$: Observable<ISuccessFetchAction<{ changed: boolean }>>) =>
    action$.pipe(
        ofType(fetchChangePasswordAction.successActionType),
        mergeMap((response: ISuccessFetchAction<{ changed: boolean }>) => {

            const isPasswordChanged = response.payload.changed
            const token = Cookies.get(USER_TOKEN)

            return isPasswordChanged ? [
                // Display snackbar
                addSuccess({userMessage: 'Password changed successfully'}),
                // After change password, logout invalidating current token
                fetchLogoutAction.build(null, FETCH_LOGOUT_ID, undefined, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
            ] : [
                // Display snackbar
                addError({userMessage: 'Error changing password'})]
        })
    )

export const userSessionEpics = combineEpics(
    // Registration epic
    successRegistrationEpic,
    // Login epic
    successLoginEpic,
    // Logout epic
    successLogoutEpic,
    // failureLogoutEpic,
    closeAccountSuccessEpic,
    // Close account epic
    successChangePasswordEpic
)

