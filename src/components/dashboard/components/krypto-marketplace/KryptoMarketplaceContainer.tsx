import React, {useCallback, useEffect, useMemo, useState} from 'react'
import VisualizationTable from '../../../shared/visualization-table/VisualizationTable'
import {KRYPTO_TABLE_STRUCTURE} from './krypto-marketplace.constants'
import Chart from '../../../../shared/Chart'
import {useDispatch, useSelector} from 'react-redux'
import {
    FETCH_KRYPTO_HISTORICAL_DATA_ID,
    FETCH_KRYPTO_MARKET_LIST_ID,
    fetchKryptoMarketListAction,
    fetchKyptoHistorticalDataAction
} from './redux/krypto-marketplace.fetch'
import Cookies from 'js-cookie'
import {KRYPTO_SYMBOL_PATH_VARIABLE, USER_TOKEN} from '../../../../core/components/utils'
import {
    chartDataHistoryMarketplaceKryptoSelector,
    currentSymbolMarketplaceSelector,
    marketKryptoListFormattedSelector
} from './redux/krypto-marketplace.selectors'
import {IKrytoInfo} from './redux/krypto-marketplace.types'
import {Tooltip, Typography} from '@material-ui/core'
import ReplayIcon from '@material-ui/icons/Replay'
import {KeyboardDatePicker} from '@material-ui/pickers'
import moment from 'moment'
import {makeStyles} from '@material-ui/core/styles'
import BuyKryptoButton from '../actions/BuyKryptoButton'
import SellKryptoButton from '../actions/SellKryptoButton'
import {kryptoSymbolWalletListSelector} from '../krypto-wallets/redux/krypto-wallets.selectors'
import {useKryptoDataHook} from '../../../home/custom-hook/useKryptoDataHook'
import {Skeleton} from '@material-ui/lab'

interface IKryptoMarketplaceContainer {
}

const useStyles = makeStyles(() => ({
    datePicker: {padding: 0}
}))

const KryptoMarketplaceContainer: React.FC<IKryptoMarketplaceContainer> = () => {

    const dispatch = useDispatch()
    const classes = useStyles()

    // Call custom hook with shared fetch (marketplace and wallets)
    const {isRequestsPending} = useKryptoDataHook()

    // Selector
    const marketKryptoList = useSelector(marketKryptoListFormattedSelector)
    const chartData = useSelector(chartDataHistoryMarketplaceKryptoSelector)
    const currentSymbol = useSelector(currentSymbolMarketplaceSelector)
    const kryptoSymbolWalletList = useSelector(kryptoSymbolWalletListSelector)

    // Local state
    const [startDateChart, setStartDateChart] = useState(moment().subtract(1, 'days').startOf('day'))
    const [endDateChart, setEndDateChart] = useState(moment().startOf('day'))

    // Check if market selected krypto is salable from user
    const isCurrentKryptoSalable = useMemo(() => currentSymbol && kryptoSymbolWalletList?.includes(currentSymbol), [currentSymbol, kryptoSymbolWalletList])

    // Get selected krypto detail
    const kryptoDetail = useMemo(() => marketKryptoList?.find(krypto => krypto?.symbol === currentSymbol), [currentSymbol, marketKryptoList])

    // shared between did mount useEffect and update action button
    const fetchKryptoData = useCallback(() => {
        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchKryptoMarketListAction.build(null, FETCH_KRYPTO_MARKET_LIST_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
    }, [dispatch])

    const onKryptoTableRowClick = useCallback((symbol: string) => {

        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchKyptoHistorticalDataAction.build(null, FETCH_KRYPTO_HISTORICAL_DATA_ID, {
            [KRYPTO_SYMBOL_PATH_VARIABLE]: symbol
        }, {
            startDate: startDateChart.startOf('day').toISOString(),
            endDate: endDateChart.endOf('day').toISOString()
        }, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }

        }))
    }, [dispatch, endDateChart, startDateChart])

    // fetch historical data krypto list first element
    useEffect(() => {
        if (marketKryptoList && marketKryptoList?.length > 0 && !chartData.length) {
            onKryptoTableRowClick(marketKryptoList[0]?.symbol)
        }
    }, [chartData, marketKryptoList, onKryptoTableRowClick])

    // On start or date change, update current symbol history
    useEffect(() => {
        currentSymbol && onKryptoTableRowClick(currentSymbol)

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [startDateChart, endDateChart])

    return <>
        <div className="col flex-grow-1 overflow-auto mt-3">
            <Typography variant="h5" color="primary" gutterBottom>
                Krypto marketplace
            </Typography>
            {isRequestsPending || !currentSymbol ?
                /*Spinner skeleton*/
                <div className="col flex-grow-1 overflow-auto mt-3">
                    <div className="row justify-content-between">
                        <Skeleton className="col-4" height={50} variant="text"/>
                        <Skeleton className="col-4" variant="text"/>
                    </div>
                </div>
                : <>
                    <div className="col flex-grow-1 overflow-auto mt-3">

                        {/*KRYPTO TITLE - with actions*/}
                        <div className="row">

                            {/*Current krypto symbol*/}
                            <div className="col-6 mt-3">
                                <Typography variant="h5">
                                    {kryptoDetail?.name + ' ' + currentSymbol}
                                </Typography>
                                <Typography variant="body1">
                                    {kryptoDetail?.price + ' $'}
                                </Typography>
                            </div>

                            <div className="col-6 d-flex justify-content-end">

                                {/*Buy krypto*/}
                                {currentSymbol && <BuyKryptoButton kryptoSymbol={currentSymbol}/>}

                                {/*Sell krypto*/}
                                {isCurrentKryptoSalable && currentSymbol &&
                                <SellKryptoButton kryptoSymbol={currentSymbol}/>}

                            </div>


                        </div>

                        {/*Wallet chart filter*/}
                        <div className="row justify-content-end">

                            {/*Start date*/}
                            <div className="col-6 col-sm-4 col-md-3">
                                <KeyboardDatePicker
                                    size="small"
                                    variant="inline"
                                    format="DD/MM/YYYY"
                                    margin="normal"
                                    label="Start date"
                                    value={startDateChart}
                                    onChange={newStartDate => newStartDate && setStartDateChart(newStartDate)}
                                    disableFuture
                                    autoOk
                                    KeyboardButtonProps={{className: classes.datePicker}}/>
                            </div>

                            {/*End date*/}
                            <div className="col-6 col-sm-4 col-md-3">
                                <KeyboardDatePicker
                                    size="small"
                                    variant="inline"
                                    format="DD/MM/YYYY"
                                    margin="normal"
                                    label="End date"
                                    value={endDateChart}
                                    onChange={newEndDate => newEndDate && setEndDateChart(newEndDate)}
                                    disableFuture
                                    autoOk
                                    KeyboardButtonProps={{className: classes.datePicker}}/>
                            </div>
                        </div>


                        {/*Kallet chart*/}
                        <div className="row">
                            <div className="col-12 justify-content-center" style={{height: 200}}>
                                <Chart title={<Typography variant="caption"> Historical data</Typography>}
                                       data={chartData}/>
                            </div>
                        </div>

                        {/*Krypto list*/}
                        <div className="row mt-3">
                            <div className="col-12">
                                <VisualizationTable
                                    tableTitle={<>
                                        List
                                        <Tooltip title="Update" placement="top">
                                            <ReplayIcon onClick={fetchKryptoData}
                                                        className="ml-2"
                                                        style={{cursor: 'pointer'}}
                                                        fontSize="small"/>
                                        </Tooltip>
                                    </>}
                                    results={marketKryptoList}
                                    tableStructure={KRYPTO_TABLE_STRUCTURE}
                                    onRowClick={(row: IKrytoInfo) => onKryptoTableRowClick(row?.symbol)}
                                    rowCPointer
                                />
                            </div>
                        </div>
                    </div>
                </>}

        </div>
    </>
}

export default KryptoMarketplaceContainer
