import * as React from 'react'
import {useCallback, useMemo} from 'react'
import {Button, CircularProgress, DialogContent, DialogTitle} from '@material-ui/core'
import {Field, Form, Formik} from 'formik'
import {KRYPTO_TRANSFER_FORM_NAME, KRYPTO_TRANSFER_INIT_VALUES} from '../krypto-wallets.constants'
import {Validators} from '../../../../../shared/form-validators'
import {TextField as FormikTextField} from 'formik-material-ui'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {currentWalletDetailSelector} from '../redux/krypto-wallets.selectors'
import {FormikNumberFormat} from '../../../../../shared/FormikNumberFormat'
import {
    FETCH_TRANSFER_KRYPTO_ID,
    fetchValidateAddressAction,
    isFetchTransferKryptoPending,
    isFetchValidateAddressPending
} from '../redux/krypto-wallets.fetch'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'
import {FormikHelpers} from 'formik/dist/types'

interface ITransferKryptoDialog {
}

const TransferKryptoDialog: React.FC<ITransferKryptoDialog> = () => {

    const dispatch = useDispatch()

    // Selector
    const currentWallet = useSelector(currentWalletDetailSelector)
    const isFetchTransferPending = useSelector(isFetchTransferKryptoPending)
    const isFetchValidatePending = useSelector(isFetchValidateAddressPending)

    const isRequestsPending = useMemo(() => isFetchTransferPending || isFetchValidatePending, [isFetchValidatePending, isFetchTransferPending])

    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    // Handle submit form
    const handleSubmit = useCallback((values, formikHelper: FormikHelpers<any>) => {

        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchValidateAddressAction.build({
            address: values?.destination,
            symbol: currentWallet?.symbol
        }, FETCH_TRANSFER_KRYPTO_ID, undefined, undefined, {
            source: currentWallet?.address,
            amount: values?.amount
        }, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

        formikHelper.setSubmitting(false)

    }, [currentWallet?.address, currentWallet?.symbol, dispatch])

    return (
        <>
            <DialogTitle>
                Transfer krypto
            </DialogTitle>

            <DialogContent>
                <Formik
                    initialValues={KRYPTO_TRANSFER_INIT_VALUES}
                    onSubmit={handleSubmit}
                    validateOnBlur={false}
                    validateOnChange={false}
                >
                    {() => <Form>
                        <div className="row align-items-center">

                            {/*Amount*/}
                            <div className="col-12">
                                <Field
                                    component={FormikNumberFormat}
                                    name={KRYPTO_TRANSFER_FORM_NAME.amount}
                                    fullWidth
                                    numberFormatProps={{
                                        label: 'Amount',
                                        decimalSeparator: ',',
                                        thousandSeparator: '.',
                                        decimalScale: 2,
                                        fixedDecimalScale: true,
                                        allowNegative: false,
                                        InputProps: {
                                            endAdornment: '€'
                                        }
                                    }}
                                    validate={Validators.required}
                                />
                            </div>

                            {/*Destination wallet address*/}
                            <div className="col-12">
                                <Field
                                    component={FormikTextField}
                                    name={KRYPTO_TRANSFER_FORM_NAME.destination}
                                    label="Wallet address destination"
                                    fullWidth
                                    required
                                    validate={Validators.required}
                                />
                            </div>

                            {/*Source*/}
                            {/* <div className="col-12">
                                <FormControl className="w-100">
                                    <InputLabel error={!!errors.source && !!touched.source} htmlFor="transferSource">
                                        Wallet source </InputLabel>
                                    <Field id="transferSource"
                                           component={Select}
                                           name={KRYPTO_TRANSFER_FORM_NAME.source}
                                           fullWidth
                                           validate={Validators.required}
                                           error={!!errors.source && !!touched.source}
                                    >

                                        <MenuItem key="default" value="" disabled>Select source</MenuItem>
                                        {walletListOptions.map((source, index: number) =>
                                            // @ts-ignore
                                            <MenuItem key={index} value={source.value}>{source.label}</MenuItem>)}
                                    </Field>
                                    <ErrorMessage name={KRYPTO_TRANSFER_FORM_NAME.source}
                                                  render={(message) => <FormHelperText error>
                                                      {message}
                                                  </FormHelperText>}
                                    />
                                </FormControl>
                                {isFetchCardListPending && <div>
                                    <CircularProgress className="ml-2" color="primary" size={20}/>
                                </div>}
                            </div>*/}

                            {/*Symbol*/}
                            {/*<div className="col-12">
                                <FormControl className="w-100">
                                    <InputLabel error={!!errors.symbol && !!touched.symbol} htmlFor="kryptoSymbol">
                                        Krypto </InputLabel>
                                    <Field id="kryptoSymbol"
                                           component={Select}
                                           name={KRYPTO_TRANSFER_FORM_NAME.symbol}
                                           fullWidth
                                           validate={Validators.required}
                                           error={!!errors.symbol && !!touched.symbol}
                                    >
                                        <MenuItem key="default" value="" disabled>Select symbol</MenuItem>

                                        {kryptoSymbolOptions.map((symbol, index: number) =>
                                            // @ts-ignore
                                            <MenuItem key={index} value={symbol.value}>{symbol.label}</MenuItem>)}
                                    </Field>
                                    <ErrorMessage name={KRYPTO_TRANSFER_FORM_NAME.symbol}
                                                  render={(message) => <FormHelperText error>
                                                      {message}
                                                  </FormHelperText>}
                                    />
                                </FormControl>

                            </div>*/}
                        </div>

                        <div className="row">
                            <div className="d-flex justify-content-between w-100 p-3">
                                <Button variant="outlined"
                                        color="default"
                                        onClick={closeDialog}> Close </Button>

                                <Button variant="contained"
                                        color="primary"
                                        type="submit"
                                        disabled={isRequestsPending}>
                                    {
                                        isRequestsPending &&
                                        <CircularProgress color="inherit" size={20} className="mr-2"/>
                                    }
                                    Transfer
                                </Button>
                            </div>
                        </div>
                    </Form>
                    }
                </Formik>
            </DialogContent>

        </>
    )
}

export default TransferKryptoDialog

