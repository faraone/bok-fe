import React, {useCallback} from 'react'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import {Typography} from '@material-ui/core'

// Define single table column definition
export interface ITableStructure {
    key: string
    label: string
}

interface IVisualizationTable {
    results: any[]
    tableTitle: string | React.ReactElement
    tableStructure: ITableStructure[]
    tableSpinnerPlaceholder?: boolean
    onRowClick?: (...args: any) => any
    rowCPointer?: boolean
}

const VisualizationTable: React.FC<IVisualizationTable> = (props) => {

    const {
        results,
        tableStructure,
        tableTitle,
        tableSpinnerPlaceholder = false,
        onRowClick,
        rowCPointer = false
    } = props

    const getTableCells = useCallback((currentResultRow: any) => {
        return tableStructure.map(({key}, index) => {
            return <TableCell key={index}>{currentResultRow[key]}</TableCell>
        })
    }, [tableStructure])

    return (<div className="overflow-auto mt-3">
            <Typography variant="h6" color="primary">{tableTitle || ''}</Typography>
            {
                !!results.length && Array.isArray(tableStructure) && !tableSpinnerPlaceholder ? <>

                    <Table size="small">
                        <TableHead>
                            <TableRow>
                                {
                                    tableStructure?.map(({label}, index) => <TableCell
                                        key={'cell' + index}>{label}</TableCell>)
                                }
                            </TableRow>
                        </TableHead>

                        <TableBody>
                            {results.map((currentResultRow: any, index: number) => (
                                <TableRow key={index}
                                          className={`${rowCPointer ? 'c-pointer' : ''}`}
                                          onClick={(e: any) => onRowClick ? onRowClick(currentResultRow, e) : null}>
                                    {
                                        getTableCells(currentResultRow)
                                    }
                                </TableRow>
                            ))}

                        </TableBody>
                    </Table> </> : <div className="d-flex justify-content-around align-items-center">
                    <Typography variant="body1"
                                className="d-flex align-items-center"
                                color="textSecondary">No results</Typography>
                </div>
            }

        </div>
    )
}

export default VisualizationTable
