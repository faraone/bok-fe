import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {FETCH_REGISTER_API} from '../../core/fetch/fetch.constants'

// Registration fetch action
export const FETCH_REGISTRATION = "FETCH_REGISTRATION"
export const FETCH_REGISTRATION_ID = "FETCH_REGISTRATION_ID"

export const fetchRegistrationAction = fetchActionFactory(FETCH_REGISTER_API, FETCH_REGISTRATION)
export const isFetchRegistrationPending = isRequestInPending(fetchRegistrationAction.pendingActionTypeWithSpinner, FETCH_REGISTRATION_ID)
