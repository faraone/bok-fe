// bank account fetch action
import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {
    FETCH_CREATE_WALLET_API,
    FETCH_DELETE_WALLET_API,
    FETCH_KRYPTO_WALLET_INFO_API,
    FETCH_TRANSFER_SEND_SYMBOL_API,
    FETCH_VALIDATE_WALLET_ADDRESS_AND_SYMBOL_API,
    FETCH_WALLET_LIST_API
} from '../../../../../core/fetch/fetch.constants'

// Fetch action wallet list
export const FETCH_WALLETS_LIST = 'FETCH_WALLETS_LIST'
export const FETCH_WALLETS_LIST_ID = 'FETCH_WALLETS_LIST_ID'

export const fetchWalletsListInfoAction = fetchActionFactory(FETCH_WALLET_LIST_API, FETCH_WALLETS_LIST)
export const isFetchWalletsListPending = isRequestInPending(fetchWalletsListInfoAction.pendingActionTypeWithSpinner, FETCH_WALLETS_LIST_ID)

// Fetch action wallet info
export const FETCH_WALLET_INFO = 'FETCH_WALLET_INFO'
export const FETCH_WALLET_INFO_ID = 'FETCH_WALLET_INFO_ID'

export const fetchWalletInfoAction = fetchActionFactory(FETCH_KRYPTO_WALLET_INFO_API, FETCH_WALLET_INFO)
export const isFetchWalletInfoPending = isRequestInPending(fetchWalletInfoAction.pendingActionTypeWithSpinner, FETCH_WALLET_INFO_ID)

// Fetch action delete wallet
export const FETCH_DELETE_WALLET = 'FETCH_DELETE_WALLET'
export const FETCH_DELETE_WALLET_ID = 'FETCH_DELETE_WALLET_ID'

export const fetchDeleteWalletAction = fetchActionFactory(FETCH_DELETE_WALLET_API, FETCH_DELETE_WALLET)
export const isFetchDeleteWalletPending = isRequestInPending(fetchDeleteWalletAction.pendingActionTypeWithSpinner, FETCH_DELETE_WALLET_ID)

// Fetch action create wallet
export const FETCH_CREATE_WALLET = 'FETCH_CREATE_WALLET'
export const FETCH_CREATE_WALLET_ID = 'FETCH_CREATE_WALLET_ID'

export const fetchCreateWalletAction = fetchActionFactory(FETCH_CREATE_WALLET_API, FETCH_CREATE_WALLET)
export const isFetchCreateWalletPending = isRequestInPending(fetchCreateWalletAction.pendingActionTypeWithSpinner, FETCH_CREATE_WALLET_ID)

// Fetch transfer krypto
export const FETCH_TRANSFER_KRYPTO = 'FETCH_TRANSFER_KRYPTO'
export const FETCH_TRANSFER_KRYPTO_ID = 'FETCH_TRANSFER_KRYPTO_ID'

export const fetchTransferKryptoAction = fetchActionFactory(FETCH_TRANSFER_SEND_SYMBOL_API, FETCH_TRANSFER_KRYPTO)
export const isFetchTransferKryptoPending = isRequestInPending(fetchTransferKryptoAction.pendingActionTypeWithSpinner, FETCH_TRANSFER_KRYPTO_ID)

export const FETCH_VALIDATE_ADDRESS = 'FETCH_VALIDATE_ADDRESS'
export const FETCH_VALIDATE_ADDRESS_ID = 'FETCH_VALIDATE_ADDRESS_ID'
export const fetchValidateAddressAction = fetchActionFactory(FETCH_VALIDATE_WALLET_ADDRESS_AND_SYMBOL_API, FETCH_VALIDATE_ADDRESS)
export const isFetchValidateAddressPending = isRequestInPending(fetchValidateAddressAction.pendingActionTypeWithSpinner, FETCH_VALIDATE_ADDRESS_ID)

export const CLEAR_CURRENT_WALLET_SYMBOL = 'CLEAR_CURRENT_WALLET_SYMBOL'
export const clearCurrentWalletSymbolAction = () => ({type: CLEAR_CURRENT_WALLET_SYMBOL})
