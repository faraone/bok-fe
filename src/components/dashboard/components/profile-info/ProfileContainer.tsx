import * as React from 'react'
import {useCallback, useState} from 'react'
import {Container, TextField, Typography} from '@material-ui/core'
import {useDispatch, useSelector} from 'react-redux'
import {profileInfoSelector} from '../../../../core/profile/redux/profile.reducer'
import ChangePasswordFormContainer from '../change-password/ChangePasswordFormContainer'
import Button from '@material-ui/core/Button'
import {openDialogAction} from '../../../shared/dialog/redux/dialog.actions'
import {DIALOG_NAMES} from '../../../shared/dialog/redux/dialog.reducer'

interface IProfileInfoContainer {
}


const ProfileInfoContainer: React.FC<IProfileInfoContainer> = () => {

    const dispatch = useDispatch()

    // Selector
    const profileInfo = useSelector(profileInfoSelector)

    // Local state display form to change password
    const [displayChangePassword, setDisplayChangePassword] = useState<boolean>(false)

    const toggleDisplayChangePassword = useCallback(() => {
        setDisplayChangePassword(!displayChangePassword)
    }, [displayChangePassword])

    const openConfirmDeleteAccountDialog = useCallback(() => {
        dispatch(openDialogAction({
            name: DIALOG_NAMES.CONFIRM_CLOSE_ACCOUNT
        }))
    }, [dispatch])

    return <div className="col flex-grow-1 overflow-auto mt-3">

        {/*Title*/}
        <div className="row justify-content-between">
            <div className="col">
                <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Profile info
                </Typography>
            </div>

            <div className="col d-flex justify-content-end">
                <Button
                    color="primary"
                    onClick={openConfirmDeleteAccountDialog}>
                    Delete account
                </Button>
            </div>
        </div>

        <Container fixed maxWidth="sm">

            <div className="row align-items-center">

                {/*Fullname info*/}
                <div className="col-12 col-sm-6 mt-1">
                    <TextField
                        label="Fullname"
                        value={profileInfo?.fullName}
                        fullWidth
                        InputProps={{
                            readOnly: true
                        }}
                    />
                </div>

                {/*Email info*/}
                <div className="col-12 col-sm-6 mt-1">
                    <TextField
                        label="Email"
                        value={profileInfo?.email}
                        fullWidth
                        InputProps={{
                            readOnly: true
                        }}
                    />
                </div>

                {/*ICC info*/}
                <div className="col-4 col-sm-2 mt-2">
                    <TextField
                        label="ICC"
                        fullWidth
                        value={profileInfo?.icc}
                        InputProps={{
                            readOnly: true
                        }}
                    />
                </div>

                {/*Mobile number info*/}
                <div className="col-8 col-sm-4 mt-1">
                    <TextField
                        label="Mobile number"
                        fullWidth
                        value={profileInfo?.mobile}
                        InputProps={{
                            readOnly: true
                        }}
                    />
                </div>
            </div>

            <div className="row">
                <div className="col-12 mt-3 justify-content-end">
                    <div style={{cursor: 'pointer'}} onClick={toggleDisplayChangePassword}>
                        <Typography variant="subtitle1">Change password</Typography>
                    </div>
                </div>
            </div>

            {/*Change password*/}
            {displayChangePassword && <ChangePasswordFormContainer/>}

        </Container>

    </div>

}

export default ProfileInfoContainer

