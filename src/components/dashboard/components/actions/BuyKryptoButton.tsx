import React, {useCallback} from 'react'
import {Button, CircularProgress} from '@material-ui/core'
import {useDispatch, useSelector} from 'react-redux'
import {isFetchBuyKryptoPending} from '../krypto-marketplace/redux/krypto-marketplace.fetch'
import {openDialogAction} from '../../../shared/dialog/redux/dialog.actions'
import {DIALOG_NAMES} from '../../../shared/dialog/redux/dialog.reducer'
import AddShoppingCartOutlinedIcon from '@material-ui/icons/AddShoppingCartOutlined'
import {cardListSourceOptionsSelector} from '../card/redux/card.selectors'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'


interface IBuyKryptoButton {
    kryptoSymbol: string
}

/**
 * Shared button BUY - open buy krypto dialog
 * */
const BuyKryptoButton: React.FC<IBuyKryptoButton> = (props) => {

    const {kryptoSymbol} = props

    const dispatch = useDispatch()

    // Selector
    const isBuyKryptoPending = useSelector(isFetchBuyKryptoPending)
    const cardListOptions = useSelector(cardListSourceOptionsSelector)

    const openDialogBuyKrypto = useCallback(() => {
        cardListOptions?.length > 0 ? dispatch(openDialogAction({
            name: DIALOG_NAMES.BUY_KRYPTO,
            meta: {
                kryptoSymbol

            }
        })) : dispatch(addWarning({userMessage: 'There isn\'t card available for the transfer. Please create one in Bank account page.'}))
    }, [cardListOptions, dispatch, kryptoSymbol])

    return <div className="d-flex align-items-center">
        {kryptoSymbol &&
        <Button style={{cursor: 'pointer'}}
                className="mt-2"
                color="primary"
                endIcon={
                    isBuyKryptoPending ? <CircularProgress size={20}
                                                           className="ml-2"
                                                           color={isBuyKryptoPending ? 'primary' : 'secondary'}/> :
                        <AddShoppingCartOutlinedIcon className="mr-3 c-pointer" color="primary"/>}
                onClick={() => openDialogBuyKrypto()}>Buy</Button>}
    </div>
}

export default BuyKryptoButton
