import {createSelector} from 'reselect'
import {IRootState} from '../../../../../reducer'
import {IBaseMarketState, IHistoricalDataResponse, IKrytoInfo} from './krypto-marketplace.types'
import moment from 'moment'
import {createChartData, DATE_TIME_CUSTOM_FORMAT} from '../../../../../core/components/utils'
import {ILabelValueOption} from '../../../dashboard.types'


// Base market selector
export const baseMarketSelector = (state: IRootState): IBaseMarketState => state?.market

export const marketKryptoListSelector = createSelector(baseMarketSelector,
    (market: IBaseMarketState): IKrytoInfo[] | null => market?.kryptoList || null
)

export const marketKryptoListFormattedSelector = createSelector(baseMarketSelector,
    (market: IBaseMarketState): IKrytoInfo[] => market?.kryptoList ? market?.kryptoList?.map((krypto) => ({
        ...krypto,
        updateTimestamp: moment(krypto?.updateTimestamp).format('DD/MM/YYYY HH:mm:ss'),
        price: krypto?.price?.toLocaleString(undefined, {minimumFractionDigits: 2})
    })) : []
)

export const chartDataHistoryMarketplaceKryptoSelector = createSelector(baseMarketSelector,
    (market: IBaseMarketState) => market?.currentHistoricalData?.history?.map((data) => {
        const time = moment(data?.instant).format(DATE_TIME_CUSTOM_FORMAT)
        return createChartData(time, data?.price)
    }) || []
)

export const currentSymbolHistoryMarketplaceSelector = createSelector(baseMarketSelector,
    (market: IBaseMarketState): IHistoricalDataResponse | null => market?.currentHistoricalData
)

export const currentSymbolMarketplaceSelector = createSelector(baseMarketSelector,
    (market: IBaseMarketState) => market?.currentHistoricalData?.symbol
)

// Available krypto symbol
export const kryptoSymbolListMarketplaceSelector = createSelector(baseMarketSelector,
    (market: IBaseMarketState): ILabelValueOption<string>[] => market?.kryptoList ?
        market?.kryptoList?.map(krypto => ({
            value: krypto.symbol,
            label: krypto.symbol + ' ' + krypto.name
        })) : []
)
