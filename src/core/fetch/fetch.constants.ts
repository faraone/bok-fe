import {HttpMethods, IRequestModel} from 'fetch-with-redux-observable'
import {CARD_ID_PATH_VARIABLE, CARD_STATUS_PATH_VARIABLE, KRYPTO_SYMBOL_PATH_VARIABLE} from '../components/utils'

const MARKET_BASE_PATH = '/market'
const WALLET_BASE_PATH = '/wallet'
const KRYPTO_BASE_PATH = '/krypto'
const BANK_BASE_PATH = '/bank'
const TRANSFER_BASE_PATH = '/transfer'
const TRANSACTION_BASE_PATH = '/transaction'
const CARD_BASE_PATH = '/card'

/**
 * @description registration fetch
 * */
export const FETCH_REGISTER_API: IRequestModel = {
    url: `/register`,
    method: HttpMethods.POST,
    headers: {
        'Content-Type': 'application/json'

    }
}

/**
 * @description login fetch
 * */
export const FETCH_LOGIN_API: IRequestModel = {
    url: `/login`,
    method: HttpMethods.POST,
    headers: {
        'Content-Type': 'application/json'
    }
}

/**
 * @description close account
 * */
export const FETCH_CLOSE_ACCOUNT_API: IRequestModel = {
    url: `/closeAccount`,
    method: HttpMethods.DELETE
}

/**
 * @description change password
 * */
export const FETCH_CHANGE_PASSWORD_API: IRequestModel = {
    url: `/changePassword`,
    method: HttpMethods.POST,
    headers: {
        'Content-Type': 'application/json'
    }
}

/**
 * @description logout fetch
 * */
export const FETCH_LOGOUT_API: IRequestModel = {
    url: `/accountLogout`,
    method: HttpMethods.GET,
    headers: {
        'Content-Type': 'application/json'
    }
}

/**
 * @description reset password fetch
 * */
export const FETCH_RESET_PASSWORD_API: IRequestModel = {
    url: `/resetPassword`,
    method: HttpMethods.POST,
    headers: {
        'Content-Type': 'application/json'
    }
}
/**
 * @description check if reset password needed
 * */
export const FETCH_CHECK_RESET_PASSWORD_NEEDED_API: IRequestModel = {
    url: `/passwordResetNeeded`,
    method: HttpMethods.POST,
    headers: {
        'Content-Type': 'application/json'
    }
}

/**
 * @description token info
 * */
export const FETCH_TOKEN_INFO_API: IRequestModel = {
    url: `/tokenInfo`,
    method: HttpMethods.POST,
    headers: {
        'Content-Type': 'application/json'
    }
}

/**
 * @description profile info
 * */
export const FETCH_PROFILE_INFO_API: IRequestModel = {
    url: `${BANK_BASE_PATH}/account/profileInfo`,
    method: HttpMethods.GET
}

/**
 * @description profile info
 * */
export const FETCH_LAST_ACCESS_INFO_API: IRequestModel = {
    url: `/lastAccessInfo`,
    method: HttpMethods.GET
}

/**
 * @description bank account info
 * */
export const FETCH_BANK_ACCOUNT_INFO_API: IRequestModel = {
    url: `${BANK_BASE_PATH}/bankAccount/bankAccountInfo`,
    method: HttpMethods.GET
}

/**
 * @description check payment amount
 * */
export const FETCH_CHECK_PAYMENT_AMOUNT_API: IRequestModel = {
    url: `${BANK_BASE_PATH}/bankAccount/checkPaymentAmount`,
    method: HttpMethods.POST
}

/**
 * @description card info by id
 * */
export const FETCH_CARD_BY_ID_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.GET
}

/**
 * @description get all user cards
 * */
export const FETCH_GET_ALL_USER_CARDS_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/allCards`,
    method: HttpMethods.GET
}

/**
 * @description create card
 * */
export const FETCH_CREATE_CARD_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/create`,
    method: HttpMethods.POST
}

/**
 * @description delete card
 * */
export const FETCH_DELETE_CARD_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/delete/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.POST
}

/**
 * @description get CVV card
 * */
export const FETCH_GET_CVV_CARD_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/getCvv/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.POST
}

/**
 * @description get plain PAN card
 * */
export const FETCH_GET_PLAIN_PAN_CARD_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/getPlainPAN/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.POST
}

/**
 * @description get card token
 * */
export const FETCH_GET_CARD_TOKEN_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/getCardToken/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.GET
}

/**
 * @description get card PIN
 * */
export const FETCH_GET_CARD_PIN_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/getPIN/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.GET
}

/**
 * @description request change card status
 * */
export const FETCH_CHANGE_CARD_STATUS_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/changeCardStatus/{${CARD_ID_PATH_VARIABLE}}/{${CARD_STATUS_PATH_VARIABLE}}`,
    method: HttpMethods.POST
}

/**
 * @description request activate card
 * */
export const FETCH_ACTIVATE_CARD_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${CARD_BASE_PATH}/activation/{${CARD_ID_PATH_VARIABLE}}`,
    method: HttpMethods.POST
}

/**
 * @description get all bank account transaction
 * */
export const FETCH_ALL_BANK_TRANSACTION_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${TRANSACTION_BASE_PATH}/allTransaction`,
    method: HttpMethods.GET
}

/**
 * @description make a bank transfer
 * */
export const FETCH_WIRE_TRANSFER_API: IRequestModel = {
    url: `${BANK_BASE_PATH}${TRANSACTION_BASE_PATH}/wireTransfer`,
    method: HttpMethods.POST
}

/**
 * @description get wallet list
 * */
export const FETCH_WALLET_LIST_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${WALLET_BASE_PATH}/list`,
    method: HttpMethods.GET
}

/**
 * @description get krypto list
 * */
export const FETCH_KRYPTO_WALLET_INFO_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${WALLET_BASE_PATH}/{${KRYPTO_SYMBOL_PATH_VARIABLE}}/info`,
    method: HttpMethods.GET
}

/**
 * @description delete wallet by symbol
 * */
export const FETCH_DELETE_WALLET_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${WALLET_BASE_PATH}/delete`,
    method: HttpMethods.POST
}

/**
 * @description create wallet by symbol
 * */
export const FETCH_CREATE_WALLET_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${WALLET_BASE_PATH}/create`,
    method: HttpMethods.POST
}

/**
 * @description get krypto info by symbols
 * */
export const FETCH_MARKET_KRYPTO_LIST_INFO_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/list`,
    method: HttpMethods.GET
}

/**
 * @description get krypto price
 * */
export const FETCH_KRYPTO_PRICE_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/{${KRYPTO_SYMBOL_PATH_VARIABLE}}/price`,
    method: HttpMethods.GET
}

/**
 * @description buy krypto
 * */
export const FETCH_BUY_KRYPTO_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/buy`,
    method: HttpMethods.POST
}

/**
 * @description get historical data market
 * */
export const FETCH_KRYPTO_HISTORICAL_DATA_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/{${KRYPTO_SYMBOL_PATH_VARIABLE}}/history`,
    method: HttpMethods.GET
}

/**
 * @description get krypto market infos
 * */
export const FETCH_KRYPTO_MARKET_INFOS_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/infos`,
    method: HttpMethods.GET
}

/**
 * @description get krypto prices
 * */
export const FETCH_MARKET_KRYPTO_PRICES_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/prices`,
    method: HttpMethods.GET
}

/**
 * @description sell krypto
 * */
export const FETCH_SELL_KRYPTO_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/sell`,
    method: HttpMethods.POST
}

/**
 * @description get krypto wallet info by symbols
 * */
export const FETCH_KRYPTO_INFO_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${MARKET_BASE_PATH}/{${KRYPTO_SYMBOL_PATH_VARIABLE}}/info`,
    method: HttpMethods.GET
}

/**
 * @description transfer krypto from wallet
 * */
export const FETCH_TRANSFER_SEND_SYMBOL_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${TRANSFER_BASE_PATH}/send`,
    method: HttpMethods.POST
}

/**
 * @description validate wallet address (transfer)
 * */
export const FETCH_VALIDATE_WALLET_ADDRESS_AND_SYMBOL_API: IRequestModel = {
    url: `${KRYPTO_BASE_PATH}${WALLET_BASE_PATH}/validateAddress`,
    method: HttpMethods.POST
}

