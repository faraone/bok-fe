import {NEVER, Observable} from 'rxjs'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {fetchBuyKryptoAction, fetchKryptoMarketListAction, fetchSellKyptoAction} from './krypto-marketplace.fetch'
import {ofType} from 'redux-observable'
import {IKrytoInfo} from './krypto-marketplace.types'
import {map, mergeMap} from 'rxjs/operators'
import {clearHistoricalDataAction} from './krypto-marketplace.reducers'
import {addError, addSuccess, addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {EActivityStatus, IActivity} from '../../../dashboard.types'

export const clearHistoricalDataOnKryptoListUpdateEpic = (action$: Observable<ISuccessFetchAction<{ kryptos: IKrytoInfo[] }>>) =>
    action$.pipe(
        ofType(fetchKryptoMarketListAction.successActionType),
        map((action: ISuccessFetchAction<{ kryptos: IKrytoInfo[] }>) => action.payload),
        mergeMap((payload) => payload?.kryptos ? [clearHistoricalDataAction()] : NEVER)
    )

export const buyKryptoSuccessEpic = (action$: Observable<ISuccessFetchAction<any>>) =>
    action$.pipe(
        ofType(fetchBuyKryptoAction.successActionType,
            fetchSellKyptoAction.successActionType),
        mergeMap((action) => {

            const status = (action?.payload as IActivity)?.status

            let snackbarMessage: any

            switch (status) {
                case EActivityStatus.AUTHORIZED:
                    snackbarMessage = [addSuccess({userMessage: 'Krypto activity authorized'})]
                    break
                case EActivityStatus.DECLINED:
                    snackbarMessage = [addError({userMessage: 'Krypto activity declined'})]
                    break
                case EActivityStatus.PENDING:
                    snackbarMessage = [addWarning({userMessage: 'Krypto activity in pending. Update activity list later'})]
                    break
                case EActivityStatus.SETTLED:
                    snackbarMessage = [addWarning({userMessage: 'Krypto activity authorized'})]
                    break
                default:
                    snackbarMessage = NEVER
            }

            return snackbarMessage
        })
    )
