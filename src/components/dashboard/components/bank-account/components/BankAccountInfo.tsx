import React, {useCallback, useState} from 'react'
import Typography from '@material-ui/core/Typography'
import VisibilityIcon from '@material-ui/icons/Visibility'
import VisibilityOffIcon from '@material-ui/icons/VisibilityOff'
import Button from '@material-ui/core/Button'
import {useDispatch, useSelector} from 'react-redux'
import {openDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import AddIcon from '@material-ui/icons/Add'
import {DIALOG_NAMES} from '../../../../shared/dialog/redux/dialog.reducer'
import {bankAccountSelector} from '../redux/bank-account.selectors'
import {MAP_CURRENCY} from '../../../../../core/components/utils'
import {ReactComponent as WireTransferIcon} from '../../../../../assets/wire-transfer-logo.svg'
import {SvgIcon} from '@material-ui/core'


interface IBankAccountInfo {
}

const BankAccountInfo: React.FC<IBankAccountInfo> = () => {

    const dispatch = useDispatch()
    const [displayAmount, setDisplayAmount] = useState<boolean>(false)

    const bankAccountInfo = useSelector(bankAccountSelector)

    // Open new card dialogs callback
    const openNewCardDialog = useCallback(() => {
        dispatch(openDialogAction({
            name: DIALOG_NAMES.NEW_CARD
        }))
    }, [dispatch])

    // Open wire transfer dialog callback
    const openWireTransferDialog = useCallback(() => {
        dispatch(openDialogAction({
            name: DIALOG_NAMES.WIRE_TRANSFER
        }))
    }, [dispatch])

    return bankAccountInfo ? <>
        <div className="row">
            <div className="col-12 d-flex flex-column ">
                <Typography component="h2" variant="h6" color="primary" gutterBottom>
                    Bank account info
                </Typography>
            </div>
        </div>
        <div className="row">
            {
                <>
                    <div className="col-12 col-sm-6 d-flex flex-column align-items-end align-items-sm-start mb-4">

                        {/* Bank account name */}
                        <Typography variant="h6">
                            {bankAccountInfo?.bankAccountName || ''}
                        </Typography>

                        {/* IBAN */}
                        <Typography variant="subtitle1">
                            {bankAccountInfo?.IBAN || ''}
                        </Typography>

                        {/* Email */}
                        <Typography variant="subtitle1">
                            {bankAccountInfo?.email || ''}
                        </Typography>

                        <Button
                            color="primary"
                            onClick={openWireTransferDialog}
                            startIcon={
                                <SvgIcon><WireTransferIcon className="mr-3 c-pointer" color="primary"/></SvgIcon>
                            }>
                            Wire transfer
                        </Button>
                    </div>

                    <div className="col-12 col-sm-6 d-flex flex-column align-items-end mt-2 mt-sm-0 ">

                        {/* Bank account amount */}
                        <Typography component="p" variant="h4">
                            {displayAmount ? (bankAccountInfo?.availableAmount).toLocaleString() + ' ' + MAP_CURRENCY[bankAccountInfo?.currency] : '******'}
                            {displayAmount ?
                                <VisibilityOffIcon className="ml-1" onClick={() => setDisplayAmount(!displayAmount)}
                                                   style={{cursor: 'pointer'}}/>
                                : <VisibilityIcon className="ml-1 c-pointer"
                                                  onClick={() => setDisplayAmount(!displayAmount)}
                                                  style={{cursor: 'pointer'}}/>}
                        </Typography>

                        {/* Blocked amount */}
                        <Typography variant="subtitle1" color="textSecondary">
                            Blocked
                            amount: {displayAmount ? bankAccountInfo?.blockedAmount + ' ' + MAP_CURRENCY[bankAccountInfo?.currency] : '****'}
                        </Typography>

                        <Button
                            color="primary"
                            onClick={openNewCardDialog}
                            startIcon={<AddIcon/>}>
                            Create card
                        </Button>

                    </div>
                </>
            }
        </div>
    </> : <div className="row">
        <div className="col-12 justify-content-center mt-3">
            <Typography variant="h4">No data, please try later</Typography>
        </div>
    </div>
}

export default BankAccountInfo
