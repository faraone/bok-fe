import {IAction} from 'fetch-with-redux-observable'
import {IProfile, IProfileInfoResponse} from '../profile.types'
import {IRootState} from '../../../reducer'
import {createSelector, Selector} from 'reselect'
import {
    fetchLastAccessInfoAction,
    fetchProfileInfoAction,
    LOGOUT_ACTION,
    PASSWORD_RESET_NEEDED_ACTION,
    SET_AUTH_STATE_ACTION
} from './profile.fetch'
import {ILoginInfo} from '../../../login/login.types'

export const INIT_PROFILE_STATE: IProfile = {
    isAuthenticated: false,
    loginInfo: null,
    profileInfo: null,
    passwordResetNeeded: false
}

export const profileReducer = (state: IProfile = INIT_PROFILE_STATE, action: IAction): IProfile => {
    switch (action.type) {
        case fetchLastAccessInfoAction.successActionType:
            const {lastAccessDateTime, lastAccessIP} = (action.payload as ILoginInfo)
            return {
                ...state,
                isAuthenticated: true,
                loginInfo: {
                    lastAccessDateTime,
                    lastAccessIP
                }
            }
        case SET_AUTH_STATE_ACTION:
            return {
                ...state,
                isAuthenticated: !!action.payload
            }
        case fetchProfileInfoAction.successActionType:
            const profileInfo = (action.payload as IProfileInfoResponse)
            return {
                ...state,
                profileInfo
            }
        case LOGOUT_ACTION:
            return INIT_PROFILE_STATE
        case PASSWORD_RESET_NEEDED_ACTION:
            return {
                ...state,
                passwordResetNeeded: (action.payload as boolean)
            }
        default:
            return state
    }
}

// logged profile base selector
export const baseProfileSelector: Selector<IRootState, IProfile | null> = (state: IRootState): IProfile | null => state.profile

// @ts-ignore
export const isAuthenticatedSelector = createSelector<IRootState, IProfile | null, boolean>(baseProfileSelector,
    (profile: IProfile) => profile?.isAuthenticated
)

// @ts-ignore
export const profileInfoSelector = createSelector<IRootState, IProfile | null, IProfileInfoResponse>(baseProfileSelector,
    (profile: IProfile) => profile?.profileInfo
)

// @ts-ignore
export const passwordResetNeededSelector = createSelector<IRootState, IProfile | null, boolean>(baseProfileSelector,
    (profile: IProfile) => profile?.passwordResetNeeded
)

// @ts-ignore
export const lastAccessDateTimeSelector = createSelector<IRootState, IProfile | null, string>(baseProfileSelector,
    (profile: IProfile) => profile?.loginInfo?.lastAccessDateTime
)

