export const CLEAR_PLAIN_PAN_CARD = 'CLEAR_PLAIN_PAN_CARD'
export const clearPlainPANCardAction = () => ({type: CLEAR_PLAIN_PAN_CARD})

// Card detail dialogo actions
export const CLEAR_CARD_DETAIL_INFO = 'CLEAR_CARD_DETAIL_INFO'
export const clearCardDetailInfo = () => ({type: CLEAR_CARD_DETAIL_INFO})


