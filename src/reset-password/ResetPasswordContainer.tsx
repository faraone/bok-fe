import React, {useCallback} from 'react'
import {makeStyles} from '@material-ui/core/styles'
import {Avatar, Button, CircularProgress, Grid, Link, Paper, Typography} from '@material-ui/core'
import {Field, Form, Formik} from 'formik'
import {TextField} from 'formik-material-ui'
import {Validators} from '../shared/form-validators'
import {LOGIN_PATH, REGISTRATION_PATH} from '../config/routing/router.constants'
import {useDispatch, useSelector} from 'react-redux'
import {
    FETCH_RESET_PASSWORD_ID,
    fetchResetPasswordAction,
    isFetchResetPasswordPending
} from './redux/reset-password.fetch'
import {ReactComponent as ResetPasswordSVG} from '../assets/password-reset.svg'


// Interface reset password form
interface IResetPasswordForm {
    email: string
    confirmEmail: string
}

// Init values reset password form
const RESET_PASSWORD_INIT_VALUES = {
    email: '',
    confirmEmail: ''
}

// Reset password form names
const RESET_PASSWORD_FORM_NAMES = {
    email: 'email',
    confirmEmail: 'confirmEmail'
}

interface IResetPasswordContainer {
}

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh'
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center'
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/DeMmn2iaFLI/1920x1277)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        opacity: '0.4'
    }
}))

const ResetPasswordContainer: React.FC<IResetPasswordContainer> = () => {

    // Style
    const classes = useStyles()

    const dispatch = useDispatch()

    // Selector
    const isResetPasswordPending = useSelector(isFetchResetPasswordPending)

    // Validate form
    const validateResetPasswordForm = useCallback((values: IResetPasswordForm) => {
        const errors: any = {}

        if (values?.email !== values?.confirmEmail)
            errors[RESET_PASSWORD_FORM_NAMES.confirmEmail] = 'Email insert doesn\'t match'

        return errors
    }, [])

    // Handle submit
    const handleResetFormSubmit = useCallback((values: IResetPasswordForm) => {

        dispatch(fetchResetPasswordAction.build({
            email: values.email
        }, FETCH_RESET_PASSWORD_ID))

    }, [dispatch])

    return <Grid container component="main" className={classes.root}>

        <Grid xs={false} sm={4} md={7} className={classes.image}/>

        <Grid xs={12} sm={8} md={5} component={Paper} elevation={6} square>

            <div className={'col ' + classes.paper}>
                <Avatar className={classes.avatar}>
                    <ResetPasswordSVG width={30}/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Reset password
                </Typography>

                <Formik
                    initialValues={RESET_PASSWORD_INIT_VALUES}
                    onSubmit={handleResetFormSubmit}
                    validate={validateResetPasswordForm}
                    validateOnBlur={false}
                >
                    <Form className={classes.form} noValidate>

                        {/*Email reset password*/}
                        <Field
                            component={TextField}
                            label="Email..."
                            id="emailResetPsw"
                            fullWidth
                            name={RESET_PASSWORD_FORM_NAMES.email}
                            disabled={isResetPasswordPending}
                            validate={Validators.required}
                        />

                        {/*Confirm email reset password*/}
                        <Field
                            component={TextField}
                            label="Confirm email..."
                            id="confirmEmailResetPsw"
                            fullWidth
                            name={RESET_PASSWORD_FORM_NAMES.confirmEmail}
                            disabled={isResetPasswordPending}
                            validate={Validators.required}
                        />

                        {/*SUBMIT*/}
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            disabled={isResetPasswordPending}
                            className={classes.submit}>
                            {
                                isResetPasswordPending &&
                                <div className="mr-2" color="white"><CircularProgress color="inherit" size={20}/></div>
                            }
                            Submit
                        </Button>

                        <Grid container>
                            <Grid item xs>
                                <Link href={LOGIN_PATH} variant="body2">
                                    Sign in
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link href={REGISTRATION_PATH} variant="body2">
                                    Don't have an account? Sign Up
                                </Link>
                            </Grid>
                        </Grid>

                    </Form>
                </Formik>

            </div>
        </Grid>
    </Grid>
}

export default ResetPasswordContainer
