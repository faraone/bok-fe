import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {
    FETCH_ALL_BANK_TRANSACTION_API,
    FETCH_BANK_ACCOUNT_INFO_API,
    FETCH_CHECK_PAYMENT_AMOUNT_API,
    FETCH_WIRE_TRANSFER_API
} from '../../../../../core/fetch/fetch.constants'

// bank account fetch action
export const FETCH_BANK_ACCOUNT_INFO = "FETCH_BANK_ACCOUNT_INFO"
export const FETCH_BANK_ACCOUNT_INFO_ID = "FETCH_BANK_ACCOUNT_INFO_ID"

export const fetchBankAccountInfoAction = fetchActionFactory(FETCH_BANK_ACCOUNT_INFO_API, FETCH_BANK_ACCOUNT_INFO)
export const isFetchBankAccountInfoPending = isRequestInPending(fetchBankAccountInfoAction.pendingActionTypeWithSpinner, FETCH_BANK_ACCOUNT_INFO_ID)

// check payment amount fetch action
export const FETCH_CHECK_PAYMENT_AMOUNT = 'FETCH_CHECK_PAYMENT_AMOUNT'
export const FETCH_CHECK_PAYMENT_AMOUNT_ID = 'FETCH_CHECK_PAYMENT_AMOUNT_ID'

export const fetchCheckPaymentAmountAction = fetchActionFactory(FETCH_CHECK_PAYMENT_AMOUNT_API, FETCH_CHECK_PAYMENT_AMOUNT)
export const isFetchCheckPaymentAmountPending = isRequestInPending(fetchCheckPaymentAmountAction.pendingActionTypeWithSpinner, FETCH_CHECK_PAYMENT_AMOUNT_ID)

// check payment amount fetch action
export const FETCH_ALL_BANK_TRANSACTION = 'FETCH_ALL_BANK_TRANSACTION'
export const FETCH_ALL_BANK_TRANSACTION_ID = 'FETCH_ALL_BANK_TRANSACTION_ID'

export const fetchAllBankTransactionAction = fetchActionFactory(FETCH_ALL_BANK_TRANSACTION_API, FETCH_ALL_BANK_TRANSACTION)
export const isFetchAllBankTransactionPending = isRequestInPending(fetchAllBankTransactionAction.pendingActionTypeWithSpinner, FETCH_ALL_BANK_TRANSACTION_ID)

// check payment amount fetch action
export const FETCH_WIRE_TRANSFER = 'FETCH_WIRE_TRANSFER'
export const FETCH_WIRE_TRANSFER_ID = 'FETCH_WIRE_TRANSFER_ID'

export const fetchWireTransferAction = fetchActionFactory(FETCH_WIRE_TRANSFER_API, FETCH_WIRE_TRANSFER)
export const isFetchWireTransferPending = isRequestInPending(fetchWireTransferAction.pendingActionTypeWithSpinner, FETCH_WIRE_TRANSFER_ID)
