import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {FETCH_CHANGE_PASSWORD_API, FETCH_LOGIN_API, FETCH_LOGOUT_API} from '../../core/fetch/fetch.constants'

// Login fetch action
export const FETCH_LOGIN = 'FETCH_LOGIN'
export const FETCH_LOGIN_ID = 'FETCH_LOGIN_ID'

export const fetchLoginAction = fetchActionFactory(FETCH_LOGIN_API, FETCH_LOGIN)
export const isFetchLoginPending = isRequestInPending(fetchLoginAction.pendingActionTypeWithSpinner, FETCH_LOGIN_ID)

// Logout fetch action
export const FETCH_LOGOUT = 'FETCH_LOGOUT'
export const FETCH_LOGOUT_ID = 'FETCH_LOGOUT_ID'

export const fetchLogoutAction = fetchActionFactory(FETCH_LOGOUT_API, FETCH_LOGOUT)
export const isFetchLogoutPending = isRequestInPending(fetchLogoutAction.pendingActionTypeWithSpinner, FETCH_LOGOUT_ID)

// Change password fetch action
export const FETCH_CHANGE_PASSWORD = 'FETCH_CHANGE_PASSWORD'
export const FETCH_CHANGE_PASSWORD_ID = 'FETCH_CHANGE_PASSWORD_ID'

export const fetchChangePasswordAction = fetchActionFactory(FETCH_CHANGE_PASSWORD_API, FETCH_CHANGE_PASSWORD)
export const isFetchChangePasswordPending = isRequestInPending(fetchChangePasswordAction.pendingActionTypeWithSpinner, FETCH_CHANGE_PASSWORD_ID)
