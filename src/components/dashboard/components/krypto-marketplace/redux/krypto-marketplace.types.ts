export interface IKrytoInfo {
    name: string
    networkFee: number
    price: number | string
    symbol: string
    updateTimestamp: string
}

export interface IBaseMarketState {
    kryptoList: IKrytoInfo[] | null
    currentHistoricalData: IHistoricalDataResponse | null
}

export interface IHistoricalData {
    id: number
    instant: string
    marketCap: number
    price: number
}

export interface IHistoricalDataResponse {
    end: string
    history: IHistoricalData[]
    start: string
    symbol: string
}
