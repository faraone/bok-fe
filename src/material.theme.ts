import {createMuiTheme, Theme} from '@material-ui/core'


export const themeSettings: Theme = createMuiTheme({
    palette: {
        primary: {
            main: '#0f4c81',
            light: '#96b3d2',
            dark: '#060a16'
        },
        secondary: {
            main: '#6BB80F',
            light: '#8DC34A',
            dark: '#283815'
        }
    }
})
