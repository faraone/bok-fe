export interface ICardInfo {
    cardId: number // int64
    cardStatus: ECardStatus
    cvv: number //int32
    label: string
    maskedPan: string
    name: string
    type: string
}

export interface INewCardRequest {
    label: string
    name: string
    type: string
}

export enum ECardType {
    CREDIT = 'CREDIT',
    DEBIT = 'DEBIT',
    VIRTUAL = 'VIRTUAL',
    ONE_USE = 'ONE_USE'
}

export enum ECardStatus {
    TO_ACTIVATE = 'TO_ACTIVATE',
    ACTIVE = 'ACTIVE',
    DEACTIVATED = 'DEACTIVATED',
    LOST = 'LOST',
    STOLEN = 'STOLEN',
    BROKEN = 'BROKEN',
    DESTROYED = 'DESTROYED',
    EXPIRED = 'EXPIRED',
}
