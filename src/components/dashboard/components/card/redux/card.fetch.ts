// bank account fetch action
import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {
    FETCH_ACTIVATE_CARD_API,
    FETCH_CARD_BY_ID_API,
    FETCH_CHANGE_CARD_STATUS_API,
    FETCH_CREATE_CARD_API,
    FETCH_DELETE_CARD_API,
    FETCH_GET_ALL_USER_CARDS_API,
    FETCH_GET_CARD_PIN_API,
    FETCH_GET_CARD_TOKEN_API,
    FETCH_GET_CVV_CARD_API,
    FETCH_GET_PLAIN_PAN_CARD_API
} from '../../../../../core/fetch/fetch.constants'

// find card
export const FETCH_CARD_BY_ID = "FETCH_CARD_BY_ID"
export const FETCH_CARD_BY_ID_ID = "FETCH_CARD_BY_ID_ID"

export const fetchCardByIdAction = fetchActionFactory(FETCH_CARD_BY_ID_API, FETCH_CARD_BY_ID)
export const isFetchCardByIdActionPending = isRequestInPending(fetchCardByIdAction.pendingActionTypeWithSpinner, FETCH_CARD_BY_ID_ID)

// get all cards
export const FETCH_ALL_USER_CARDS = 'FETCH_ALL_USER_CARDS'
export const FETCH_ALL_USER_CARDS_ID = 'FETCH_ALL_USER_CARDS_ID'
export const fetchAllUserCardsAction = fetchActionFactory(FETCH_GET_ALL_USER_CARDS_API, FETCH_ALL_USER_CARDS)
export const isFetchAllUserCardsPending = isRequestInPending(fetchAllUserCardsAction.pendingActionTypeWithSpinner, FETCH_ALL_USER_CARDS_ID)

export const FETCH_ALL_USER_CARDS_HOME_ID = 'FETCH_ALL_USER_CARDS_HOME_ID'
export const isFetchAllUserCardsHomePending = isRequestInPending(fetchAllUserCardsAction.pendingActionTypeWithSpinner, FETCH_ALL_USER_CARDS_HOME_ID)

// get create card
export const FETCH_CREATE_CARD = 'FETCH_CREATE_CARD'
export const FETCH_CREATE_CARD_ID = 'FETCH_CREATE_CARD_ID'

export const fetchCreateCardAction = fetchActionFactory(FETCH_CREATE_CARD_API, FETCH_CREATE_CARD)
export const isFetchCreateCardPending = isRequestInPending(fetchCreateCardAction.pendingActionTypeWithSpinner, FETCH_CREATE_CARD_ID)

// get delete card
export const FETCH_DELETE_CARD = 'FETCH_DELETE_CARD'
export const FETCH_DELETE_CARD_ID = 'FETCH_DELETE_CARD_ID'

export const fetchDeleteCardAction = fetchActionFactory(FETCH_DELETE_CARD_API, FETCH_DELETE_CARD)
export const isFetchDeleteCardPending = isRequestInPending(fetchDeleteCardAction.pendingActionTypeWithSpinner, FETCH_DELETE_CARD_ID)

// get all cards
export const FETCH_GET_CVV_CARD = 'FETCH_GET_CVV_CARD'
export const FETCH_GET_CVV_CARD_ID = 'FETCH_GET_CVV_CARD_ID'

export const fetchGetCvvCardAction = fetchActionFactory(FETCH_GET_CVV_CARD_API, FETCH_GET_CVV_CARD)
export const isFetchGetCvvCardPending = isRequestInPending(fetchGetCvvCardAction.pendingActionTypeWithSpinner, FETCH_GET_CVV_CARD_ID)

// get plain PAN
export const FETCH_GET_PLAIN_PAN_CARD = 'FETCH_GET_PLAIN_PAN_CARD'

export const FETCH_GET_PLAIN_PAN_CARD_ID = 'FETCH_GET_PLAIN_PAN_CARD_ID'
// Request id from dialog
export const FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID = 'FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID'

export const fetchGetPlainPanCardAction = fetchActionFactory(FETCH_GET_PLAIN_PAN_CARD_API, FETCH_GET_PLAIN_PAN_CARD)
export const isFetchGetPlainPanCardPending = isRequestInPending(fetchGetPlainPanCardAction.pendingActionTypeWithSpinner, FETCH_GET_PLAIN_PAN_CARD_ID)
export const isFetchGetPanCardDialogPending = isRequestInPending(fetchGetPlainPanCardAction.pendingActionTypeWithSpinner, FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID)

// get card token
export const FETCH_GET_CARD_TOKEN = 'FETCH_GET_CARD_TOKEN'
export const FETCH_GET_CARD_TOKEN_ID = 'FETCH_GET_CARD_TOKEN_ID'

export const fetchGetCardTokenAction = fetchActionFactory(FETCH_GET_CARD_TOKEN_API, FETCH_GET_CARD_TOKEN)
export const isFetchGetCardTokenPending = isRequestInPending(fetchGetCardTokenAction.pendingActionTypeWithSpinner, FETCH_GET_CARD_TOKEN_ID)

// change card status
export const FETCH_CHANGE_CARD_STATUS = 'FETCH_CHANGE_CARD_STATUS'
export const FETCH_CHANGE_CARD_STATUS_ID = 'FETCH_CHANGE_CARD_STATUS_ID'

export const fetchChangeCardStatusAction = fetchActionFactory(FETCH_CHANGE_CARD_STATUS_API, FETCH_CHANGE_CARD_STATUS)
export const isFetchChangeCardStatusPending = isRequestInPending(fetchChangeCardStatusAction.pendingActionTypeWithSpinner, FETCH_CHANGE_CARD_STATUS_ID)

// activate card
export const FETCH_ACTIVATE_CARD = 'FETCH_ACTIVATE_CARD'
export const FETCH_ACTIVATE_CARD_ID = 'FETCH_ACTIVATE_CARD_ID'

export const fetchActivateCardAction = fetchActionFactory(FETCH_ACTIVATE_CARD_API, FETCH_ACTIVATE_CARD)
export const isFetchActivateCardPending = isRequestInPending(fetchActivateCardAction.pendingActionTypeWithSpinner, FETCH_ACTIVATE_CARD_ID)


// activate card
export const FETCH_CARD_PIN = 'FETCH_CARD_PIN'
export const FETCH_CARD_PIN_ID = 'FETCH_CARD_PIN_ID'

export const fetchCardPINAction = fetchActionFactory(FETCH_GET_CARD_PIN_API, FETCH_CARD_PIN)
export const isFetchCardPINPending = isRequestInPending(fetchCardPINAction.pendingActionTypeWithSpinner, FETCH_CARD_PIN_ID)

