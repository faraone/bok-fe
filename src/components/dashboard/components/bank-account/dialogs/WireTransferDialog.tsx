import * as React from 'react'
import {useCallback, useEffect} from 'react'
import {Button, CircularProgress, DialogContent} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import moment from 'moment'
import {Field, Form, Formik} from 'formik'
import {FormikNumberFormat} from '../../../../../shared/FormikNumberFormat'
import {Validators} from '../../../../../shared/form-validators'
import {CheckboxWithLabel, TextField as FormikTextField} from 'formik-material-ui'
import {DatePicker} from 'formik-material-ui-pickers'
import {FormikHelpers} from 'formik/dist/types'
import {
    FETCH_ALL_BANK_TRANSACTION_ID,
    FETCH_BANK_ACCOUNT_INFO_ID,
    FETCH_WIRE_TRANSFER_ID,
    fetchAllBankTransactionAction,
    fetchBankAccountInfoAction,
    fetchWireTransferAction,
    isFetchWireTransferPending
} from '../redux/bank-account.fetch'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'

const WIRE_TRANSFER_FORM_NAME = {
    destinationIBAN: 'destinationIBAN',
    beneficiary: 'beneficiary',
    amount: 'amount',
    executionDate: 'executionDate',
    instantTransfer: 'instantTransfer',
    causal: 'causal'
}

const WIRE_TRANSFER_INIT_VALUES = {
    destinationIBAN: '',
    beneficiary: '',
    amount: 0,
    executionDate: moment(),
    instantTransfer: false,
    causal: ''
}

interface IWireTransferDialog {
}

const WireTransferDialog: React.FC<IWireTransferDialog> = () => {

    const dispatch = useDispatch()

    const isWireTransferPending = useSelector(isFetchWireTransferPending)

    // Handle submit
    const handleSubmit = useCallback((values, formikHelper: FormikHelpers<any>) => {

        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchWireTransferAction.build({
            destinationIBAN: values.destinationIBAN,
            beneficiary: values.beneficiary,
            money: {
                amount: values.amount,
                currency: 'EUR'
            },
            executionDate: values.instantTransfer ? '' : values.executionDate.format('YYYY-MM-DD'),
            instantTransfer: values.instantTransfer,
            causal: values.causal
        }, FETCH_WIRE_TRANSFER_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

        formikHelper.setSubmitting(false)
    }, [dispatch])

    // Close dialog
    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    useEffect(() => () => {
        const token = Cookies.get(USER_TOKEN)

        // Bank account info
        dispatch(fetchBankAccountInfoAction.build(null, FETCH_BANK_ACCOUNT_INFO_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

        dispatch(fetchAllBankTransactionAction.build(null, FETCH_ALL_BANK_TRANSACTION_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

    }, [dispatch])

    return <DialogContent>
        <Typography variant="h6" color="primary" gutterBottom> Bank transfer </Typography>

        <Formik
            initialValues={WIRE_TRANSFER_INIT_VALUES}
            onSubmit={handleSubmit}
            validateOnChange={false}
        >
            {({values, setFieldValue}) => <Form>

                <div className="row align-items-center">

                    {/*Destination IBAN*/}
                    <div className="col-12">
                        <Field
                            component={FormikTextField}
                            label="IBAN *"
                            fullWidth
                            name={WIRE_TRANSFER_FORM_NAME.destinationIBAN}
                            validate={Validators.required}
                        />
                    </div>

                    {/*Beneficiary*/}
                    <div className="col-12 col-sm-6 mt-sm-2">
                        <Field
                            component={FormikTextField}
                            name={WIRE_TRANSFER_FORM_NAME.beneficiary}
                            label="Beneficiary *"
                            fullWidth
                            validate={Validators.required}
                        />
                    </div>

                    {/*Amount*/}
                    <div className="col-12 col-sm-6 mt-sm-2">
                        <Field
                            component={FormikNumberFormat}
                            name={WIRE_TRANSFER_FORM_NAME.amount}
                            numberFormatProps={{
                                label: 'Amount *',
                                decimalSeparator: ',',
                                thousandSeparator: '.',
                                decimalScale: 2,
                                fixedDecimalScale: true,
                                allowNegative: false,
                                InputProps: {
                                    endAdornment: '€',
                                    className: 'w-100'
                                }
                            }}
                            validate={Validators.required}
                        />
                    </div>

                    {/*Causal*/}
                    <div className="col-12 mt-sm-2">
                        <Field
                            component={FormikTextField}
                            name={WIRE_TRANSFER_FORM_NAME.causal}
                            label="Causal *"
                            fullWidth
                            validate={Validators.required}
                            multiline
                            inputProps={{
                                maxLength: 255
                            }}
                            helperText={`${values?.causal?.length || '0'}/255`}

                        />
                    </div>

                    {/*Instant transfer*/}
                    <div className="col-12 col-sm-6 mt-sm-2">
                        <Field
                            name={WIRE_TRANSFER_FORM_NAME.instantTransfer}
                            component={CheckboxWithLabel}
                            type="checkbox"
                            Label={{label: 'Instant transfer'}}
                            size="small"
                            color="primary"
                            onChange={(event: any) => {
                                setFieldValue(WIRE_TRANSFER_FORM_NAME.instantTransfer, event.target.checked)
                                setFieldValue(WIRE_TRANSFER_FORM_NAME.executionDate, moment())
                            }}
                        />
                    </div>

                    {/*Execution date*/}
                    <div className="col-12 col-sm-6 mt-sm-2">
                        <Field
                            name={WIRE_TRANSFER_FORM_NAME.executionDate}
                            component={DatePicker}
                            size="small"
                            variant="inline"
                            format="DD/MM/YYYY"
                            margin="normal"
                            label="Execution date"
                            disablePast
                            autoOk
                            className="m-0"
                            disabled={values.instantTransfer}/>
                    </div>

                </div>

                <div className="row">
                    <div className="d-flex justify-content-between w-100 p-3">
                        <Button variant="outlined" color="default" onClick={closeDialog}> Close </Button>
                        <Button variant="contained" color="primary" type="submit" disabled={isWireTransferPending}>
                            {isWireTransferPending && <CircularProgress className="ml-2" color="inherit" size={20}/>}
                            Transfer
                        </Button>
                    </div>
                </div>
            </Form>
            }
        </Formik>

    </DialogContent>
}

export default WireTransferDialog
