import * as React from 'react'
import {useCallback, useEffect, useMemo, useState} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {ECardStatus, ICardInfo} from '../../../card/redux/card.types'
import {clearCardDetailInfo} from '../../../card/redux/card.actions'
import {
    Button,
    CircularProgress,
    DialogActions,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Tooltip
} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import VisibilityIcon from '@material-ui/icons/Visibility'
import {
    CARD_ID_PATH_VARIABLE,
    CARD_STATUS_PATH_VARIABLE,
    MAP_CARD_STATUS,
    MAP_CARD_STATUS_COLOR,
    MAP_CARD_TYPE,
    USER_TOKEN
} from '../../../../../../core/components/utils'
import {
    FETCH_CARD_PIN_ID,
    FETCH_DELETE_CARD_ID,
    FETCH_GET_CVV_CARD_ID,
    FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID,
    fetchActivateCardAction,
    fetchCardPINAction,
    fetchChangeCardStatusAction,
    fetchDeleteCardAction,
    fetchGetCvvCardAction,
    fetchGetPlainPanCardAction,
    isFetchChangeCardStatusPending,
    isFetchDeleteCardPending,
    isFetchGetCvvCardPending,
    isFetchGetPanCardDialogPending
} from '../../../card/redux/card.fetch'
import {
    cardPinDialogSelector,
    currentCvvCardSelector,
    currentPanCardDialogSelector
} from '../../../card/redux/card.selectors'
import Cookies from 'js-cookie'
import {closeDialogAction} from '../../../../../shared/dialog/redux/dialog.actions'
import CheckIcon from '@material-ui/icons/Check'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import {ILabelValueOption} from '../../../../dashboard.types'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'

interface ICardDetailDialog {
    card: ICardInfo
}

// List of dialog actions that required Pin before execute them
enum ERequestWithPin {
    SHOW_PAN = 'SHOW_PAN',
    SHOW_CVV = 'SHOW_CVV',
    DELETE_CARD = 'DELETE_CARD',
    ACTIVATE_CARD = 'ACTIVATE_CARD',
    CHANGE_STATUS = 'CHANGE_STATUS'
}

const cardStatusChangeOptions: ILabelValueOption<ECardStatus>[] = [
    {
        label: 'Card lost',
        value: ECardStatus.LOST
    },
    {
        label: 'Card broken',
        value: ECardStatus.BROKEN
    },
    {
        label: 'Card stolen',
        value: ECardStatus.STOLEN
    }
]

const CardDetailDialog: React.FC<ICardDetailDialog> = (props) => {

    const dispatch = useDispatch()

    const {card} = props

    // Local state - visibility icon PAN and CVV
    const [showPan, setShowPan] = useState<boolean>(false)
    const [showCVV, setShowCVV] = useState<boolean>(false)
    // Local state - PIN
    const [cardPin, setCardPin] = useState<string>('')
    // Local state - Display change card state select
    const [displayChangeStatus, setDisplayChangeStatus] = useState<boolean>(false)
    // Local state - change card status select
    const [statusUpdatedSelect, setStatusUpdatedSelect] = useState<ECardStatus | ''>('')

    const [handleRequestWithPin, setHandleRequestWithPin] = useState<{
        showInsertPin: boolean,
        requestClicked: ERequestWithPin | null
    }>({
        showInsertPin: false,
        requestClicked: null
    })

    // Selector
    const isFetchCVVPending = useSelector(isFetchGetCvvCardPending)
    const isFetchCurrentPlainPANPending = useSelector(isFetchGetPanCardDialogPending)
    const isFetchDeletePending = useSelector(isFetchDeleteCardPending)
    const isFetchChangeStatusPending = useSelector(isFetchChangeCardStatusPending)

    const currentCVV = useSelector(currentCvvCardSelector)
    const currentPlainPAN = useSelector(currentPanCardDialogSelector)
    const cardPinState = useSelector(cardPinDialogSelector)

    const isFetchAfterInsertPinPinding = useMemo(() => isFetchCVVPending ||
        isFetchCurrentPlainPANPending ||
        isFetchDeletePending, [isFetchCVVPending, isFetchCurrentPlainPANPending, isFetchDeletePending])

    // get current pan from props or from selector and separate char
    // es: 12345678 -> 1234 5678
    const currentPan = useMemo(() => currentPlainPAN ?
        (currentPlainPAN + '')?.match(/.{0,4}/g)?.join(' ') : ''
        , [currentPlainPAN])

    // check pin
    const submitRequestWithPin = useCallback(() => {

        const token = Cookies.get(USER_TOKEN)

        let fetchRequest

        switch (handleRequestWithPin.requestClicked) {
            /** fetch CVV */
            case ERequestWithPin.SHOW_CVV:
                fetchRequest = fetchGetCvvCardAction.build({
                    pin: cardPinState ? cardPinState : cardPin
                }, FETCH_GET_CVV_CARD_ID, {
                    [CARD_ID_PATH_VARIABLE]: card.cardId
                }, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
                break

            /** fetch PAN */
            case ERequestWithPin.SHOW_PAN:
                fetchRequest = fetchGetPlainPanCardAction.build({
                    pin: cardPinState ? cardPinState : cardPin
                }, FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID, {
                    [CARD_ID_PATH_VARIABLE]: card.cardId
                }, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
                break

            /** fetch delete card */
            case ERequestWithPin.DELETE_CARD:
                fetchRequest = fetchDeleteCardAction.build({
                    pin: cardPinState ? cardPinState : cardPin
                }, FETCH_DELETE_CARD_ID, {
                    [CARD_ID_PATH_VARIABLE]: card.cardId
                }, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
                break

            /** fetch activate card */
            case ERequestWithPin.ACTIVATE_CARD:
                fetchRequest = fetchActivateCardAction.build({
                    pin: cardPinState ? cardPinState : cardPin
                }, FETCH_DELETE_CARD_ID, {
                    [CARD_ID_PATH_VARIABLE]: card.cardId
                }, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
                break

            /** fetch change card status */
            case ERequestWithPin.CHANGE_STATUS:
                fetchRequest = fetchChangeCardStatusAction.build({
                    pin: cardPinState ? cardPinState : cardPin
                }, FETCH_DELETE_CARD_ID, {
                    [CARD_ID_PATH_VARIABLE]: card.cardId,
                    [CARD_STATUS_PATH_VARIABLE]: statusUpdatedSelect
                }, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })
                break

            default:
                console.log('default')

        }
        fetchRequest && dispatch(fetchRequest)
        setHandleRequestWithPin({...handleRequestWithPin, requestClicked: null})
    }, [card.cardId, cardPin, cardPinState, dispatch, handleRequestWithPin, statusUpdatedSelect])

    useEffect(() => {
        cardPinState && submitRequestWithPin()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [handleRequestWithPin.requestClicked])

    // set show pan local state and fetch it if not present
    const getPlainPan = useCallback(() => {
        // showPan set true -> set false and don't show it
        if (showPan) {
            setShowPan(false)
        } else {
            if (currentPan) {
                setShowPan(true)
            } else {
                // else, if card PIN already get, simply show it
                if (cardPinState) {
                    // else fetch PAN request without password
                    setHandleRequestWithPin({requestClicked: ERequestWithPin.SHOW_PAN, showInsertPin: false})
                } else {
                    // else request pin to user, show insert Pin field
                    setHandleRequestWithPin({requestClicked: ERequestWithPin.SHOW_PAN, showInsertPin: true})
                }
            }
        }
    }, [cardPinState, currentPan, showPan])

    // get cvv and show it
    const getCvv = useCallback(() => {
        // showCVV set true -> set false, clear state and don't show it
        if (showCVV) {
            setShowCVV(false)
        } else {
            if (currentCVV) {
                setShowCVV(true)
            } else {

                // else, if card PIN already get, simply show it
                if (cardPinState) {
                    // else fetch PAN request without password
                    setHandleRequestWithPin({requestClicked: ERequestWithPin.SHOW_CVV, showInsertPin: false})
                } else {
                    // else request pin to user, show insert Pin field
                    setHandleRequestWithPin({requestClicked: ERequestWithPin.SHOW_CVV, showInsertPin: true})
                }
            }
        }
    }, [cardPinState, currentCVV, showCVV])

    // On currentCVV setting, after request success, set local state to show cvv
    useEffect(() => {
        setShowCVV(!!currentCVV)
    }, [currentCVV])

    // On currentPan setting, after request success, set local state to show pan
    useEffect(() => {
        setShowPan(!!currentPlainPAN)
    }, [currentPlainPAN])

    // CLOSE INSERT PIN FIELD - If at least one time currentCVV or
    // currentPlainPAN are setting with value. It means that user already enter pin card
    useEffect(() => {
        if (cardPinState) {
            setHandleRequestWithPin({...handleRequestWithPin, showInsertPin: false})
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [currentCVV, currentPlainPAN])

    // Close dialog callback
    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    // On Delete card button click callback
    const dispatchDeleteCard = useCallback(() => {
        // If pin already entered and verified, just call service
        if (cardPinState) {
            const token = Cookies.get(USER_TOKEN)

            dispatch(fetchDeleteCardAction.build({
                pin: cardPinState ? cardPinState : cardPin
            }, FETCH_DELETE_CARD_ID, {
                [CARD_ID_PATH_VARIABLE]: card.cardId
            }, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))
        } else {
            setHandleRequestWithPin({requestClicked: ERequestWithPin.DELETE_CARD, showInsertPin: true})
        }
    }, [card.cardId, cardPin, cardPinState, dispatch])

    // On Activate card button click callback
    const dispatchActivateCard = useCallback(() => {
        // If pin already entered and verified, just call service
        if (cardPinState) {
            const token = Cookies.get(USER_TOKEN)

            dispatch(fetchActivateCardAction.build({
                pin: cardPinState ? cardPinState : cardPin
            }, FETCH_DELETE_CARD_ID, {
                [CARD_ID_PATH_VARIABLE]: card.cardId
            }, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))
        } else {
            setHandleRequestWithPin({requestClicked: ERequestWithPin.ACTIVATE_CARD, showInsertPin: true})
        }
    }, [card.cardId, cardPin, cardPinState, dispatch])


    // On change card status button click callback
    const submitChangeCardStatus = useCallback(() => {
        if (statusUpdatedSelect) {
            // If pin already entered and verified, just call service
            if (cardPinState) {
                const token = Cookies.get(USER_TOKEN)

                dispatch(fetchChangeCardStatusAction.build({
                    pin: cardPinState ? cardPinState : cardPin
                }, FETCH_DELETE_CARD_ID, {
                    [CARD_ID_PATH_VARIABLE]: card.cardId,
                    [CARD_STATUS_PATH_VARIABLE]: statusUpdatedSelect
                }, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                }))
            } else {
                setHandleRequestWithPin({requestClicked: ERequestWithPin.CHANGE_STATUS, showInsertPin: true})
            }
        } else {
            dispatch(addWarning({userMessage: 'Select card problem before confirm'}))
        }

    }, [card.cardId, cardPin, cardPinState, dispatch, statusUpdatedSelect])

    const getCardPinCallback = useCallback(() => {
        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchCardPINAction.build(null, FETCH_CARD_PIN_ID, {
            [CARD_ID_PATH_VARIABLE]: card.cardId
        }, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
    }, [card.cardId, dispatch])

    // Unmount - clear dialog state
    useEffect(() => () => {
        dispatch(clearCardDetailInfo())
    }, [dispatch])


    return <>

        <DialogTitle disableTypography>
            <div className="d-flex align-items-center justify-content-between">
                <Typography variant="h6" color="primary" gutterBottom> Card detail </Typography>

                {card.cardStatus === ECardStatus.TO_ACTIVATE ?
                    <Button color="primary"
                            onClick={dispatchActivateCard}>Activate</Button>
                    : <Button color="primary"
                              onClick={dispatchDeleteCard}>Delete</Button>}
            </div>
        </DialogTitle>

        <DialogContent>

            {/*Card info*/}
            <div className="row">

                {/*Masked pan*/}
                <div className="col-12 d-flex align-items-center ">
                    <Typography variant="h5" className="mr-3">
                        {showPan ? currentPan : card?.maskedPan}
                    </Typography>
                    {isFetchCurrentPlainPANPending ? <CircularProgress size="20"/> :
                        <VisibilityIcon fontSize="small"
                                        style={{cursor: 'pointer'}}
                                        onClick={getPlainPan}/>}
                </div>

                <div className="col-12">
                    {/*CVV*/}
                    <Typography variant="caption"> CVV </Typography>
                    <Typography variant="subtitle1" className="mr-3">
                        {showCVV ? currentCVV : '***'}
                        {isFetchCVVPending ? <CircularProgress size="20"/> :
                            <VisibilityIcon fontSize="small"
                                            style={{cursor: 'pointer', marginLeft: 10}}
                                            onClick={getCvv}/>}
                    </Typography>

                    {/*Insert card PIN*/}
                    {handleRequestWithPin.showInsertPin && <div className="my-2">
                        <TextField id="cardPin"
                                   value={cardPin}
                                   onChange={(event) => setCardPin(event.target.value)}
                                   label="PIN"/>

                        {
                            isFetchAfterInsertPinPinding ?
                                <CircularProgress className="ml-2" color="inherit" size={20}/> :
                                <>
                                    {/*Close card pin*/}
                                    <IconButton
                                        aria-label="close"
                                        onClick={() => setHandleRequestWithPin({
                                            ...handleRequestWithPin,
                                            showInsertPin: false
                                        })}
                                    >
                                        <CloseIcon fontSize="small"/>
                                    </IconButton>

                                    {/*Check card pin*/}
                                    <IconButton
                                        aria-label="checkPin"

                                        disabled={isFetchAfterInsertPinPinding}
                                        onClick={submitRequestWithPin}
                                    >
                                        <CheckIcon fontSize="small"/>
                                    </IconButton>
                                </>
                        }

                        <br/><Typography
                        variant="caption"
                        color="primary"
                        className="c-pointer"
                        onClick={getCardPinCallback}>Resend PIN</Typography>

                    </div>}

                </div>
            </div>

            <div className="row mt-2">

                <div className="col-6">
                    {/*Card name*/}
                    <Typography variant="caption"> Card name </Typography>
                    <Typography variant="subtitle1"> {card.name} </Typography>
                </div>

                <div className="col-6">
                    {/*Card label*/}
                    <Typography variant="caption"> Card label </Typography>
                    <Typography variant="subtitle1"> {card.label} </Typography>
                </div>

                <div className="col-6">
                    {/*Card type*/}
                    <Typography variant="caption"> Type </Typography>
                    <Typography variant="subtitle1"> {MAP_CARD_TYPE[card.type]} </Typography>
                </div>

                <div className="col-6">
                    {/*Card status*/}
                    <Typography variant="caption"> Status </Typography>
                    <Tooltip title={`status: ${MAP_CARD_STATUS[card.cardStatus]}`}>
                                                            <span
                                                                style={{
                                                                    height: '10px',
                                                                    width: '10px',
                                                                    backgroundColor: MAP_CARD_STATUS_COLOR[card.cardStatus],
                                                                    borderRadius: '50%',
                                                                    display: 'inline-block'
                                                                }}/>
                    </Tooltip>
                    <Typography variant="subtitle1"> {MAP_CARD_STATUS[card.cardStatus]} </Typography>
                </div>
            </div>

            <div className="row mt-3">
                <div className="col-12 c-pointer"
                     onClick={() => setDisplayChangeStatus(!displayChangeStatus)}
                >
                    <Typography variant="subtitle2" color="primary"> Did you have some problem with card? </Typography>

                </div>

                {displayChangeStatus && <div className="col-12 d-flex align-items-end ">

                    <FormControl className="w-75 mt-2">
                        <InputLabel htmlFor="destinationCard"> Report it *</InputLabel>

                        <Select id="cardStatus"
                                value={statusUpdatedSelect}
                                onChange={(event) =>
                                    //@ts-ignore
                                    setStatusUpdatedSelect(event.target.value)}>

                            <MenuItem key="default" value="" disabled>Select problem</MenuItem>

                            {cardStatusChangeOptions.map((cardStatusOption, index: number) =>
                                // @ts-ignore
                                <MenuItem key={index}
                                          value={cardStatusOption.value}>{cardStatusOption.label}</MenuItem>)}
                        </Select>
                    </FormControl>

                    {/* Check change card status*/}
                    <IconButton
                        aria-label="checkStatus"
                        disabled={isFetchChangeStatusPending}
                        onClick={submitChangeCardStatus}
                    >
                        <CheckIcon fontSize="small"/>
                    </IconButton>
                </div>}
            </div>

        </DialogContent>

        <DialogActions>
            <div className="d-flex justify-content-end w-100 p-3">
                <Button variant="contained" color="primary" onClick={closeDialog}> Close </Button>
            </div>
        </DialogActions>

    </>
}

export default CardDetailDialog
