import * as React from 'react'
import {useCallback, useMemo, useState} from 'react'
import {
    Button,
    DialogActions,
    DialogContent,
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField
} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import NumberFormat, {NumberFormatValues} from 'react-number-format'
import {USER_TOKEN} from '../../../../../core/components/utils'
import Cookies from 'js-cookie'
import {FETCH_SELL_KRYPTO_ID, fetchSellKyptoAction} from '../../krypto-marketplace/redux/krypto-marketplace.fetch'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {marketKryptoListSelector} from '../../krypto-marketplace/redux/krypto-marketplace.selectors'
import {walletListSelector} from '../../krypto-wallets/redux/krypto-wallets.selectors'
import {cardListSourceOptionsSelector} from '../../card/redux/card.selectors'
import {ICardInfo} from '../../card/redux/card.types'

interface ISellKryptoDialog {
    kryptoSymbol: string
}

const SellKryptoDialog: React.FC<ISellKryptoDialog> = (props) => {

    const dispatch = useDispatch()

    // props
    const {kryptoSymbol} = props

    // Selector
    const kryptoMarketList = useSelector(marketKryptoListSelector)
    const walletList = useSelector(walletListSelector)
    const cardListOptions = useSelector(cardListSourceOptionsSelector)

    const currentAvailableAmount = useMemo(() => walletList?.[kryptoSymbol]?.availableAmount || 0, [kryptoSymbol, walletList])
    const currentPrice = useMemo(() => kryptoMarketList?.find(kyptoInfo => kyptoInfo?.symbol === kryptoSymbol)?.price, [kryptoMarketList, kryptoSymbol])

    // krypto amount local state
    const [cashToSell, setCashToSell] = useState(0)
    const [selectedSourceCard, setSelectedSourceCard] = useState<ICardInfo | string>('')

    // Handle change source card
    const handleChange = useCallback((event) => {
        setSelectedSourceCard(event.target.value)
    }, [])

    const kryptoAmountSalable = useMemo(() => {
        const amountSalable = (Number(currentAvailableAmount) * Number(currentPrice))?.toLocaleString('it-IT', {maximumFractionDigits: 2})
        return amountSalable ? amountSalable : '0'
    }, [currentPrice, currentAvailableAmount])

    const sellKrypto = useCallback(() => {

        // fetch request only if cashToSell > 0
        if (cashToSell && !!selectedSourceCard) {

            const token = Cookies.get(USER_TOKEN)

            dispatch(fetchSellKyptoAction.build({
                symbol: kryptoSymbol,
                amount: cashToSell,
                currencyCode: 'EUR'
            }, FETCH_SELL_KRYPTO_ID, undefined, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))

            dispatch(closeDialogAction())

        } else {
            dispatch(addWarning({userMessage: 'Please, enter all required fields'}))
        }

    }, [cashToSell, selectedSourceCard, dispatch, kryptoSymbol])

    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    return (
        <>
            <DialogContent>
                <Typography variant="h6" color="primary" gutterBottom> Sell {kryptoSymbol || ''} </Typography>

                <div className="row mt-2">

                    {/*Sell krypto amount*/}
                    <div className="col-12">

                        <Typography variant="caption">
                            Current price: {`${Number(currentPrice).toLocaleString()} $`}
                        </Typography> <br/>

                        <Typography variant="caption">
                            Available amount: {`${currentAvailableAmount} ${kryptoSymbol}`}
                        </Typography> <br/>

                        <Typography variant="caption">
                            Salable amount: {`${kryptoAmountSalable} $`}
                        </Typography>

                        <Typography variant="subtitle1" className="mt-2"> Cash to sell </Typography>

                    </div>

                    {/*Amount*/}
                    <div className="col-12">
                        <NumberFormat customInput={TextField}
                                      label="Amount *"
                                      className="mt-2"
                                      value={cashToSell}
                                      onValueChange={(values: NumberFormatValues) => setCashToSell(values.floatValue || 0)}
                                      displayType="input"
                                      decimalSeparator=","
                                      fixedDecimalScale
                                      decimalScale={2}
                                      thousandSeparator="."
                                      allowNegative={false}
                                      fullWidth
                                      InputProps={{
                                          endAdornment: '€'
                                      }}
                        />
                    </div>

                    {/*Destination card*/}
                    <div className="col-12">
                        <FormControl className="w-100">
                            <InputLabel htmlFor="destinationCard"> Destination card *</InputLabel>

                            <Select id="destinationCard"
                                    value={selectedSourceCard}
                                    onChange={handleChange}>

                                <MenuItem key="default" value="" disabled>Select symbol</MenuItem>

                                {cardListOptions.map((card, index: number) =>
                                    // @ts-ignore
                                    <MenuItem key={index} value={card.value}>{card.label}</MenuItem>)}

                            </Select>

                        </FormControl>
                    </div>

                </div>
            </DialogContent>

            <DialogActions>
                <div className="d-flex justify-content-between w-100 p-3">
                    <Button variant="outlined"
                            color="default"
                            onClick={closeDialog}> Close </Button>

                    <Button variant="contained"
                            color="primary"
                            onClick={sellKrypto}>
                        Sell
                    </Button>
                </div>
            </DialogActions>
        </>
    )
}

export default SellKryptoDialog
