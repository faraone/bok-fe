import * as React from 'react'
import {Dialog} from '@material-ui/core'
import {useSelector} from 'react-redux'
import {baseDialogStateSelector} from './redux/dialog.reducer'

/**
 * Generic dialogs container
 * */
export const DialogContainer: React.FC<any> = () => {

    const dialogState = useSelector(baseDialogStateSelector)
    const isDialogOpen = !!dialogState
    const DialogContentComponent = dialogState?.component

    return (
        <Dialog open={isDialogOpen}
                maxWidth={dialogState?.maxWidth || 'md'}
                fullWidth
        >
            {DialogContentComponent && <DialogContentComponent {...dialogState?.meta}/>}
        </Dialog>
    )
}
