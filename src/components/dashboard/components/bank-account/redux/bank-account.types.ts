import {ICardInfo} from '../../card/redux/card.types'
import {IGenericEntities} from 'fetch-with-redux-observable/dist/types'
import {IBankTransaction, IMoney} from '../../../dashboard.types'

export interface IBaseBankState {
    accountInfo: IBankAccountInfoResponse | null
    cardList: ICardInfo[] | null
    cardsCurrentPlainPAN: IGenericEntities<string> | null
    cardDialogDetail: ICardDialogDetailState
    bankTransaction: IBankTransaction[] | null
}

export interface ICardDialogDetailState {
    currentCardCVV: string | null
    currentPanCard: string | null
    cardPin: string | null
}

export interface IBankAccountInfoResponse {
    IBAN: string
    availableAmount: number
    bankAccountName: string
    blockedAmount: number
    currency: string
    email: string
    iban: string
    label: string
    status: string
}

export interface ICheckPaymentAmountResponse {
    available: boolean
    reason: string
}

export interface IWiredTransferRequest {
    destinationIBAN: string
    beneficiary: string
    money: IMoney
    executionDate: string
    instantTransfer: boolean
}

export interface IWireTransferResponse {
    accepted: boolean
    reason: string
}
