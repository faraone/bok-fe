import * as React from 'react'
import {connect} from 'react-redux'
import {withSnackbar, WithSnackbarProps} from 'notistack'
import {lastMessageSelector} from 'fetch-with-redux-observable/dist/user-message/user-message.reducers'
import {IUserMessage} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import Button from '@material-ui/core/Button'
import withStyles from '@material-ui/styles/withStyles'
import {IRootState} from '../../reducer'

export interface ISnackbarConsumerComponentProps extends WithSnackbarProps {
    lastMessage: IUserMessage,
    variant?: any
    messageType?: string
    time?: number
    timestamp: number
    snackBarAction?: any
    persist?: boolean
    anchorOrigin?: { horizontal: 'left' | 'center' | 'right', vertical: 'top' | 'bottom' }
}

const ButtonWhite = withStyles({
    root: {
        '&:hover': {
            background: 'rgba(255,255,255,0.1)',
            outline: 'none'
        }
    }

})(Button)

class SnackbarConsumerContainer extends React.Component<ISnackbarConsumerComponentProps> {
    public state = {
        closeCurrentSnackbar: false
    }

    public render() {
        return (
            <></>
        )
    }

    public componentDidUpdate(prevProps: Readonly<ISnackbarConsumerComponentProps>): void {

        // Inserisce il messaggio nella coda
        const {lastMessage} = this.props

        const key = lastMessage && lastMessage.userMessage && this.props.enqueueSnackbar(
            lastMessage.userMessage, {
                variant: this.props.variant,
                autoHideDuration: this.props.time,
                action: () => {
                    return <>
                        <ButtonWhite onClick={() => this.props.closeSnackbar(key)}
                                     className="text-uppercase ml-1 c-pointer text-white">
                            Close
                        </ButtonWhite>
                    </>
                }

            })
        if (this.state.closeCurrentSnackbar) {
            this.props.closeSnackbar(key)
        }
    }


    public shouldComponentUpdate(nextProps: Readonly<ISnackbarConsumerComponentProps>): boolean {

        const currentMessage = this.props.lastMessage
        const nextMessage = nextProps.lastMessage

        /**
         *  Se non c'è attualmente un messaggio (currentMessage) e ce n'è uno nuovo (nextMessage) lo visualizzo
         *  Se c'è sia il vecchio che il nuovo controllo che abbiano timestamp diversi
         */
        return !currentMessage ? !!nextMessage : nextMessage && (currentMessage.timestamp !== nextMessage.timestamp)
    }

}

export const mStP = (state: IRootState, ownProps: any) => ({
        lastMessage: lastMessageSelector(ownProps.messageType)(state),
        variant: ownProps.messageType
    }
)


// @ts-ignore
export const SnackbarConsumer = connect(mStP)(withSnackbar(SnackbarConsumerContainer))
