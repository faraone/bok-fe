import * as React from 'react'
import {useCallback, useState} from 'react'
import {CircularProgress, DialogContent, TextField} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {CARD_ID_PATH_VARIABLE, USER_TOKEN} from '../../../../../core/components/utils'
import Cookies from 'js-cookie'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {
    FETCH_GET_PLAIN_PAN_CARD_ID,
    fetchGetPlainPanCardAction,
    isFetchGetPanCardDialogPending
} from '../../card/redux/card.fetch'
import IconButton from '@material-ui/core/IconButton'
import CloseIcon from '@material-ui/icons/Close'
import CheckIcon from '@material-ui/icons/Check'
import {clearPlainPANCardAction} from '../../card/redux/card.actions'
import NumberFormat, {NumberFormatValues} from 'react-number-format'

interface IInsertCardPinForPanDialog {
    cardId: string
}

const InsertCardPinForPanDialog: React.FC<IInsertCardPinForPanDialog> = (props) => {

    const dispatch = useDispatch()

    // props
    const {cardId} = props

    const [cardPin, setCardPin] = useState<string>('')

    // Selector
    const isFetchCurrentPlainPANPending = useSelector(isFetchGetPanCardDialogPending)

    const submitGetPlainPan = useCallback(() => {

        // check entered pin
        if (cardPin) {

            // clear plain PAN card state
            dispatch(clearPlainPANCardAction())

            const token = Cookies.get(USER_TOKEN)

            dispatch(fetchGetPlainPanCardAction.build({
                pin: cardPin
            }, FETCH_GET_PLAIN_PAN_CARD_ID, {
                [CARD_ID_PATH_VARIABLE]: cardId
            }, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))

        } else {
            dispatch(addWarning({userMessage: 'Please, enter Pin'}))
        }

    }, [cardId, cardPin, dispatch])

    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    return (
        <>
            <DialogContent>
                <Typography variant="h6" color="primary" gutterBottom> Inser card PIN </Typography>

                <div className="row mt-2 mb-3">

                    <div className="col-12 col-sm-8">
                        <NumberFormat
                            customInput={TextField}
                            id="cardPin"
                            value={cardPin}
                            isNumericString
                            fullWidth
                            onValueChange={(value: NumberFormatValues) => setCardPin(value.value || '')}
                            label="PIN"
                            allowNegative={false}
                            format="#####"
                            decimalScale={0}

                        />
                    </div>
                    <div className="d-flex col-12 col-sm-4 justify-content-end">
                        {
                            isFetchCurrentPlainPANPending ?
                                <CircularProgress className="ml-2" color="inherit" size={20}/> :
                                <>
                                    {/*Close dialog*/}
                                    <IconButton
                                        aria-label="close"
                                        onClick={closeDialog}>
                                        <CloseIcon fontSize="small"/>
                                    </IconButton>

                                    {/*Submit entered pin*/}
                                    <IconButton
                                        aria-label="checkPin"
                                        disabled={isFetchCurrentPlainPANPending}
                                        onClick={submitGetPlainPan}
                                    >
                                        <CheckIcon fontSize="small"/>
                                    </IconButton>
                                </>}
                    </div>
                </div>
            </DialogContent>
        </>
    )
}

export default InsertCardPinForPanDialog
