import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton'
import MenuIcon from '@material-ui/icons/Menu'
import Typography from '@material-ui/core/Typography'
import React, {useCallback, useMemo} from 'react'
import {useDispatch, useSelector} from 'react-redux'
import {openDrawerAciont} from '../components/shared/drawer-home/redux/drawer-home.actions'
import {lastAccessDateTimeSelector, profileInfoSelector} from '../core/profile/redux/profile.reducer'
import {isFetchLastAccessInfoActionPending} from '../core/profile/redux/profile.fetch'
import {CircularProgress} from '@material-ui/core'
import moment from 'moment'
import {ReactComponent as BokWhiteLogo} from '../assets/logo-white.svg'
import {BANK_ACCOUNT_PATH, HOME_PAGE_PATH} from '../config/routing/router.constants'
import {useHistory} from 'react-router-dom'
import {DATE_TIME_CUSTOM_FORMAT} from '../core/components/utils'

interface IHomeAppBar {
}

const HomeAppBar: React.FC<IHomeAppBar> = () => {

    const dispatch = useDispatch()
    const history = useHistory()

    // Selector
    const profileInfo = useSelector(profileInfoSelector)
    const lastAccess = useSelector(lastAccessDateTimeSelector)
    const isLastAccessInfoPending = useSelector(isFetchLastAccessInfoActionPending)

    const displayName = useMemo(() => profileInfo?.fullName, [profileInfo?.fullName])
    const lastAccessDateTime = useMemo(() => moment(lastAccess).isValid() ?
        moment(lastAccess).local().format(DATE_TIME_CUSTOM_FORMAT) : '', [lastAccess])

    // Dispatch open drawer action
    const handleDrawerOpen = useCallback(() => {
        dispatch(openDrawerAciont())
    }, [dispatch])

    const goToBankAccount = useCallback(() => {
        history.push(`${HOME_PAGE_PATH}${BANK_ACCOUNT_PATH}`)
    }, [history])

    return (
        <>
            {/* Home page app bar */}
            <AppBar position="sticky" elevation={1}>
                <Toolbar style={{paddingRight: 24}}>

                    <IconButton
                        edge="start"
                        color="inherit"
                        aria-label="open drawer"
                        onClick={handleDrawerOpen}
                    >
                        <MenuIcon/>
                    </IconButton>

                    <div className="row w-100">

                        {/*Open drawer button*/}
                        <div
                            className="col flex-grow-1 d-flex align-items-center justify-content-end justify-content-sm-start c-pointer"
                            onClick={() => goToBankAccount()}
                        >

                            <div><BokWhiteLogo style={{transform: 'scale(0.75)'}}/></div>
                            <Typography component="h1" variant="h6" color="inherit" noWrap>
                                Bank Of Kaos
                            </Typography>

                        </div>

                        <div className="col flex-grow-1 d-flex flex-column justify-content-end">
                            <Typography variant="h6" color="inherit" noWrap style={{textAlign: 'right'}}>
                                {displayName}
                            </Typography>
                            <Typography variant="caption" color="inherit" noWrap style={{textAlign: 'right'}}>
                                Last access: {isLastAccessInfoPending ?
                                <CircularProgress size={20}/> : lastAccessDateTime}
                            </Typography>
                        </div>

                    </div>
                </Toolbar>
            </AppBar>

        </>
    )
}
export default HomeAppBar


