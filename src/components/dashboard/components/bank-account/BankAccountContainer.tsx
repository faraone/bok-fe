import React from 'react'
import BankAccountInfo from './components/BankAccountInfo'
import VisualizationTable from '../../../shared/visualization-table/VisualizationTable'
import {BANK_INFO_TRANSACTION_TABLE_STRUCTURE} from './bank-account.constants'
import {BankCards} from './components/BankCards'
import {useSelector} from 'react-redux'
import {allBankTransactionSelector} from './redux/bank-account.selectors'
import {useBankAccountHook} from './useBankAccountHook'
import {Skeleton} from '@material-ui/lab'

interface IBankAccountContainer {
}

const BankAccountContainer: React.FC<IBankAccountContainer> = () => {

    const bankTransactions = useSelector(allBankTransactionSelector)

    const {isBankAccountRequestsPending} = useBankAccountHook()

    return (
        isBankAccountRequestsPending ?
            <div className="col-12 d-flex justify-content-between">
                <Skeleton className="col-4 mr-4" height={50} variant="text"/>
                <Skeleton className="col-4 mr-4" variant="text"/>
            </div> : <>
                <div className="col flex-grow-1 overflow-auto mt-3">

                    {/*Bank account info*/}
                    <BankAccountInfo/>

                    {/*Bank account card*/}
                    <BankCards/>

                    {/*Bank account transaction*/}
                    <div className="row">
                        <div className="col-12">
                            <VisualizationTable
                                tableTitle="Transactions"
                                results={bankTransactions}
                                tableStructure={BANK_INFO_TRANSACTION_TABLE_STRUCTURE}
                                onRowClick={() => null}
                            />
                        </div>
                    </div>
                </div>
            </>
    )

}

export default BankAccountContainer
