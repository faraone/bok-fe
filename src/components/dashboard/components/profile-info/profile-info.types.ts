export interface IProfileInfoForm {
    email: string
    fullName: string
    icc: string
    mobile: string
}

export interface IChangePasswordRequest {
    oldPassword: string,
    newPassword: string,
}
