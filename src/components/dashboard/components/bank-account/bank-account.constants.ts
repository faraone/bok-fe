import {ITableStructure} from '../../../shared/visualization-table/VisualizationTable'

export const BANK_INFO_TRANSACTION_TABLE_STRUCTURE: ITableStructure[] = [
    {key: 'publicId', label: 'Public id'},
    {key: 'amount', label: 'Amount'},
    {key: 'type', label: 'Type'},
    {key: 'status', label: 'Status'},
    {key: 'timestamp', label: 'Date'}
]

export const NEW_CARD_INIT_VALUES = {
    label: '',
    name: '',
    type: ''
}

