import Drawer from '@material-ui/core/Drawer'
import clsx from 'clsx'
import IconButton from '@material-ui/core/IconButton'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import AccountBalanceIcon from '@material-ui/icons/AccountBalance'
import ListItemText from '@material-ui/core/ListItemText'
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet'
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart'
import Divider from '@material-ui/core/Divider'
import AccountCircleIcon from '@material-ui/icons/AccountCircle'
import React, {useCallback} from 'react'
import {useHistory} from 'react-router-dom'
import {makeStyles} from '@material-ui/core/styles'
import {BANK_ACCOUNT_ITEM, MAP_ITEM_TO_PATH, MARKETPLACE_ITEM, WALLET_ITEM} from './drawer.constants'
import {useDispatch, useSelector} from 'react-redux'
import {baseDrawerHomeSelector} from './redux/drawer-home.reducer'
import {closeDrawerAciont} from './redux/drawer-home.actions'
import ExitToAppIcon from '@material-ui/icons/ExitToApp'
import {USER_TOKEN} from '../../../core/components/utils'
import {PROFILE_INFO_PATH} from '../../../config/routing/router.constants'
import {FETCH_LOGOUT_ID, fetchLogoutAction} from '../../../login/redux/login.fetch'
import Cookies from 'js-cookie'

const drawerWidth = 240

const useStyles = makeStyles((theme) => ({
    title: {
        flexGrow: 1
    },
    drawerPaper: {
        position: 'relative',
        whiteSpace: 'nowrap',
        width: drawerWidth,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen
        })
    },
    drawerPaperClose: {
        overflowX: 'hidden',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen
        }),
        width: theme.spacing(7),
        [theme.breakpoints.up('sm')]: {
            width: theme.spacing(9)
        }
    },
    toolbarIcon: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: '0 8px',
        ...theme.mixins.toolbar
    }
}))

interface IDrawerHome {
}

// Home page drawer
const DrawerHome: React.FC<IDrawerHome> = () => {

    const history = useHistory()
    const classes = useStyles()
    const dispatch = useDispatch()

    // Drawer state selector
    const isDrawerOpen = useSelector(baseDrawerHomeSelector)?.isDrawerOpen

    // Dispatch close drawer action
    const handleDrawerClose = useCallback(() => {
        dispatch(closeDrawerAciont())
    }, [dispatch])

    // Redirecting home path on drawer item click
    const handleRedirectHomePage = useCallback((itemKey: string) => {
        if (MAP_ITEM_TO_PATH[itemKey]) {
            dispatch(closeDrawerAciont())
            history.push({
                pathname: MAP_ITEM_TO_PATH[itemKey]
            })
        }
    }, [dispatch, history])

    // Logout
    const handleLogout = useCallback(() => {

        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchLogoutAction.build(null, FETCH_LOGOUT_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

    }, [dispatch])

    return (<>
            <Drawer
                classes={{
                    paper: clsx(classes.drawerPaper, !isDrawerOpen && classes.drawerPaperClose)
                }}
                open={isDrawerOpen}
            >
                <div className={classes.toolbarIcon}>
                    <IconButton onClick={handleDrawerClose}>
                        <ChevronLeftIcon/>
                    </IconButton>
                </div>
                <div className="col justify-content-between">
                    <List>
                        <div>
                            {/* Bank account - List item*/}
                            <ListItem button onClick={() => handleRedirectHomePage(BANK_ACCOUNT_ITEM)}>
                                <ListItemIcon>
                                    <AccountBalanceIcon/>
                                </ListItemIcon>
                                <ListItemText primary="Bank account"/>
                            </ListItem>

                            {/* Krypto wallets - List item*/}
                            <ListItem button onClick={() => handleRedirectHomePage(WALLET_ITEM)}>
                                <ListItemIcon>
                                    <AccountBalanceWalletIcon/>
                                </ListItemIcon>
                                <ListItemText primary="Krypto wallets"/>
                            </ListItem>

                            {/* Krypto marketplace - List item*/}
                            <ListItem button onClick={() => handleRedirectHomePage(MARKETPLACE_ITEM)}>
                                <ListItemIcon>
                                    <ShoppingCartIcon/>
                                </ListItemIcon>
                                <ListItemText primary="Krypto marketplace"/>
                            </ListItem>

                        </div>
                    </List>

                    <Divider/>
                    <List>

                        <div>

                            {/* Profile - List item*/}
                            <ListItem button onClick={() => handleRedirectHomePage(PROFILE_INFO_PATH)}>
                                <ListItemIcon>
                                    <AccountCircleIcon/>
                                </ListItemIcon>
                                <ListItemText primary="Profile"/>
                            </ListItem>

                            {/* Logout - List item*/}
                            <ListItem button onClick={() => handleLogout()}>
                                <ListItemIcon>
                                    <ExitToAppIcon/>
                                </ListItemIcon>
                                <ListItemText primary="Logout"/>
                            </ListItem>
                        </div>
                    </List>
                </div>
            </Drawer>
        </>
    )
}

export default DrawerHome
