import React from 'react'
import WalletDetail from './components/WalletDetail'
import WalletsList from './components/WalletsList'
import {Button, Paper, Tooltip} from '@material-ui/core'
import {useSelector} from 'react-redux'
import {Skeleton} from '@material-ui/lab'
import {kryptoSymbolWalletListSelector} from './redux/krypto-wallets.selectors'
import WalletActivities from './components/WalletActivities'
import Typography from '@material-ui/core/Typography'
import AddIcon from '@material-ui/icons/Add'
import {useKryptoWalletsHook} from './useKryptoWalletsHook'
import WalletDetailTitle from './components/WalletDetailTitle'
import {useKryptoDataHook} from '../../../home/custom-hook/useKryptoDataHook'

interface IKryptoWalletsContainer {
}

const KryptoWalletsContainer: React.FC<IKryptoWalletsContainer> = () => {

    // Call custom hook with shared fetch (marketplace and wallets)
    const {isRequestsPending} = useKryptoDataHook()

    // Krypto wallet custom hook
    const {handleCreateNewWallet} = useKryptoWalletsHook()

    // Selector
    const walletList = useSelector(kryptoSymbolWalletListSelector)

    return isRequestsPending ? <div className="col-12 d-flex justify-content-between">
            <Skeleton className="col-4 mr-4" height={50} variant="text"/>
            <Skeleton className="col-4 mr-4" variant="text"/>
        </div>
        :
        <>
            <div className="row mt-3">

                {/*PAGE TITLE*/}
                <div className="col">
                    <Typography variant="h5" color="primary" gutterBottom>
                        Krypto Wallets
                    </Typography>
                </div>

                {/*CREATE NEW WALLET BUTTON*/}
                <div className="col d-flex justify-content-end">
                    <Tooltip title="Create wallet">
                        <Button
                            color="primary"
                            onClick={handleCreateNewWallet}
                            endIcon={<AddIcon className="mr-3 c-pointer" color="primary"/>}>
                            Create
                        </Button>

                    </Tooltip>
                </div>
            </div>

            <div className="col flex-grow-1 overflow-auto mt-3">

                {/*Krypto wallets list*/}
                <WalletsList/>

                {/*Wallet details*/}
                {Array.isArray(walletList) && walletList?.length > 0 && <Paper elevation={3} className="pr-3 pl-3 mb-3">

                    {/*Wallet detail title - symbol and action*/}
                    <WalletDetailTitle/>

                    {/*Wallet detail*/}
                    <WalletDetail/>

                    {/*Wallet activites*/}
                    <WalletActivities/>

                </Paper>}
            </div>
        </>
}

export default KryptoWalletsContainer
