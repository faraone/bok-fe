import * as React from 'react'
import {useCallback, useState} from 'react'
import {Button, CircularProgress, DialogContent, TextField} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'
import {
    FETCH_CLOSE_ACCOUNT_ID,
    fetchCloseAccountAction,
    isFetchCloseAccountActionPending
} from '../../../../../core/profile/redux/profile.fetch'
import {profileInfoSelector} from '../../../../../core/profile/redux/profile.reducer'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'

interface IConfirmDeleteAccountDialog {
}

const ConfirmDeleteAccountDialog: React.FC<IConfirmDeleteAccountDialog> = () => {

    const dispatch = useDispatch()

    const [destinationIBAN, setDestinationIBAN] = useState<string>('')

    const isDeleteAccountPending = useSelector(isFetchCloseAccountActionPending)
    const emailProfile = useSelector(profileInfoSelector)?.email

    // Close dialogs callback
    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    // Submit delete account
    const requestDeleteAccount = useCallback(() => {

        if (destinationIBAN) {

            const token = Cookies.get(USER_TOKEN)

            dispatch(fetchCloseAccountAction.build({
                email: emailProfile,
                iban: destinationIBAN
            }, FETCH_CLOSE_ACCOUNT_ID, undefined, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))
        } else {
            dispatch(addWarning({userMessage: 'Please enter IBAN value'}))
        }
    }, [destinationIBAN, dispatch, emailProfile])

    return (
        <>

            <DialogContent>

                <Typography variant="h5" color="primary" gutterBottom> Confirm </Typography>

                <Typography variant="body1"> Are you sure about closing account? </Typography>

                <Typography variant="caption"> Enter IBAN on which to credit the remaining balance </Typography>

                {/*Destination IBAN*/}
                <TextField id="destinationIBAN"
                           value={destinationIBAN}
                           fullWidth
                           onChange={(event) => setDestinationIBAN(event.target.value)}
                           label="Destination IBAN"/>

                <div className="d-flex justify-content-between w-100 p-3">
                    <Button variant="outlined" color="default" onClick={closeDialog}> No </Button>
                    <Button variant="contained" color="primary" onClick={requestDeleteAccount}
                            disabled={isDeleteAccountPending}>
                        {
                            isDeleteAccountPending &&
                            <CircularProgress color="inherit" size={20} className="mr-2"/>
                        }
                        Yes
                    </Button>
                </div>
            </DialogContent>

        </>
    )
}

export default ConfirmDeleteAccountDialog
