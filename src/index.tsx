import 'bootstrap/dist/css/bootstrap.min.css'
import './App.scss'
import * as React from 'react'
import * as ReactDOM from 'react-dom'
import App from './App'
import {Provider} from 'react-redux'
import getConfiguredStore from './reducer'
import {MuiThemeProvider} from '@material-ui/core'
import {ThemeProvider} from '@material-ui/styles'
import {themeSettings} from './material.theme'
import {FetchProvider} from 'react-fetch-it-hook'
import {setBaseRequestURL} from 'fetch-with-redux-observable'
import {SnackbarProvider} from 'notistack'
import MomentUtils from '@date-io/moment'
import {MuiPickersUtilsProvider} from '@material-ui/pickers'
import {createBrowserHistory} from 'history'
import {verifyTokenIfPresent} from './initConfig'
import {unregister} from './serviceWorkers'

/** Browser router history instance */
export const BrowserHistory = createBrowserHistory()

/** Set base request URL */
setBaseRequestURL({
    devUrl: process.env.REACT_APP_SERVER_URL || '.',
    prodUrl: process.env.REACT_APP_SERVER_URL || '.',
    retryStrategy: {attempts: 0, delayMs: 0},
    headers: {}
})

/**
 * Check token if present and then call React render method
 * */
verifyTokenIfPresent().then((tokenInitState) => {
    ReactDOM.render(
        <React.StrictMode>
            <Provider store={getConfiguredStore()(tokenInitState)}>
                <SnackbarProvider maxSnack={3} anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}>
                    <MuiThemeProvider theme={themeSettings}>
                        <ThemeProvider theme={themeSettings}>
                            <MuiPickersUtilsProvider utils={MomentUtils} locale={'it'}>
                                <FetchProvider value={{basePath: '.', logLevel: 'INFO'}}>
                                    <App/>
                                </FetchProvider>
                            </MuiPickersUtilsProvider>
                        </ThemeProvider>
                    </MuiThemeProvider>
                </SnackbarProvider>
            </Provider>
        </React.StrictMode>, document.getElementById('root'))
})

// unregister service worker
unregister()
