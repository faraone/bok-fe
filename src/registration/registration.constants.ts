/**
 * Registration form names
 * */
export const REGISTRATION_FORM_NAMES = {
    name: 'name',
    surname: 'surname',
    birthdate: 'birthdate',
    business: 'business',
    fiscalCode: 'fiscalCode',
    gender: 'gender',
    middleName: 'middleName',
    vatNumber: 'vatNumber',
    icc: 'icc',
    number: 'number',
    email: 'email',
    city: 'city',
    country: 'country',
    county: 'county',
    houseNumber: 'houseNumber',
    postalCode: 'postalCode',
    street: 'street'
}

export const INIT_VALUES_REGISTRATION = {
    name: '',
    surname: '',
    birthdate: null,
    business: false,
    fiscalCode: '',
    gender: '',
    middleName: '',
    vatNumber: '',
    icc: '',
    number: '',
    email: '',
    city: '',
    country: '',
    county: '',
    houseNumber: '',
    postalCode: '',
    street: ''
}

// Success message registration
export const SUCCESS_REGISTRATION_MESSAGE = 'Account registered. Please check your email to confirm account (also spam)'

export const REGISTRATION_KEY_PAYLOAD = {
    address: 'address',
    credentials: 'credentials',
    mobile: 'mobile'
}
