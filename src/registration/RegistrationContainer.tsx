import React, {useCallback, useEffect} from 'react'
import Avatar from '@material-ui/core/Avatar'
import LockOutlinedIcon from '@material-ui/icons/LockOutlined'
import Typography from '@material-ui/core/Typography'
import RegistrationFormContainer from './RegistrationFormContainer'
import {Formik} from 'formik'
import {INIT_VALUES_REGISTRATION, REGISTRATION_FORM_NAMES, REGISTRATION_KEY_PAYLOAD} from './registration.constants'
import {IRegistrationForm} from './registration.types'
import {registrationValidator} from './registration.validators'
import {useDispatch, useSelector} from 'react-redux'
import {FETCH_REGISTRATION_ID, fetchRegistrationAction} from './redux/registration.fetch'
import {clearRedirectPathAction} from '../core/components/utils'
import {useHistory} from 'react-router-dom'
import {baseRedirectingPathSelector} from '../core/components/redux/redirect.reducer'
import {parsePhoneNumber} from 'react-phone-number-input'
import {Grid} from '@material-ui/core'
import {makeStyles} from '@material-ui/core/styles'

interface IRegistrationProps {
}

const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh'
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/DeMmn2iaFLI/1920x1277)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        opacity: '0.4'
    }
}))

export const RegistrationContainer: React.FC<IRegistrationProps> = () => {

    // Style
    const classes = useStyles()

    const dispatch = useDispatch()
    const history = useHistory()

    const pathRedirecting = useSelector(baseRedirectingPathSelector)?.path

    useEffect(() => {
        if (pathRedirecting) {
            dispatch(clearRedirectPathAction())
            history.push(pathRedirecting)
        }
    }, [dispatch, history, pathRedirecting])

    const getPayload = useCallback((values: IRegistrationForm) => {

        const phoneNumber = parsePhoneNumber(values?.number)

        return {
            [REGISTRATION_KEY_PAYLOAD.address]: {
                [REGISTRATION_FORM_NAMES.city]: values?.city || '',
                [REGISTRATION_FORM_NAMES.country]: values?.country || '',
                [REGISTRATION_FORM_NAMES.county]: values?.county || '',
                [REGISTRATION_FORM_NAMES.houseNumber]: values?.houseNumber || '',
                [REGISTRATION_FORM_NAMES.postalCode]: values?.postalCode || '',
                [REGISTRATION_FORM_NAMES.street]: values?.street || ''
            },
            [REGISTRATION_FORM_NAMES.birthdate]: values?.birthdate?.isValid() ? values?.birthdate?.toISOString() : null,
            [REGISTRATION_FORM_NAMES.business]: values?.business,

            [REGISTRATION_KEY_PAYLOAD.credentials]: {
                [REGISTRATION_FORM_NAMES.email]: values?.email || ''
            },
            [REGISTRATION_FORM_NAMES.fiscalCode]: values.business ? '' : values?.fiscalCode,
            [REGISTRATION_FORM_NAMES.gender]: values?.gender || '',
            [REGISTRATION_FORM_NAMES.middleName]: values?.middleName || '',

            [REGISTRATION_KEY_PAYLOAD.mobile]: {
                [REGISTRATION_FORM_NAMES.icc]: phoneNumber?.countryCallingCode ? `+${phoneNumber?.countryCallingCode}` : '',
                [REGISTRATION_FORM_NAMES.number]: phoneNumber?.nationalNumber || ''
            },
            [REGISTRATION_FORM_NAMES.name]: values?.name || '',
            [REGISTRATION_FORM_NAMES.surname]: values.business ? '' : values?.surname,
            [REGISTRATION_FORM_NAMES.vatNumber]: values.business ? values?.vatNumber : ''
        }
    }, [])

    const handleSubmit = useCallback(async (values: IRegistrationForm) => {
        await dispatch(fetchRegistrationAction.build(
            getPayload(values), FETCH_REGISTRATION_ID
        ))
    }, [dispatch, getPayload])

    return <Grid container component="main" className={classes.root}>

        <Grid xs={false} sm={4} md={7} className={classes.image}/>

        <Grid xs={12} sm={8} md={5} component="div">

            <div className="col d-flex flex-column align-items-center mt-4">
                <Avatar className="bg-primary">
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    BOK - Sign up
                </Typography>

                <Formik
                    initialValues={INIT_VALUES_REGISTRATION}
                    onSubmit={handleSubmit}
                    validate={registrationValidator}
                    component={RegistrationFormContainer}
                    validateOnBlur={false}
                />

            </div>

        </Grid>
    </Grid>
}
