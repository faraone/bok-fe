import {Moment} from 'moment'

export interface IRegistrationForm {
    name: string
    surname: string
    birthdate: Moment | null
    business: boolean
    fiscalCode: string
    gender: string
    middleName: string
    vatNumber: string
    // mobile
    icc: string
    number: string
    // credentials
    email: string
    // address
    city: string
    country: string
    county: string
    houseNumber: string
    postalCode: string
    street: string
}

// CredentialsDTO
export interface ICredentials {
    email: string
    password: string
}

// AddressDTO
export interface IAddress {
    city: string
    country: string
    county: string
    houseNumber: string
    postalCode: string
    street: string
}

// MobileDTO
export interface IMobile {
    icc: string
    number: string
}

// body request in registration fetch
export interface IAcoountRegistration {
    address: IAddress
    birthdate: string
    business: boolean
    credentials: ICredentials
    fiscalCode: string
    gender: string
    middleName: string
    mobile: IMobile
    name: string
    surname: string
    vatNumber: string
}

// Gender options
export enum EGender {
    MALE = 'M',
    FEMALE = 'F'
}
