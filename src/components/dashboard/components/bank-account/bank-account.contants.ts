import {ECardType} from '../card/redux/card.types'
import {IGenericEntities} from 'fetch-with-redux-observable/dist/types'


export const CARD_TYPE_LABEL: IGenericEntities<string> = {
    [ECardType.CREDIT]: 'Credit',
    [ECardType.DEBIT]: 'Debit',
    [ECardType.VIRTUAL]: 'Virtual',
    [ECardType.ONE_USE]: 'One use'
}
