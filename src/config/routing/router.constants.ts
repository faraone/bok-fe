export const HOME_PAGE_PATH = '/home'
export const LOGIN_PATH = '/login'
export const REGISTRATION_PATH = '/registration'
export const RESET_PASSWORD_PATH = '/resetPassword'

export const BANK_ACCOUNT_PATH = '/bank'
export const WALLETS_PATH = '/wallets'
export const KRYPTO_MARKETPLACE_PATH = '/marketplace'
export const PROFILE_INFO_PATH = '/profile'

