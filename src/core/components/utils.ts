import Cookies from 'js-cookie'
import {ECardStatus, ECardType} from '../../components/dashboard/components/card/redux/card.types'

// Cookies user token key
export const USER_TOKEN = 'BOK_USER_TOKEN'

// Email value remember me session storage item key
export const EMAIL_REMEMBER_ME_KEY = 'EMAIL_REMEMBER_ME_KEY'

// path variable
export const CARD_ID_PATH_VARIABLE = 'cardId'
export const KRYPTO_SYMBOL_PATH_VARIABLE = 'symbol'
export const CARD_STATUS_PATH_VARIABLE = 'status'

// date time format visualization
export const DATE_TIME_CUSTOM_FORMAT = 'DD/MM/YYYY HH:mm'

// Redirect action
export const REDIRECT_PATH_ACTION = 'REDIRECT_PATH_ACTION'
export const redirectPathAction = (path: string) => ({type: REDIRECT_PATH_ACTION, payload: path})

export const CLEAR_REDIRECT_PATH_ACTION = 'CLEAR_REDIRECT_PATH_ACTION'
export const clearRedirectPathAction = () => ({type: CLEAR_REDIRECT_PATH_ACTION})

// Get token if present
export const getTokenCookies = (): string | undefined => {
    return Cookies.get(USER_TOKEN)
}

export const getTokenCookiesFunction = () => (): string | undefined => {
    return Cookies.get(USER_TOKEN)
}

// Set token
export const setTokenCookies = (token: string) => {
    token && Cookies.set(USER_TOKEN, token)
}

// Remove token
export const removeTokenCookies = () => {
    Cookies.remove(USER_TOKEN)
}

// Generate chart data
export const createChartData = (time: string, amount: number | undefined) => ({time, amount})

// return capitalized string
export const toCapitalizeString = (string: string) => {
    const camelCaseString = string?.toLowerCase()
    return camelCaseString?.charAt(0)?.toUpperCase() + camelCaseString?.slice(1)
}

// Minimum password length
export const MINIMUM_PASSWORD_LENGTH = 8

// Map card type
export const MAP_CARD_TYPE: { [key: string]: string } = {
    [ECardType.CREDIT]: 'Credit',
    [ECardType.DEBIT]: 'Debit',
    [ECardType.VIRTUAL]: 'Virtual',
    [ECardType.ONE_USE]: 'One use'
}

// Map card type
export const MAP_CARD_STATUS: { [key: string]: string } = {
    [ECardStatus.ACTIVE]: 'Active',
    [ECardStatus.TO_ACTIVATE]: 'To activate',
    [ECardStatus.DEACTIVATED]: 'Deactivated',
    [ECardStatus.LOST]: 'Lost',
    [ECardStatus.STOLEN]: 'Stolen',
    [ECardStatus.BROKEN]: 'Broken',
    [ECardStatus.DESTROYED]: 'Destroyed',
    [ECardStatus.EXPIRED]: 'Expired',
}

export const MAP_CARD_STATUS_COLOR: { [key: string]: string } = {
    [ECardStatus.ACTIVE]: '#069C3F',
    [ECardStatus.TO_ACTIVATE]: '#FADA4D',
    [ECardStatus.DEACTIVATED]: '#FADA4D',
    [ECardStatus.LOST]: '#349BF7',
    [ECardStatus.STOLEN]: '#349BF7',
    [ECardStatus.BROKEN]: '#AB120A',
    [ECardStatus.DESTROYED]: '#D96930',
    [ECardStatus.EXPIRED]: '#AB120A'
}

export const MAP_CURRENCY: { [key: string]: string } = {
    'EUR': '€',
    'USD': '$'
}
