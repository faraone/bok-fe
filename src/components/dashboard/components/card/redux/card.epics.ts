import {Observable} from 'rxjs'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {combineEpics, ofType} from 'redux-observable'
import {filter, mergeMap} from 'rxjs/operators'
import {
    FETCH_ALL_USER_CARDS_HOME_ID,
    FETCH_ALL_USER_CARDS_ID,
    FETCH_GET_PLAIN_PAN_CARD_ID,
    fetchActivateCardAction,
    fetchAllUserCardsAction,
    fetchCardPINAction,
    fetchChangeCardStatusAction,
    fetchCreateCardAction,
    fetchDeleteCardAction,
    fetchGetCardTokenAction,
    fetchGetPlainPanCardAction
} from './card.fetch'
import {addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {FETCH_BUY_KRYPTO_ID, fetchBuyKryptoAction} from '../../krypto-marketplace/redux/krypto-marketplace.fetch'

/**
 * on create new card fetch action success, dispatch card list fetch action
 * to update displayed list
 * */
export const createCardSuccessEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchCreateCardAction.successActionType),
        mergeMap(() => {
                const token = Cookies.get(USER_TOKEN)

                return [
                    addSuccess({userMessage: 'Card created. Check your email to active it'}),
                    fetchAllUserCardsAction.build(null, FETCH_ALL_USER_CARDS_ID, undefined, undefined, undefined, {
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Content-Type': 'application/json'
                        }
                    }),
                    closeDialogAction()
                ]
            }
        )
    )


/**
 * on get card token success, build buy fetch action
 * */
export const getCardTokenAndBuyRequestEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchGetCardTokenAction.successActionType),
        mergeMap((action) => {

                const cardToken = action?.payload

                const {
                    symbol,
                    amount,
                    currencyCode
                } = (action.meta.meta as any)

                const token = Cookies.get(USER_TOKEN)

                return [
                    fetchBuyKryptoAction.build({
                        symbol,
                        amount,
                        currencyCode,
                        cardToken
                    }, FETCH_BUY_KRYPTO_ID, undefined, undefined, undefined, {
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Content-Type': 'application/json'
                        }
                    })
                ]
            }
        )
    )

/**
 * on card delete success, close dialog and update card list
 * */
export const updateCardListAfterDeleteEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchDeleteCardAction.successActionType),
        mergeMap((action) => {

                const payloadMessage = action?.payload

                const token = Cookies.get(USER_TOKEN)

                return [
                    closeDialogAction(),
                    fetchAllUserCardsAction.build(null, FETCH_ALL_USER_CARDS_HOME_ID, undefined, undefined, undefined, {
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Content-Type': 'application/json'
                        }
                    }),
                    addSuccess({userMessage: payloadMessage || ''})
                ]
            }
        )
    )

/**
 * on card change state success, close dialog and update card list
 * */
export const changeCardStateSuccessEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchChangeCardStatusAction.successActionType,
            fetchActivateCardAction.successActionType),
        mergeMap((action) => {

                const payloadMessage = action?.payload

                const token = Cookies.get(USER_TOKEN)

                return [
                    closeDialogAction(),
                    fetchAllUserCardsAction.build(null, FETCH_ALL_USER_CARDS_HOME_ID, undefined, undefined, undefined, {
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Content-Type': 'application/json'
                        }
                    }),
                    addSuccess({userMessage: payloadMessage || ''})
                ]
            }
        )
    )

/**
 * close pin dialog after fetchGetPlainPanCardAction success
 * */
export const closePinDialogOnGetPanSuccessEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchGetPlainPanCardAction.successActionType),
        filter(action => action.meta.meta.requestId === FETCH_GET_PLAIN_PAN_CARD_ID),
        mergeMap(() => [closeDialogAction()])
    )

/**
 * close pin dialog after fetchGetPlainPanCardAction success
 * */
export const getPinMessageSuccessEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchCardPINAction.successActionType),
        mergeMap(() => [addSuccess({userMessage: 'Please check your email, the PIN has been send'})])
    )

export const cardEpics = combineEpics(
    createCardSuccessEpic,
    getCardTokenAndBuyRequestEpic,
    updateCardListAfterDeleteEpic,
    changeCardStateSuccessEpic,
    closePinDialogOnGetPanSuccessEpic,
    getPinMessageSuccessEpic
)
