import {useEffect, useMemo} from 'react'
import Cookies from 'js-cookie'
import {clearRedirectPathAction, USER_TOKEN} from '../../../core/components/utils'
import {
    FETCH_LAST_ACCESS_INFO_ID,
    FETCH_PROFILE_INFO_ID,
    fetchLastAccessInfoAction,
    fetchProfileInfoAction,
    isFetchLastAccessInfoActionPending,
    isFetchProfileInfoActionPending
} from '../../../core/profile/redux/profile.fetch'
import {LOGIN_PATH} from '../../../config/routing/router.constants'
import {useDispatch, useSelector} from 'react-redux'
import {useHistory} from 'react-router-dom'
import {isAuthenticatedSelector} from '../../../core/profile/redux/profile.reducer'
import {baseRedirectingPathSelector} from '../../../core/components/redux/redirect.reducer'

/**
 * Home container custom hook. Get profile info and listen to path redirecting action
 * */
export const useHomeContainerHook = () => {

    const dispatch = useDispatch()
    const history = useHistory()

    // Selector
    const isAuthenticated = useSelector(isAuthenticatedSelector)
    const pathRedirecting = useSelector(baseRedirectingPathSelector)?.path
    const isProfilePending = useSelector(isFetchProfileInfoActionPending)
    const isLastAccessPending = useSelector(isFetchLastAccessInfoActionPending)

    useEffect(() => {
        if (pathRedirecting) {
            dispatch(clearRedirectPathAction())
            history.push(pathRedirecting)
        }
    }, [dispatch, history, pathRedirecting])

    // did mount - Fetch profile info
    useEffect(() => {
        if (isAuthenticated) {

            const token = Cookies.get(USER_TOKEN)

            /** PROFILE DATA FETCH ACTION */
            // Fetch profile info
            dispatch(fetchProfileInfoAction.build(null, FETCH_PROFILE_INFO_ID, undefined, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))
            // Fetch last access info
            dispatch(fetchLastAccessInfoAction.build(null, FETCH_LAST_ACCESS_INFO_ID, undefined, undefined, undefined, {
                headers: {
                    'Authorization': `Bearer ${token}`,
                    'Content-Type': 'application/json'
                }
            }))

        } else {
            history.push(LOGIN_PATH)
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    const isRequestsPending = useMemo(() =>
        isProfilePending ||
        isLastAccessPending
        , [isLastAccessPending, isProfilePending])

    return {isRequestsPending}
}
