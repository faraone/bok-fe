import {HOME_PAGE_PATH, LOGIN_PATH, REGISTRATION_PATH, RESET_PASSWORD_PATH} from './router.constants'
import {RegistrationContainer} from '../../registration/RegistrationContainer'
import LoginContainer from '../../login/LoginContainer'
import HomeContainer from '../../components/home/HomeContainer'
import ResetPasswordContainer from '../../reset-password/ResetPasswordContainer'

interface IRoute {
    component: any
    path: string
    isPrivate: boolean
    subroute?: IRoute
}

export const ROUTE: IRoute[] = [
    {
        component: LoginContainer,
        path: LOGIN_PATH,
        isPrivate: false
    }, {
        component: RegistrationContainer,
        path: REGISTRATION_PATH,
        isPrivate: false
    }, {
        component: ResetPasswordContainer,
        path: RESET_PASSWORD_PATH,
        isPrivate: false
    }, {
        component: HomeContainer,
        path: HOME_PAGE_PATH,
        isPrivate: true
    }
]

export default ROUTE
