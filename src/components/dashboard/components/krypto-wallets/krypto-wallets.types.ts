import {IWallet} from '../../dashboard.types'

export interface IWalletsResponse {
    wallets: IWallet[]
}

export interface IWalletEntities {
    [key: string]: IWallet
}
