// Reset password fetch action
import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {FETCH_CHECK_RESET_PASSWORD_NEEDED_API, FETCH_RESET_PASSWORD_API} from '../../core/fetch/fetch.constants'

// Request reset password
export const FETCH_RESET_PASSWORD = 'FETCH_RESET_PASSWORD'
export const FETCH_RESET_PASSWORD_ID = 'FETCH_RESET_PASSWORD_ID'
export const fetchResetPasswordAction = fetchActionFactory(FETCH_RESET_PASSWORD_API, FETCH_RESET_PASSWORD)
export const isFetchResetPasswordPending = isRequestInPending(fetchResetPasswordAction.pendingActionTypeWithSpinner, FETCH_RESET_PASSWORD_ID)

// Check if reset password is needed
export const FETCH_RESET_NEEDED_PASSWORD = 'FETCH_RESET_NEEDED_PASSWORD'
export const FETCH_RESET_NEEDED_PASSWORD_ID = 'FETCH_RESET_NEEDED_PASSWORD_ID'
export const fetchResetPasswordNeededAction = fetchActionFactory(FETCH_CHECK_RESET_PASSWORD_NEEDED_API, FETCH_RESET_NEEDED_PASSWORD)
export const isFetchResetPasswordNeededPending = isRequestInPending(fetchResetPasswordNeededAction.pendingActionTypeWithSpinner, FETCH_RESET_NEEDED_PASSWORD_ID)
