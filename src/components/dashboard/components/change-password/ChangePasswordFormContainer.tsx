import * as React from 'react'
import {useCallback, useState} from 'react'
import {Field, Form, Formik} from 'formik'
import {TextField} from 'formik-material-ui'
import InputAdornment from '@material-ui/core/InputAdornment'
import {CircularProgress, IconButton} from '@material-ui/core'
import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'
import Button from '@material-ui/core/Button'
import {useDispatch, useSelector} from 'react-redux'
import {fetchChangePasswordAction, isFetchChangePasswordPending} from '../../../../login/redux/login.fetch'
import {CHANGE_PASSWORD_FORM_NAMES, CHANGE_PASSWORD_INIT_VALUES, IChangePasswordForm} from './change-password.constants'
import {MINIMUM_PASSWORD_LENGTH, USER_TOKEN} from '../../../../core/components/utils'
import {FormikHelpers} from 'formik/dist/types'
import Cookies from 'js-cookie'
import {sha256} from 'js-sha256'
import {FETCH_PROFILE_INFO_ID} from '../../../../core/profile/redux/profile.fetch'

interface IChangePasswordFormContainer {
}

const ChangePasswordFormContainer: React.FC<IChangePasswordFormContainer> = () => {

    const dispatch = useDispatch()

    const isChangePasswordInPending = useSelector(isFetchChangePasswordPending)

    // show password field local states
    const [showOldPassword, setShowOldPassword] = useState<boolean>(false)
    const [showNewPassword1, setShowNewPassword1] = useState<boolean>(false)
    const [showNewPassword2, setShowNewPassword2] = useState<boolean>(false)

    const handleSubmit = useCallback((values: IChangePasswordForm, formikHelper: FormikHelpers<IChangePasswordForm>) => {

        const token = Cookies.get(USER_TOKEN)

        const payload = {
            oldPassword: sha256(values?.oldPassword),
            newPassword: sha256(values?.newPassword1)
        }

        // Fetch change password
        dispatch(fetchChangePasswordAction.build(payload, FETCH_PROFILE_INFO_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))

        // Clear form
        formikHelper.setSubmitting(false)
        formikHelper.resetForm()
    }, [dispatch])

    // Validate form callback
    const validateForm = useCallback((values: IChangePasswordForm) => {

        const errors = {}

        if (values.oldPassword === '') {
            // @ts-ignore
            errors[CHANGE_PASSWORD_FORM_NAMES.oldPassword] = 'Insert old password'

            /** Validate strong password */
        } else if (
            // minimum length
            values.newPassword1.length < MINIMUM_PASSWORD_LENGTH ||
            // at least one upper case char
            values.newPassword1.toLowerCase() === values.newPassword1 ||
            // at least a number
            !/\d/.test(values.newPassword1) ||
            // at least one special char
            !/([#?!@$%^&.,;()*-])/.test(values.newPassword1)
        ) {
            // @ts-ignore
            errors[CHANGE_PASSWORD_FORM_NAMES.newPassword1] = <ul>
                Insert a valid password
                <li>Minimum length {MINIMUM_PASSWORD_LENGTH}</li>
                <li>At least one number</li>
                <li>At least one special char ([#?!@$%^&.,;()*-])</li>
            </ul>
        } else if (values.newPassword1 !== values.newPassword2) {
            // @ts-ignore
            errors[CHANGE_PASSWORD_FORM_NAMES.newPassword2] = 'Passwords doesn\'t match'
        }

        return errors

    }, [])

    return <Formik
        initialValues={CHANGE_PASSWORD_INIT_VALUES}
        onSubmit={handleSubmit}
        validate={validateForm}
        validateOnChange={false}
        validateOnBlur={false}
    >

        {() => <Form>
            <div className="row align-items-center">
                {/*Old password*/}
                <div className="col-12 col-sm-4 mt-1 align-self-start">
                    <Field
                        component={TextField}
                        label="Old password"
                        fullWidth
                        name={CHANGE_PASSWORD_FORM_NAMES.oldPassword}
                        type={showOldPassword ? 'text' : 'password'}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() => setShowOldPassword(!showOldPassword)}
                                >
                                    {showOldPassword ? <Visibility fontSize="small"/> :
                                        <VisibilityOff fontSize="small"/>}
                                </IconButton>
                            </InputAdornment>
                        }}
                    />
                </div>

                {/*New password*/}
                <div className="col-12 col-sm-4 mt-1 align-self-start">
                    <Field
                        component={TextField}
                        label="New password"
                        fullWidth
                        name={CHANGE_PASSWORD_FORM_NAMES.newPassword1}
                        type={showNewPassword1 ? 'text' : 'password'}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() => setShowNewPassword1(!showNewPassword1)}
                                >
                                    {showNewPassword1 ? <Visibility fontSize="small"/> :
                                        <VisibilityOff fontSize="small"/>}
                                </IconButton>
                            </InputAdornment>
                        }}
                    />
                </div>

                {/*Confirm new password*/}
                <div className="col-12 col-sm-4 mt-1 align-self-start">
                    <Field
                        component={TextField}
                        label="Confirm password"
                        fullWidth
                        name={CHANGE_PASSWORD_FORM_NAMES.newPassword2}
                        type={showNewPassword2 ? 'text' : 'password'}
                        InputProps={{
                            endAdornment: <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() => setShowNewPassword2(!showNewPassword2)}
                                >
                                    {showNewPassword2 ? <Visibility fontSize="small"/> :
                                        <VisibilityOff fontSize="small"/>}
                                </IconButton>
                            </InputAdornment>
                        }}
                    />
                </div>

                {/*Submit change password*/}
                <div className="d-flex col-12 mt-3 justify-content-end">
                    <Button
                        color="primary"
                        variant="outlined"
                        disabled={isChangePasswordInPending}
                        type="submit">
                        {isChangePasswordInPending &&
                        <CircularProgress size={20} className="mr-2" color="inherit"/>}
                        Confirm
                    </Button>
                </div>
            </div>

        </Form>}
    </Formik>
}

export default ChangePasswordFormContainer
