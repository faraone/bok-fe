import {IAction} from 'fetch-with-redux-observable'
import {CLOSE_DRAWER_HOME, OPEN_DRAWER_HOME} from './drawer-home.actions'
import {Selector} from 'reselect'
import {IRootState} from '../../../../reducer'

export interface IDrawerState {
    isDrawerOpen: boolean
}

// handle changing drawer action
export const drawerHomeReducer = (state: IDrawerState = {isDrawerOpen: false}, action: IAction): IDrawerState => {
    switch (action.type) {
        case OPEN_DRAWER_HOME:
            return {isDrawerOpen: true}
        case CLOSE_DRAWER_HOME:
            return {isDrawerOpen: false}
        default:
            return state
    }
}

// base drawer state selector
export const baseDrawerHomeSelector: Selector<IRootState, IDrawerState> = (state: IRootState): IDrawerState => state.drawer
