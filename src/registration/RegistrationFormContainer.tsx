import * as React from 'react'
import {ErrorMessage, Field, Form, useFormikContext} from 'formik'
import Button from '@material-ui/core/Button'
import {REGISTRATION_FORM_NAMES} from './registration.constants'
import {
    CircularProgress,
    FormControlLabel,
    FormHelperText,
    FormLabel,
    Link,
    makeStyles,
    Radio,
    Typography
} from '@material-ui/core'
import Grid from '@material-ui/core/Grid'
import {LOGIN_PATH} from '../config/routing/router.constants'
import {CheckboxWithLabel, RadioGroup, TextField} from 'formik-material-ui'
import {Validators} from '../shared/form-validators'
import {EGender, IRegistrationForm} from './registration.types'
import {useSelector} from 'react-redux'
import {isFetchRegistrationPending} from './redux/registration.fetch'
import {KeyboardDatePicker} from 'formik-material-ui-pickers'
import PhoneInputField from '../shared/PhoneInputField'


const useStyles = makeStyles((theme) => ({
    wrapper: {
        position: 'relative'
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    },
    datePicker: {padding: 0}
}))


const RegistrationFormContainer: React.FC = () => {

    // Style
    const classes = useStyles()

    // Formik
    const {values, errors, touched} = useFormikContext<IRegistrationForm>()

    // Selector
    const isRegistrationPending = useSelector(isFetchRegistrationPending)

    return <Form className={classes.form}>
        <Grid container spacing={2} className="px-4 mb-2">

            {/*Name*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.name}
                    label="Name*"
                    fullWidth
                    validate={Validators.required}
                />
            </Grid>

            {/*Surname*/}
            {!values.business && <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.surname}
                    label="Surname*"
                    fullWidth
                    validate={!values.business && Validators.required}
                />
            </Grid>}

            {/*Business*/}
            <Grid item sm={12}>
                <FormLabel component="legend">Account type</FormLabel>
                <Field
                    name={REGISTRATION_FORM_NAMES.business}
                    component={CheckboxWithLabel}
                    type="checkbox"
                    Label={{
                        label: 'Business'
                    }}
                    size="small"
                />
            </Grid>

            {/*Birthday*/}
            {!values.business && <Grid item xs={12} sm={6}>
                <Field
                    name={REGISTRATION_FORM_NAMES.birthdate}
                    component={KeyboardDatePicker}
                    size="small"
                    variant="inline"
                    format="DD/MM/YYYY"
                    margin="normal"
                    label="Birthday*"
                    disableFuture
                    autoOk
                    validate={Validators.isAdult}
                    KeyboardButtonProps={{className: classes.datePicker}}/>
            </Grid>}

            {/*Gender*/}
            {!values.business && <Grid item sm={6}>
                <Field
                    component={RadioGroup}
                    name={REGISTRATION_FORM_NAMES.gender}
                    validate={Validators.required}
                    row
                >
                    <FormLabel component="legend"
                               error={!!touched.gender && !!errors.gender}> Gender* </FormLabel>
                    <FormControlLabel
                        value={EGender.FEMALE}
                        control={<Radio size="small"/>}
                        label="Female"
                    />
                    <FormControlLabel
                        value={EGender.MALE}
                        control={<Radio size="small"/>}
                        label="Male"
                    />
                </Field>

                <ErrorMessage name={REGISTRATION_FORM_NAMES.gender}
                              render={(message) => <FormHelperText
                                  error>
                                  {message}
                              </FormHelperText>}/>
            </Grid>}

            {/*Fiscal code or Vat number*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={values.business ? REGISTRATION_FORM_NAMES.vatNumber : REGISTRATION_FORM_NAMES.fiscalCode}
                    label={values.business ? 'Vat number*' : 'Fiscal code*'}
                    fullWidth
                    validate={Validators.required}
                />
            </Grid>

            {/*ICC - Mobile number*/}
            <Grid item xs={12} sm={6}>
                <Field
                    name={REGISTRATION_FORM_NAMES.number}
                    label="Mobile number*"
                    component={PhoneInputField}
                    fullWidth
                    validate={Validators.required}
                />
            </Grid>

            {/*Street*/}
            <Grid item xs={12}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.street}
                    label="Street*"
                    fullWidth
                    validate={Validators.required}
                />
            </Grid>

            {/*City*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.city}
                    label="City*"
                    fullWidth
                    validate={Validators.required}
                />
            </Grid>

            {/*Postal code*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.postalCode}
                    label="Postal code*"
                    fullWidth
                    validate={Validators.postalCode}
                />
            </Grid>

            {/*County*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.county}
                    label="County"
                    fullWidth
                />
            </Grid>

            {/*Country*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.country}
                    label="State"
                    fullWidth
                />
            </Grid>

            {/*House number*/}
            <Grid item xs={12} sm={6}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.houseNumber}
                    label="House number"
                    fullWidth
                />
            </Grid>


            {/*Email*/}
            <Grid item xs={12}>
                <Field
                    component={TextField}
                    name={REGISTRATION_FORM_NAMES.email}
                    label="Email*"
                    fullWidth
                    validate={Validators.email}
                />
            </Grid>

            {/*Password*/}
            <Grid item xs={12}>
                <Typography variant="caption"> Password will be generated after account confirmation </Typography>
            </Grid>

            {/* Subscribe to newsletter */}
            {/*<Grid item xs={12}>
                <Field
                    component={CheckboxWithLabel}
                    type="checkbox"
                    name="checked"
                    Label={{
                        label: "I want to receive inspiration, marketing promotions and updates via email."
                    }}
                    color="primary"
                />
            </Grid>*/}

            <Grid item xs={12}>
                <div className="d-flex flex-column align-items-end">
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        disabled={isRegistrationPending}
                        className={classes.submit}>
                        {isRegistrationPending && <CircularProgress size={20} color="inherit" className="mr-2"/>}
                        Sign Up
                    </Button>

                    <Link href={LOGIN_PATH} variant="body2">
                        Already have an account? Sign in
                    </Link>
                </div>

            </Grid>

        </Grid>



    </Form>

}

export default RegistrationFormContainer
