
export const LOGIN_FORM_NAMES = {
    email: 'email',
    password: 'password'
}

export const LOGIN_FORM_INIT_VALUES = {
    email: '',
    password: ''
}
