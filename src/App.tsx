import * as React from 'react'
import {BrowserRouter as Router, Redirect, Route, Switch} from 'react-router-dom'
import ROUTE from './config/routing/routes'
import {useSelector} from 'react-redux'
import {SnackbarConsumer} from './core/components/SnackbarConsumer'
import {isAuthenticatedSelector} from './core/profile/redux/profile.reducer'
import {DialogContainer} from './components/shared/dialog/DialogContainer'
import {BANK_ACCOUNT_PATH, HOME_PAGE_PATH, LOGIN_PATH} from './config/routing/router.constants'

function App() {

    // Selectors
    const isAuthenticated = useSelector(isAuthenticatedSelector)

    return <>

        <Router>
            <div className="container-fluid p-0">

                {/*Dialog container*/}
                <DialogContainer/>

                <Switch>
                    {
                        ROUTE.map(({
                                       path,
                                       component: Component,
                                       isPrivate
                                   }) => (
                            <Route
                                key={path}
                                path={path}
                                render={props => isPrivate && !isAuthenticated ? <Redirect to={LOGIN_PATH}/> :
                                    <Component {...props}/>
                                }
                            />
                        ))
                    }

                    <Redirect to={isAuthenticated ? `${HOME_PAGE_PATH}${BANK_ACCOUNT_PATH}` : LOGIN_PATH}/>

                </Switch>
            </div>

        </Router>

        {/*SNACKABR*/}
        <React.Fragment>
            <SnackbarConsumer messageType="error" time={6000}/>
            <SnackbarConsumer messageType="info" time={4000}/>
            <SnackbarConsumer messageType="warning" time={15000}/>
            <SnackbarConsumer messageType="success" time={6000}/>
        </React.Fragment>

    </>
}

export default App
