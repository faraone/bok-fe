import React, {useMemo} from 'react'
import VisualizationTable from '../../../../shared/visualization-table/VisualizationTable'
import {WALLET_ACTIVITIES_TABLE_STRUCTURE} from '../krypto-wallets.constants'
import {useSelector} from 'react-redux'
import {currentWalletDetailSelector} from '../redux/krypto-wallets.selectors'
import {toCapitalizeString} from '../../../../../core/components/utils'


interface IWalletActivities {
}

/**
 * Wallet activities component with table
 * */
const WalletActivities: React.FC<IWalletActivities> = () => {

    const walletDetail = useSelector(currentWalletDetailSelector)

    const walletActivitiesList = useMemo(() => {
        return walletDetail?.activities ? walletDetail?.activities.map(activity => ({
            ...activity,
            amount: activity.amount?.toLocaleString(undefined, {maximumFractionDigits: 10}),
            type: toCapitalizeString(activity.type),
            status: toCapitalizeString(activity.status)
        })) : []
    }, [walletDetail?.activities])

    return <div className="row">
        <div className="col-12 mb-3">
            <VisualizationTable
                tableTitle="Wallet activities"
                results={walletActivitiesList}
                tableStructure={WALLET_ACTIVITIES_TABLE_STRUCTURE}
            />
        </div>
    </div>
}

export default WalletActivities
