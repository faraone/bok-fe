import {IDialogAction} from './dialog.reducer'

// Open dialogs action
export const OPEN_DIALOG_ACTION = 'OPEN_DIALOG_ACTION'
export const openDialogAction = (payload: IDialogAction) => ({
    type: OPEN_DIALOG_ACTION,
    payload
})

// Close dialogs action
export const CLOSE_DIALOG_ACTION = 'CLOSE_DIALOG_ACTION'
export const closeDialogAction = () => ({
    type: CLOSE_DIALOG_ACTION
})
