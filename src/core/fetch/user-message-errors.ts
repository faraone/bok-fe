import {fetchChangePasswordAction, fetchLoginAction} from '../../login/redux/login.fetch'
import {DEFAULT_ERROR_MESSAGE404} from 'fetch-with-redux-observable/dist/fetch/fetch.configs'
import {fetchDeleteWalletAction} from '../../components/dashboard/components/krypto-wallets/redux/krypto-wallets.fetch'
import {DEFAULT_ERROR_MESSAGE} from './fetch.utils'


interface IStatusErrors {
    [key: number]: string,
}

interface IHttpClientErrorMessage {
    [key: string]: IStatusErrors

}

/**
 * Custom error message of some fetch action failure
 * */
export const HTTP_ERROR_CLIENT_MESSAGE: IHttpClientErrorMessage = {
    [fetchChangePasswordAction.failureActionType]: {
        404: DEFAULT_ERROR_MESSAGE404,
        500: 'Password changed failed'
    },
    [fetchLoginAction.failureActionType]: {
        404: DEFAULT_ERROR_MESSAGE404,
        400: 'Invalid credential or account not verified',
        500: DEFAULT_ERROR_MESSAGE
    },
    [fetchDeleteWalletAction.failureActionType]: {
        404: DEFAULT_ERROR_MESSAGE404,
        400: 'Wallet deleting failed'
    }
}
