import {useCallback} from 'react'
import {openDialogAction} from '../../../shared/dialog/redux/dialog.actions'
import {DIALOG_NAMES} from '../../../shared/dialog/redux/dialog.reducer'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {useDispatch, useSelector} from 'react-redux'
import {chartWalletHistoryKryptoSelector, currentWalletDetailSelector} from './redux/krypto-wallets.selectors'
import {cardListSourceOptionsSelector} from '../card/redux/card.selectors'


export const useKryptoWalletsHook = () => {

    const dispatch = useDispatch()

    // Selector
    const selectedWalletDetail = useSelector(currentWalletDetailSelector)
    const cardListOptions = useSelector(cardListSourceOptionsSelector)
    const chartData = useSelector(chartWalletHistoryKryptoSelector)

    const openConfirmDeleteDialog = useCallback(() => {
        dispatch(openDialogAction({
            name: DIALOG_NAMES.CONFIRM_DELETE_WALLET,
            meta: {
                walletSymbol: selectedWalletDetail?.symbol || ''
            }
        }))
    }, [selectedWalletDetail?.symbol, dispatch])

    // Open transfer krypto dialog
    const openTransferKryptoDialog = useCallback(() => {
        cardListOptions?.length > 0 ? dispatch(openDialogAction({
            name: DIALOG_NAMES.TRANSFER_KRYPTO,
            meta: {}
        })) : dispatch(addWarning({userMessage: 'There isn\'t card available for the transfer. Please create one in Bank account page.'}))
    }, [cardListOptions?.length, dispatch])

    // Open create new wallet dialog
    const handleCreateNewWallet = useCallback(() => {
        dispatch(openDialogAction({
            name: DIALOG_NAMES.CREATE_WALLET,
            meta: {}
        }))
    }, [dispatch])

    return (
        {
            chartData,
            openConfirmDeleteDialog,
            openTransferKryptoDialog,
            handleCreateNewWallet,
            walletAddress: selectedWalletDetail?.address
        }
    )
}
