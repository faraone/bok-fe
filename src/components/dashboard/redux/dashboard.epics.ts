import {combineEpics, ofType} from 'redux-observable'
import {
    buyKryptoSuccessEpic,
    clearHistoricalDataOnKryptoListUpdateEpic
} from '../components/krypto-marketplace/redux/krypto-marketplace.epics'
import {makeWireTransferSuccessEpic} from '../components/bank-account/redux/bank-account.epics'
import {Observable} from 'rxjs'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {mergeMap} from 'rxjs/operators'
import {
    fetchBuyKryptoAction,
    fetchSellKyptoAction
} from '../components/krypto-marketplace/redux/krypto-marketplace.fetch'
import {closeDialogAction} from '../../shared/dialog/redux/dialog.actions'
import {
    FETCH_WALLETS_LIST_ID,
    fetchTransferKryptoAction,
    fetchWalletsListInfoAction
} from '../components/krypto-wallets/redux/krypto-wallets.fetch'
import {USER_TOKEN} from '../../../core/components/utils'
import Cookies from 'js-cookie'
import {FETCH_ALL_USER_CARDS_ID, fetchAllUserCardsAction} from '../components/card/redux/card.fetch'


/**
 * update krytpo wallet and marketplace on buy, sell and transfer fetch action success
 * */
export const updateKryptoDataEpic = (action$: Observable<ISuccessFetchAction<string>>) =>
    action$.pipe(
        ofType(fetchBuyKryptoAction.successActionType,
            fetchSellKyptoAction.successActionType,
            fetchTransferKryptoAction.successActionType),
        mergeMap(() => {

            const token = Cookies.get(USER_TOKEN)

            return [
                closeDialogAction(),
                // Fetch account card list
                fetchAllUserCardsAction.build(null, FETCH_ALL_USER_CARDS_ID, undefined, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                }),
                fetchWalletsListInfoAction.build(null, FETCH_WALLETS_LIST_ID,
                    undefined, undefined, undefined, {
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Content-Type': 'application/json'
                        }
                    })
            ]
        })
    )


export const dashboardEpics = combineEpics(
    // clear historical data
    clearHistoricalDataOnKryptoListUpdateEpic,
    // add success buy krypto
    buyKryptoSuccessEpic,
    // wire transfer
    makeWireTransferSuccessEpic,

    updateKryptoDataEpic
)
