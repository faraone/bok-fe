import {applyMiddleware, combineReducers, compose, createStore} from 'redux'
import {profileReducer} from './core/profile/redux/profile.reducer'
import {IProfile} from './core/profile/profile.types'
import {createEpicMiddleware} from 'redux-observable'
import {rootEpics} from './core/epics'
import {ICoreState} from 'fetch-with-redux-observable/dist/types'
import {CORE_REDUCER_KEY} from 'fetch-with-redux-observable/dist/constants'
import {coreCombineReducer} from 'fetch-with-redux-observable/dist/reducer'
import {drawerHomeReducer, IDrawerState} from './components/shared/drawer-home/redux/drawer-home.reducer'
import {IRedirectState, redirectPathReducer} from './core/components/redux/redirect.reducer'
import {dialogContainerReducer, IDialogState} from './components/shared/dialog/redux/dialog.reducer'
import {IBaseBankState} from './components/dashboard/components/bank-account/redux/bank-account.types'
import {bankCombineReducer} from './components/dashboard/components/bank-account/redux/bank-account.reducers'
import {IBaseWalletsState} from './components/dashboard/components/krypto-wallets/redux/krypto-wallets.types'
import {walletsCombineReducer} from './components/dashboard/components/krypto-wallets/redux/krypto-wallets.reducers'
import {IBaseMarketState} from './components/dashboard/components/krypto-marketplace/redux/krypto-marketplace.types'
import {marketCombineReducer} from './components/dashboard/components/krypto-marketplace/redux/krypto-marketplace.reducers'

/**
 * Root reducer interface
 * */
export interface IRootState {
    core: ICoreState
    profile: IProfile
    drawer: IDrawerState
    redirectPath: IRedirectState
    dialog: IDialogState | null
    bankAccount: IBaseBankState
    wallets: IBaseWalletsState
    market: IBaseMarketState
}

//@ts-ignore
const rootReducer = combineReducers<IRootState>({
    [CORE_REDUCER_KEY]: coreCombineReducer,
    profile: profileReducer,
    drawer: drawerHomeReducer,
    redirectPath: redirectPathReducer,
    dialog: dialogContainerReducer,
    bankAccount: bankCombineReducer,
    wallets: walletsCombineReducer,
    market: marketCombineReducer
})

export default function configureStore() {

    /**
     * Redux DevTools Extension
     * */
    const composeEnhancers = typeof window !== 'undefined' && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
        (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose

    /**
     * Redux Observable
     * */
    const epicsMiddleware = createEpicMiddleware()

    /**
     * return a function that instance new state. Take one argument,
     * tokenInitState, that will be set as default
     * */
    return (tokenInitState: {
        isTokenPresentAndValid: boolean,
        isPasswordChangeNeeded: boolean
    }) => {
        const store = createStore(rootReducer,
            {
                profile: {
                    isAuthenticated: tokenInitState.isTokenPresentAndValid,
                    loginInfo: null,
                    profileInfo: null,
                    passwordResetNeeded: tokenInitState.isPasswordChangeNeeded
                }
            },
            composeEnhancers(
                applyMiddleware(epicsMiddleware)
            )
        )

        epicsMiddleware.run(rootEpics)

        return store
    }
}



