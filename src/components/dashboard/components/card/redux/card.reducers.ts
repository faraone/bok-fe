import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {ICardInfo} from './card.types'
import {
    FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID,
    FETCH_GET_PLAIN_PAN_CARD_ID,
    fetchAllUserCardsAction,
    fetchDeleteCardAction,
    fetchGetCvvCardAction,
    fetchGetPlainPanCardAction
} from './card.fetch'
import {CLEAR_CARD_DETAIL_INFO, CLEAR_PLAIN_PAN_CARD} from './card.actions'
import {CARD_ID_PATH_VARIABLE} from '../../../../../core/components/utils'
import {IGenericEntities} from 'fetch-with-redux-observable/dist/types'
import {combineReducers} from 'redux'
import {ICardDialogDetailState} from '../../bank-account/redux/bank-account.types'

export const cardListReducer = (state: ICardInfo[] | null = null, action: ISuccessFetchAction<ICardInfo[]>): ICardInfo[] | null => {
    switch (action.type) {
        case fetchAllUserCardsAction.successActionType:
            return action?.payload || null
        default:
            return state
    }
}

export const cardsPlainPANReducer = (state: IGenericEntities<string> | null = null, action: ISuccessFetchAction<string>): IGenericEntities<string> | null => {
    switch (action.type) {
        case fetchGetPlainPanCardAction.successActionType:
            const requestId = action?.meta?.meta?.requestId === FETCH_GET_PLAIN_PAN_CARD_ID
            if (requestId) {
                const cardId = action?.meta?.meta?.pathVariable?.[CARD_ID_PATH_VARIABLE] as string

                const response = action?.payload + ''

                return action?.payload ? {
                    ...state,
                    [cardId]: response?.match(/.{0,4}/g)?.join(' ') || ''
                } : state
            } else
                return state
        case CLEAR_PLAIN_PAN_CARD:
            return null
        default:
            return state
    }
}

export const currentCvvCardReducer = (state: string | null = null, action: ISuccessFetchAction<string>): string | null => {
    switch (action.type) {
        case fetchGetCvvCardAction.successActionType:
            return action?.payload ? action?.payload : state
        case CLEAR_CARD_DETAIL_INFO:
            return null
        default:
            return state
    }
}

// Get payload only if request starts from dialog (with custom request id)
export const currentPanCardDialogReducer = (state: string | null = null, action: ISuccessFetchAction<string>): string | null => {
    switch (action.type) {
        case fetchGetPlainPanCardAction.successActionType:
            const requestId = action?.meta?.meta?.requestId === FETCH_GET_PLAIN_PAN_CARD_DIALOG_ID
            return requestId && action?.payload ? action?.payload : state
        case CLEAR_CARD_DETAIL_INFO:
            return null
        default:
            return state
    }
}

// Get payload only if request starts from dialog (with custom request id)
export const cardPinReducer = (state: string | null = null, action: ISuccessFetchAction<any>): string | null => {
    switch (action.type) {
        case fetchGetPlainPanCardAction.successActionType:
        case fetchGetCvvCardAction.successActionType:
        case fetchDeleteCardAction.successActionType:
            //@ts-ignore
            const pin = action?.meta?.payload?.pin as string
            return pin
        case CLEAR_CARD_DETAIL_INFO:
            return null
        default:
            return state
    }
}

export const cardDialogCombinedReducer = combineReducers<ICardDialogDetailState>({
    currentCardCVV: currentCvvCardReducer,
    currentPanCard: currentPanCardDialogReducer,
    cardPin: cardPinReducer
})
