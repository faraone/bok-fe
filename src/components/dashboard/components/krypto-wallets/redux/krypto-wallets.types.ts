import {IWallet} from '../../../dashboard.types'
import {IWalletEntities} from '../krypto-wallets.types'


export interface IBaseWalletsState {
    walletList: IWalletEntities | null
    currentWalletSymbol: string | null
}

export interface IWalletListResponse {
    wallets: IWallet[]
}

export interface IKryptoTransfersRequest {
    amount: number
    destination: string
    source: string
    symbol: string
}
