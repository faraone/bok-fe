import Cookies from 'js-cookie'
import {USER_TOKEN} from './core/components/utils'
import {FETCH_CHECK_RESET_PASSWORD_NEEDED_API, FETCH_TOKEN_INFO_API} from './core/fetch/fetch.constants'
import {urlBuilder} from 'fetch-with-redux-observable'
import moment from 'moment'

/**
 * Async function, check if USER_TOKEN Cookie is present and valid,
 * otherwise remove saved token.
 * After that, if token present, check if password reset is needed
 * @return {boolean, boolean}
 * */
export async function verifyTokenIfPresent() {
    const token = Cookies.get(USER_TOKEN)

    let isTokenPresentAndValid = false
    let isPasswordChangeNeeded = false

    if (token) {
        const url = urlBuilder(`${process.env.REACT_APP_SERVER_URL}${FETCH_TOKEN_INFO_API.url}`, {}, {})
        await fetch(url, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            method: 'POST'
        }).then((response) => {
            if (!response.ok) {
                throw Error(response.statusText)
            }
            return response.json()
        }).then((response) => {
            const {expirationDate} = response
            const isTokenValid = moment().diff(expirationDate) < 0
            if (!isTokenValid) {
                Cookies.remove(USER_TOKEN)
            } else {
                isTokenPresentAndValid = true
            }
        }).catch(() => {
            Cookies.remove(USER_TOKEN)
        })

        const urlPswRst = urlBuilder(`${process.env.REACT_APP_SERVER_URL}${FETCH_CHECK_RESET_PASSWORD_NEEDED_API.url}`, {}, {})
        await fetch(urlPswRst, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            },
            method: 'GET'
        }).then((response) => {
            if (!response.ok) {
                throw Error(response.statusText)
            }
            return response.json()
        }).then((response) => {
            isPasswordChangeNeeded = response
        }).catch(() => null)
    }

    return (
        {
            isTokenPresentAndValid,
            isPasswordChangeNeeded
        }
    )
}
