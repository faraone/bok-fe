import * as React from 'react'
import {useCallback} from 'react'
import {Button, CircularProgress, DialogContent, DialogTitle} from '@material-ui/core'
import Typography from '@material-ui/core/Typography'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'
import {
    FETCH_DELETE_WALLET_ID,
    fetchDeleteWalletAction,
    isFetchDeleteWalletPending
} from '../../krypto-wallets/redux/krypto-wallets.fetch'

interface IConfirmDeleteWalletDialog {
    walletSymbol: string
}

const ConfirmDeleteWalletDialog: React.FC<IConfirmDeleteWalletDialog> = (props) => {

    const dispatch = useDispatch()
    const {walletSymbol} = props

    const isDeleteWalletPending = useSelector(isFetchDeleteWalletPending)


    // Close dialogs callback
    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    // Submit new card
    const deleteCurrentWallet = useCallback(() => {
        const token = Cookies.get(USER_TOKEN)

        dispatch(fetchDeleteWalletAction.build({
            symbol: walletSymbol
        }, FETCH_DELETE_WALLET_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        }))
    }, [dispatch, walletSymbol])

    return (
        <>
            <DialogTitle> Confirm </DialogTitle>

            <DialogContent>

                <Typography variant="body1"> Are you sure about deleting wallet? </Typography>
                <Typography variant="caption">
                    If you delete this wallet all the contained krypto will be converted back to money and accredited to
                    you bank account
                </Typography>


                <div className="d-flex justify-content-between w-100 p-3">
                    <Button variant="outlined" color="default" onClick={closeDialog}> No </Button>
                    <Button variant="contained" color="primary" onClick={deleteCurrentWallet}
                            disabled={isDeleteWalletPending}>
                        {
                            isDeleteWalletPending &&
                            <CircularProgress color="inherit" size={20} className="mr-2"/>
                        }
                        Yes
                    </Button>
                </div>
            </DialogContent>

        </>
    )
}

export default ConfirmDeleteWalletDialog
