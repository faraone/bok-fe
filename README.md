# BOK (Bank of kaos) - FE

React project of BOK web app. User can manage their bank account and krypto wallets, buying them from marketplace.

Built with React, Redux, Formik and Typescript.

## Table of contents

- [Getting started](#Getting started)
- [Installation and Setup](#Installation and Setup)

## Getting started

This project use REST API of Java project BOK. Endpoints: fetch.constants.ts

User will be redirecting to login page

    http://localhost:3000/login

![Alt text](./src/assets/screenshots/screen-login.png?raw=true "Login page")

In this page user could click on link to the registration page

    http://localhost:3000/registration

![Alt text](./src/assets/screenshots/screen-registration.png?raw=true "Registration page")

After logging in, there are three main pages:

#### Bank account

User can manage bank account and create new card

    http://localhost:3000/home/bankAccount

#### Krypto wallet

See user krypto wallets

    http://localhost:3000/home/kryptoWallet

#### Krypto marketplace

Have a look at krypto marketplace and buy them

    http://localhost:3000/home/kryptoMarketplace

User can also change password in profile page

    http://localhost:3000/home/profile

## Installation and Setup

Package manager used: *yarn*

#### Install dependency

### `yarn install`

#### To start project

### `yarn start`

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

#### To build project

### `yarn build`

Builds the app for production to the `build` folder.\

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

