// Wallet detail type - shared
export interface IWalletBalanceHistory {
    amount: string
    instant: string
    value: string
}

export interface IDeleteWalletRequest {
    destinationIBAN: string
    symbol: string
}

export interface ILabelValueOption<T> {
    label: string,
    value: T
}

export enum EActivityType {
    PURCHASE = 'PURCHASE',
    SELL = 'SELL',
    TRANSFER = 'TRANSFER'
}

export enum EActivityStatus {
    AUTHORIZED = 'AUTHORIZED', //if bank has approved
    PENDING = 'PENDING', //at creation
    SETTLED = 'SETTLED',
    DECLINED = 'DECLINED'
}

export interface IActivity {
    accountId: number // integer($int64)
    amount: number
    publicId: string
    status: EActivityStatus
    type: EActivityType
}

export interface IWallet {
    activities: IActivity[]
    address: string
    availableAmount: string
    balanceHistory: IWalletBalanceHistory[]
    creationTimestamp: string
    symbol: string
    updateTimestamp: string
    status: string
}

export interface IMoney {
    amount: number
    currency: number
}

export interface IBankTransaction {
    amount: IMoney
    publicId: string
    status: string
    timestamp: string
    type: string
}

