import {combineReducers} from 'redux'
import {IBaseWalletsState, IWalletListResponse} from './krypto-wallets.types'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {IWallet} from '../../../dashboard.types'
import {CLEAR_CURRENT_WALLET_SYMBOL, fetchWalletInfoAction, fetchWalletsListInfoAction} from './krypto-wallets.fetch'
import {IWalletEntities} from '../krypto-wallets.types'

// change selected wallet symbol
export const CHANGE_SELECTED_WALLET_SYMBOL = 'CHANGE_SELECTE_WALLET_SYMBOL'
export const changeSelectedWalletSymbol = (symbol: string) => ({
    type: CHANGE_SELECTED_WALLET_SYMBOL,
    payload: symbol
})

export const walletListReducer = (state: IWalletEntities | null = null, action: ISuccessFetchAction<IWalletListResponse | IWallet>): IWalletEntities | null => {
    switch (action.type) {
        case fetchWalletsListInfoAction.successActionType:
            return action?.payload ? (action?.payload as IWalletListResponse)?.wallets.reduce((agg: { [key: string]: IWallet }, wallet: IWallet) =>
                ({...agg, [wallet.symbol]: wallet}), {}) : state
        case fetchWalletInfoAction.successActionType:
            return action?.payload ? {
                ...state,
                [(action?.payload as IWallet)?.symbol]: (action?.payload as IWallet)
            } : state
        default:
            return state
    }
}

export const currentWalletSymbolReducer = (state: string | null = null, action: any): string | null => {
    switch (action.type) {
        case CHANGE_SELECTED_WALLET_SYMBOL:
            return action?.payload
        // on fetch wallet list success, set first wallet symbol on redux
        case fetchWalletsListInfoAction.successActionType:
            return action?.payload?.wallets?.[0]?.symbol || null
        case CLEAR_CURRENT_WALLET_SYMBOL:
            return null
        default:
            return state
    }
}

export const walletsCombineReducer = combineReducers<IBaseWalletsState>({
    walletList: walletListReducer,
    currentWalletSymbol: currentWalletSymbolReducer
})
