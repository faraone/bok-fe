import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {combineReducers} from 'redux'
import {IBaseMarketState, IHistoricalDataResponse, IKrytoInfo} from './krypto-marketplace.types'
import {fetchKryptoMarketListAction, fetchKyptoHistorticalDataAction} from './krypto-marketplace.fetch'

export const CLEAR_HISTORIAL_DATA_ACTION = 'CLEAR_HISTORIAL_DATA_ACTION'
export const clearHistoricalDataAction = () => ({type: CLEAR_HISTORIAL_DATA_ACTION})

export const marketKryptoListReducer = (state: IKrytoInfo[] | null = null, action: ISuccessFetchAction<{ kryptos: IKrytoInfo[] }>): IKrytoInfo[] | null => {
    switch (action.type) {
        case fetchKryptoMarketListAction.successActionType:
            const response = action?.payload
            return response?.kryptos ? response?.kryptos : []
        default:
            return state
    }
}

export const currentHistoricalDataReducer = (state: IHistoricalDataResponse | null = null, action: ISuccessFetchAction<IHistoricalDataResponse>): IHistoricalDataResponse | null => {
    switch (action.type) {
        case fetchKyptoHistorticalDataAction.successActionType:
            return action?.payload ? action?.payload : null
        case CLEAR_HISTORIAL_DATA_ACTION:
            return null
        default:
            return state
    }
}

export const marketCombineReducer = combineReducers<IBaseMarketState>({
    kryptoList: marketKryptoListReducer,
    currentHistoricalData: currentHistoricalDataReducer
})
