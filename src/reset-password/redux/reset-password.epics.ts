import {NEVER, Observable} from 'rxjs'
import {IAction} from 'fetch-with-redux-observable'
import {ofType} from 'redux-observable'
import {flatMap} from 'rxjs/internal/operators'
import {addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {redirectPathAction} from '../../core/components/utils'
import {LOGIN_PATH} from '../../config/routing/router.constants'
import {fetchResetPasswordAction} from './reset-password.fetch'

export const requestResetPasswordSuccessEpic = (action$: Observable<IAction>) =>
    action$.pipe(
        ofType(fetchResetPasswordAction.successActionType),
        flatMap((response: any) => {
            return response.payload?.message ? [
                // Success snackbar
                addSuccess({userMessage: 'Password reset required. Please check your email (also spam)'}),
                // Redirect to login
                redirectPathAction(LOGIN_PATH)
            ] : NEVER
        })
    )
