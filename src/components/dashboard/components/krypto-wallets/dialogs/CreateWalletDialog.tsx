import * as React from 'react'
import {useCallback, useState} from 'react'
import {
    Button,
    CircularProgress,
    DialogActions,
    DialogContent,
    FormControl,
    InputLabel,
    MenuItem,
    Select
} from '@material-ui/core'
import {useDispatch, useSelector} from 'react-redux'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {kryptoSymbolListMarketplaceSelector} from '../../krypto-marketplace/redux/krypto-marketplace.selectors'
import {
    FETCH_CREATE_WALLET_ID,
    fetchCreateWalletAction,
    isFetchCreateWalletPending
} from '../redux/krypto-wallets.fetch'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'
import Typography from '@material-ui/core/Typography'

interface ICreateWalletDialog {
}

const CreateWalletDialog: React.FC<ICreateWalletDialog> = () => {

    const dispatch = useDispatch()

    // Selector
    const kryptoList = useSelector(kryptoSymbolListMarketplaceSelector)
    const isFetchCreatePending = useSelector(isFetchCreateWalletPending)

    // Select local state
    const [selectedSymbol, setSelectedSymbol] = useState<string>('')

    // Handle change symbol
    const handleChange = useCallback((event) => {
        setSelectedSymbol(event.target.value)
    }, [])

    const closeDialog = useCallback(() => {
        dispatch(closeDialogAction())
    }, [dispatch])

    const handleConfirm = useCallback(() => {

        const token = Cookies.get(USER_TOKEN)

        selectedSymbol !== '' ? dispatch(fetchCreateWalletAction.build({
            symbol: selectedSymbol
        }, FETCH_CREATE_WALLET_ID, undefined, undefined, undefined, {
            headers: {
                'Authorization': `Bearer ${token}`,
                'Content-Type': 'application/json'
            }
        })) : dispatch(addWarning({userMessage: 'Select krypto before confirm'}))

    }, [dispatch, selectedSymbol])


    return (
        <>
            <DialogContent>

                <Typography variant="h5" color="primary" gutterBottom> Create krypto wallet </Typography>

                {/*Select symbol*/}
                <div className="col-12 mt-2">
                    <FormControl className="w-100">
                        <InputLabel htmlFor="kryptoSymbol"> Krypto </InputLabel>

                        <Select id="kryptoSymbol"
                                value={selectedSymbol}
                                onChange={handleChange}>

                            <MenuItem key="default" value="" disabled>Select symbol</MenuItem>

                            {kryptoList.map((symbol, index: number) =>
                                // @ts-ignore
                                <MenuItem key={index} value={symbol.value}>{symbol.label}</MenuItem>)}

                        </Select>

                    </FormControl>
                </div>

            </DialogContent>

            <DialogActions>

                <div className="d-flex justify-content-between w-100 p-3">

                    <Button variant="outlined" color="default" onClick={closeDialog}> Close </Button>

                    <Button variant="contained"
                            color="primary"
                            onClick={handleConfirm}
                            disabled={isFetchCreatePending}>
                        {
                            isFetchCreatePending &&
                            <CircularProgress color="inherit" size={20} className="mr-2"/>
                        }
                        Create
                    </Button>
                </div>
            </DialogActions>

        </>
    )
}

export default CreateWalletDialog

