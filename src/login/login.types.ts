// Login request and form type
export interface ILogin {
    email: string
    password: string
}

// Login success response
export interface ILoginResponse {
    lastAccessInfo: {
        lastAccessDateTime: string
        lastAccessIP: string
    }
    passwordResetNeeded: boolean
    token: string
}

export interface ILoginInfo {
    lastAccessDateTime: string
    lastAccessIP: string
}
