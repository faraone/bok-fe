import {Observable} from 'rxjs'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {ofType} from 'redux-observable'
import {mergeMap} from 'rxjs/operators'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import {fetchWireTransferAction} from './bank-account.fetch'
import {addError, addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'

/**
 * close pin dialog after fetchWireTransferAction success
 * */
export const makeWireTransferSuccessEpic = (action$: Observable<ISuccessFetchAction<{ accepted: boolean, reason: string }>>) =>
    action$.pipe(
        ofType(fetchWireTransferAction.successActionType),
        mergeMap((action) => action?.payload?.accepted ?
            // Wire transfer accepted
            [
                addSuccess({userMessage: 'Bank transfer success'}),
                closeDialogAction()
            ] :
            // Wire transfer refused
            [
                addError({userMessage: action.payload.reason})
            ]
        ))
