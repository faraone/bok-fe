import React, {useCallback} from 'react'
import {Button, CircularProgress, SvgIcon} from '@material-ui/core'
import {useDispatch, useSelector} from 'react-redux'
import {isFetchSellKyptoPending} from '../krypto-marketplace/redux/krypto-marketplace.fetch'
import {openDialogAction} from '../../../shared/dialog/redux/dialog.actions'
import {DIALOG_NAMES} from '../../../shared/dialog/redux/dialog.reducer'
import {ReactComponent as SellIconSVG} from '../../../../assets/sell-icon.svg'
import {addWarning} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {cardListSourceOptionsSelector} from '../card/redux/card.selectors'


interface ISellKryptoButton {
    kryptoSymbol: string
}

/**
 * Shared button SELL - open sell krypto dialog
 * */
const SellKryptoButton: React.FC<ISellKryptoButton> = (props) => {

    const {kryptoSymbol} = props

    const dispatch = useDispatch()

    // Selector
    const isSellKryptoPending = useSelector(isFetchSellKyptoPending)
    const cardListOptions = useSelector(cardListSourceOptionsSelector)

    const openDialogSellKrypto = useCallback(() => {
        cardListOptions?.length > 0 ? dispatch(openDialogAction({
            name: DIALOG_NAMES.SELL_KRYPTO,
            meta: {
                kryptoSymbol
            }
        })) : dispatch(addWarning({userMessage: 'There isn\'t card available for the transfer. Please create one in Bank account page.'}))

    }, [cardListOptions, dispatch, kryptoSymbol])

    return <div className="d-flex align-items-center">
        {kryptoSymbol && <Button style={{cursor: 'pointer'}}
                                 className="mt-2"
                                 color="primary"
                                 endIcon={
                                     isSellKryptoPending ?
                                         <CircularProgress size={20} className="ml-2"
                                                           color={isSellKryptoPending ? 'primary' : 'secondary'}/>
                                         :
                                         <SvgIcon><SellIconSVG className="mr-3 c-pointer" color="primary"/></SvgIcon>}
                                 onClick={() => openDialogSellKrypto()}>Sell</Button>}
    </div>
}

export default SellKryptoButton
