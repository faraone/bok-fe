import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {FETCH_CLOSE_ACCOUNT_API, FETCH_LAST_ACCESS_INFO_API, FETCH_PROFILE_INFO_API} from '../../fetch/fetch.constants'

// Profile info fetch action
export const FETCH_PROFILE_INFO = 'FETCH_PROFILE_INFO'
export const FETCH_PROFILE_INFO_ID = 'FETCH_PROFILE_INFO_ID'

export const fetchProfileInfoAction = fetchActionFactory(FETCH_PROFILE_INFO_API, FETCH_PROFILE_INFO)
export const isFetchProfileInfoActionPending = isRequestInPending(fetchProfileInfoAction.pendingActionTypeWithSpinner, FETCH_PROFILE_INFO_ID)

// Last access info fetch action
export const FETCH_LAST_ACCESS_INFO = 'FETCH_LAST_ACCESS_INFO'
export const FETCH_LAST_ACCESS_INFO_ID = 'FETCH_LAST_ACCESS_INFO_ID'

export const fetchLastAccessInfoAction = fetchActionFactory(FETCH_LAST_ACCESS_INFO_API, FETCH_LAST_ACCESS_INFO)
export const isFetchLastAccessInfoActionPending = isRequestInPending(fetchLastAccessInfoAction.pendingActionTypeWithSpinner, FETCH_LAST_ACCESS_INFO_ID)

// Close account fetch action
export const FETCH_CLOSE_ACCOUNT = 'FETCH_CLOSE_ACCOUNT'
export const FETCH_CLOSE_ACCOUNT_ID = 'FETCH_CLOSE_ACCOUNT_ID'

export const fetchCloseAccountAction = fetchActionFactory(FETCH_CLOSE_ACCOUNT_API, FETCH_CLOSE_ACCOUNT)
export const isFetchCloseAccountActionPending = isRequestInPending(fetchCloseAccountAction.pendingActionTypeWithSpinner, FETCH_CLOSE_ACCOUNT_ID)

// Set authentication state action
export const SET_AUTH_STATE_ACTION = 'SET_AUTH_STATE_ACTION'
export const setAuthStateAction = (isAuth: boolean) => ({type: SET_AUTH_STATE_ACTION, payload: isAuth})

// Logout action
export const LOGOUT_ACTION = 'LOGOUT_ACTION'
export const logoutAction = () => ({type: LOGOUT_ACTION})

// Set passwordResetNeeded flag action
export const PASSWORD_RESET_NEEDED_ACTION = 'PASSWORD_RESET_NEEDED_ACTION'
export const setPasswordResetNeededAction = (passwordResetNeeded: boolean) => ({
    type: PASSWORD_RESET_NEEDED_ACTION,
    payload: passwordResetNeeded
})

