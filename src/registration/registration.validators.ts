/**
 * Registration validator
 * */
import {IRegistrationForm} from './registration.types'
import {REGISTRATION_FORM_NAMES} from './registration.constants'

export const registrationValidator = (registrationFormValues: IRegistrationForm) => {

    const errors: any = {}

    if (registrationFormValues?.email === '')
        errors[REGISTRATION_FORM_NAMES.email] = 'Please enter an email address'

    return errors

}
