import {ITableStructure} from '../../../shared/visualization-table/VisualizationTable'

// Wallet activities table structure
export const WALLET_ACTIVITIES_TABLE_STRUCTURE: ITableStructure[] = [
    {key: 'publicId', label: 'Public ID'},
    {key: 'amount', label: 'Amount'},
    {key: 'type', label: 'Type'},
    {key: 'status', label: 'Status'}
]

// Krypto transfer dialog constants
export const KRYPTO_TRANSFER_INIT_VALUES = {
    amount: 0,
    destination: '',
    source: '',
    symbol: ''
}

export const KRYPTO_TRANSFER_FORM_NAME = {
    amount: 'amount',
    destination: 'destination',
    source: 'source',
    symbol: 'symbol'
}

