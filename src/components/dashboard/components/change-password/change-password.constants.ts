export interface IChangePasswordForm {
    oldPassword: string
    newPassword1: string
    newPassword2: string
}

export const CHANGE_PASSWORD_INIT_VALUES = {
    oldPassword: '',
    newPassword1: '',
    newPassword2: ''
}

export const CHANGE_PASSWORD_FORM_NAMES = {
    oldPassword: 'oldPassword',
    newPassword1: 'newPassword1',
    newPassword2: 'newPassword2'
}
