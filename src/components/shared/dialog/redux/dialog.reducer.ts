import * as React from 'react'
import {IAction} from 'fetch-with-redux-observable'
import {CLOSE_DIALOG_ACTION, OPEN_DIALOG_ACTION} from './dialog.actions'
import {IRootState} from '../../../../reducer'
import {Selector} from 'reselect'
import BuyKryptoDialog from '../../../dashboard/components/bank-account/dialogs/BuyKryptoDialog'
import CreateNewCardDialog from '../../../dashboard/components/bank-account/dialogs/CreateNewCardDialog'
import ConfirmDeleteWalletDialog from '../../../dashboard/components/bank-account/dialogs/ConfirmDeleteWalletDialog'
import TransferKryptoDialog from '../../../dashboard/components/krypto-wallets/dialogs/TransferKryptoDialog'
import CreateWalletDialog from '../../../dashboard/components/krypto-wallets/dialogs/CreateWalletDialog'
import SellKryptoDialog from '../../../dashboard/components/bank-account/dialogs/SellKryptoDialog'
import CardDetailDialog from '../../../dashboard/components/bank-account/dialogs/card-detail-dialog/CardDetailDialog'
import InsertCardPinForPanDialog from '../../../dashboard/components/bank-account/dialogs/InsertCardPinForPanDialog'
import WireTransferDialog from '../../../dashboard/components/bank-account/dialogs/WireTransferDialog'
import ConfirmDeleteAccountDialog from '../../../dashboard/components/bank-account/dialogs/ConfirmDeleteAccountDialog'

// Dialog action interface
export interface IDialogAction {
    name: string,
    component?: React.FunctionComponent<any> | React.FC<any>,
    maxWidth?: 'xs' | 'sm' | 'md' | 'lg' | 'xl' | false,
    meta?: { [key: string]: string | null }
}

// Dialog state interface
export interface IDialogState extends IDialogAction {
}

export const DIALOG_NAMES: any = {
    BUY_KRYPTO: 'BUY_KRYPTO',
    NEW_CARD: 'NEW_CARD',
    CONFIRM_DELETE_WALLET: 'CONFIRM_DELETE_WALLET',
    TRANSFER_KRYPTO: 'TRANSFER_KRYPTO',
    CREATE_WALLET: 'CREATE_WALLET',
    SELL_KRYPTO: 'SELL_KRYPTO',
    CARD_DETAIL: 'CARD_DETAIL',
    INSERT_CARD_PIN_FOR_PAN: 'INSERT_CARD_PIN_FOR_PAN',
    WIRE_TRANSFER: 'WIRE_TRANSFER',
    CONFIRM_CLOSE_ACCOUNT: 'CONFIRM_CLOSE_ACCOUNT'
}

const DIALOG_CONFIG: any = {
    [DIALOG_NAMES.BUY_KRYPTO]: {
        name: DIALOG_NAMES.BUY_KRYPTO,
        component: BuyKryptoDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.NEW_CARD]: {
        name: DIALOG_NAMES.NEW_CARD,
        component: CreateNewCardDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.CONFIRM_DELETE_WALLET]: {
        name: DIALOG_NAMES.CONFIRM_DELETE_WALLET,
        component: ConfirmDeleteWalletDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.TRANSFER_KRYPTO]: {
        name: DIALOG_NAMES.TRANSFER_KRYPTO,
        component: TransferKryptoDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.CREATE_WALLET]: {
        name: DIALOG_NAMES.CREATE_WALLET,
        component: CreateWalletDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.SELL_KRYPTO]: {
        name: DIALOG_NAMES.SELL_KRYPTO,
        component: SellKryptoDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.CARD_DETAIL]: {
        name: DIALOG_NAMES.CARD_DETAIL,
        component: CardDetailDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.INSERT_CARD_PIN_FOR_PAN]: {
        name: DIALOG_NAMES.INSERT_CARD_PIN_FOR_PAN,
        component: InsertCardPinForPanDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.WIRE_TRANSFER]: {
        name: DIALOG_NAMES.WIRE_TRANSFER,
        component: WireTransferDialog,
        maxWidth: 'xs'
    },
    [DIALOG_NAMES.CONFIRM_CLOSE_ACCOUNT]: {
        name: DIALOG_NAMES.CONFIRM_CLOSE_ACCOUNT,
        component: ConfirmDeleteAccountDialog,
        maxWidth: 'xs'
    }
}


// handle changing dialogs action
export const dialogContainerReducer = (state: IDialogState | null = null, action: IAction<IDialogAction>): IDialogState | null => {
    switch (action.type) {
        case OPEN_DIALOG_ACTION:
            if (typeof action.payload === 'string') {
                return DIALOG_CONFIG[action.payload] ?
                    DIALOG_CONFIG[action.payload] : state
            }
            if (typeof action.payload === 'object') {
                // @ts-ignore
                return DIALOG_CONFIG[action.payload.name] ?
                    {...DIALOG_CONFIG[action?.payload?.name], ...action.payload} : state
            }
            return null
        case CLOSE_DIALOG_ACTION:
            return null
        default:
            return state
    }
}

// base dialogs selector
export const baseDialogStateSelector: Selector<IRootState, IDialogState | null> = (state: IRootState) => state.dialog
