import {IBankAccountInfoResponse, IBaseBankState} from './bank-account.types'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {fetchAllBankTransactionAction, fetchBankAccountInfoAction} from './bank-account.fetch'
import {combineReducers} from 'redux'
import {cardDialogCombinedReducer, cardListReducer, cardsPlainPANReducer} from '../../card/redux/card.reducers'
import {IRootState} from '../../../../../reducer'
import {IBankTransaction} from '../../../dashboard.types'


export const bankAccountInfoReducer = (state: IBankAccountInfoResponse | null = null, action: ISuccessFetchAction<IBankAccountInfoResponse>): IBankAccountInfoResponse | null => {
    switch (action.type) {
        case fetchBankAccountInfoAction.successActionType:
            return action?.payload || null
        default:
            return state
    }
}

export const bankTransactionReducer = (state: IBankTransaction[] | null = null, action: ISuccessFetchAction<IBankTransaction[]>): IBankTransaction[] | null => {
    switch (action.type) {
        case fetchAllBankTransactionAction.successActionType:
            return action?.payload || null
        default:
            return state
    }
}

export const bankCombineReducer = combineReducers<IBaseBankState>({
    accountInfo: bankAccountInfoReducer,
    cardList: cardListReducer,
    cardsCurrentPlainPAN: cardsPlainPANReducer,
    cardDialogDetail: cardDialogCombinedReducer,
    bankTransaction: bankTransactionReducer
})

// Base bank selector
export const baseBankSelector = (state: IRootState): IBaseBankState => state?.bankAccount
