import {fetchActionFactory, isRequestInPending} from 'fetch-with-redux-observable'
import {
    FETCH_BUY_KRYPTO_API,
    FETCH_KRYPTO_HISTORICAL_DATA_API,
    FETCH_KRYPTO_INFO_API,
    FETCH_KRYPTO_MARKET_INFOS_API,
    FETCH_KRYPTO_PRICE_API,
    FETCH_MARKET_KRYPTO_LIST_INFO_API,
    FETCH_MARKET_KRYPTO_PRICES_API,
    FETCH_SELL_KRYPTO_API
} from '../../../../../core/fetch/fetch.constants'

// get krypto list in market
export const FETCH_KRYPTO_MARKET_LIST_ = 'FETCH_KRYPTO_MARKET_LIST_'
export const FETCH_KRYPTO_MARKET_LIST_ID = 'FETCH_KRYPTO_MARKET_LIST_ID'

export const fetchKryptoMarketListAction = fetchActionFactory(FETCH_MARKET_KRYPTO_LIST_INFO_API, FETCH_KRYPTO_MARKET_LIST_)
export const isFetchKryptoMarketListPending = isRequestInPending(fetchKryptoMarketListAction.pendingActionTypeWithSpinner, FETCH_KRYPTO_MARKET_LIST_ID)

// get krypto price by symbol
export const FETCH_KRYPTO_PRICE_BY_SYMBOL = "FETCH_KRYPTO_PRICE_BY_SYMBOL"
export const FETCH_KRYPTO_PRICE_BY_SYMBOL_ID = "FETCH_KRYPTO_PRICE_BY_SYMBOL_ID"

export const fetchKryptoPriceBySymbolAction = fetchActionFactory(FETCH_KRYPTO_PRICE_API, FETCH_KRYPTO_PRICE_BY_SYMBOL)
export const isFetchKryptoPriceBySymbolPending = isRequestInPending(fetchKryptoPriceBySymbolAction.pendingActionTypeWithSpinner, FETCH_KRYPTO_PRICE_BY_SYMBOL_ID)

// buy krypto in market
export const FETCH_BUY_KRYPTO = "FETCH_BUY_KRYPTO"
export const FETCH_BUY_KRYPTO_ID = "FETCH_BUY_KRYPTO_ID"

export const fetchBuyKryptoAction = fetchActionFactory(FETCH_BUY_KRYPTO_API, FETCH_BUY_KRYPTO)
export const isFetchBuyKryptoPending = isRequestInPending(fetchBuyKryptoAction.pendingActionTypeWithSpinner, FETCH_BUY_KRYPTO_ID)

// get krypto historical data by symbol
export const FETCH_KRYPTO_HISTORICAL_DATA = "FETCH_KRYPTO_HISTORICAL_DATA"
export const FETCH_KRYPTO_HISTORICAL_DATA_ID = "FETCH_KRYPTO_HISTORICAL_DATA_ID"

export const fetchKyptoHistorticalDataAction = fetchActionFactory(FETCH_KRYPTO_HISTORICAL_DATA_API, FETCH_KRYPTO_HISTORICAL_DATA)
export const isFetchKyptoHistorticalDataPending = isRequestInPending(fetchKyptoHistorticalDataAction.pendingActionTypeWithSpinner, FETCH_KRYPTO_HISTORICAL_DATA_ID)

// get krypto market infos
export const FETCH_KRYPTO_MARKET_INFOS = "FETCH_KRYPTO_MARKET_INFOS"
export const FETCH_KRYPTO_MARKET_INFOS_ID = "FETCH_KRYPTO_MARKET_INFOS_ID"

export const fetchKyptoMarketInfosAction = fetchActionFactory(FETCH_KRYPTO_MARKET_INFOS_API, FETCH_KRYPTO_MARKET_INFOS)
export const isFetchKyptoMarketInfosPending = isRequestInPending(fetchKyptoMarketInfosAction.pendingActionTypeWithSpinner, FETCH_KRYPTO_MARKET_INFOS_ID)

// get krypto market prices
export const FETCH_KRYPTO_MARKET_PRICES = "FETCH_KRYPTO_MARKET_PRICES"
export const FETCH_KRYPTO_MARKET_PRICES_ID = "FETCH_KRYPTO_MARKET_PRICES_ID"

export const fetchKyptoMarketPricesAction = fetchActionFactory(FETCH_MARKET_KRYPTO_PRICES_API, FETCH_KRYPTO_MARKET_PRICES)
export const isFetchKyptoMarketIPricePending = isRequestInPending(fetchKyptoMarketPricesAction.pendingActionTypeWithSpinner, FETCH_KRYPTO_MARKET_PRICES_ID)

// sell krypto wallet
export const FETCH_SELL_KRYPTO = "FETCH_SELL_KRYPTO"
export const FETCH_SELL_KRYPTO_ID = "FETCH_SELL_KRYPTO_ID"

export const fetchSellKyptoAction = fetchActionFactory(FETCH_SELL_KRYPTO_API, FETCH_SELL_KRYPTO)
export const isFetchSellKyptoPending = isRequestInPending(fetchSellKyptoAction.pendingActionTypeWithSpinner, FETCH_SELL_KRYPTO_ID)

// get krypto info by symbol
export const FETCH_KRYPTO_INFO_BY_SYMBOL = "FETCH_KRYPTO_INFO_BY_SYMBOL"
export const FETCH_KRYPTO_INFO_BY_SYMBOL_ID = "FETCH_KRYPTO_INFO_BY_SYMBOL_ID"

export const fetchKyptoInfoBySymbolAction = fetchActionFactory(FETCH_KRYPTO_INFO_API, FETCH_KRYPTO_INFO_BY_SYMBOL)
export const isFetchKyptoInfoBySymbolPending = isRequestInPending(fetchKyptoInfoBySymbolAction.pendingActionTypeWithSpinner, FETCH_KRYPTO_INFO_BY_SYMBOL_ID)
