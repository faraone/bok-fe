import React from 'react'
import Typography from '@material-ui/core/Typography'
import AccountBalanceWalletIcon from '@material-ui/icons/AccountBalanceWallet'
import {Button} from '@material-ui/core'
import {useSelector} from 'react-redux'
import {currentWalletDetailSelector} from '../redux/krypto-wallets.selectors'
import {cardListSourceOptionsSelector} from '../../card/redux/card.selectors'
import {useKryptoWalletsHook} from '../useKryptoWalletsHook'
import BuyKryptoButton from '../../actions/BuyKryptoButton'
import SellKryptoButton from '../../actions/SellKryptoButton'

interface IWalletDetailTitle {
}

/**
 * Wallet detail title, with current krypto symbol and action available on wallet
 * */
const WalletDetailTitle: React.FC<IWalletDetailTitle> = () => {

    const {
        openTransferKryptoDialog,
        openConfirmDeleteDialog
    } = useKryptoWalletsHook()

    // Selector
    const selectedWalletDetail = useSelector(currentWalletDetailSelector)
    const cardListOptions = useSelector(cardListSourceOptionsSelector)

    return <div className="row">

        <div className="col-12 col-sm-6 mt-3">
            <Typography component="h2" variant="h6" color="primary" gutterBottom>
                <AccountBalanceWalletIcon className="mr-2"/>
                Wallet details
            </Typography>
        </div>

        {selectedWalletDetail && <div className="col-12 col-sm-6 d-flex justify-content-end flex-wrap mb-2">

            {/*TRANSFER KRYPTO BUTTON*/}
            {cardListOptions && <Button color="primary"
                                        className="mt-2"
                                        onClick={openTransferKryptoDialog}>Transfer</Button>}

            {/*DELETE WALLET BUTTON*/}
            {selectedWalletDetail && <Button color="primary"
                                             className="mt-2"
                                             onClick={openConfirmDeleteDialog}>Delete</Button>}

            {/*BUY KRYPTO*/}
            {selectedWalletDetail && <BuyKryptoButton kryptoSymbol={selectedWalletDetail?.symbol}/>}

            {/*SELL KRYPTO*/}
            {selectedWalletDetail && <SellKryptoButton kryptoSymbol={selectedWalletDetail?.symbol}/>}

        </div>}
    </div>
}

export default WalletDetailTitle
