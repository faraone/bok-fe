import React from 'react'
import {Redirect, Route, Switch} from 'react-router-dom'
import {
    BANK_ACCOUNT_PATH,
    HOME_PAGE_PATH,
    KRYPTO_MARKETPLACE_PATH,
    LOGIN_PATH,
    PROFILE_INFO_PATH,
    WALLETS_PATH
} from '../../config/routing/router.constants'
import HomeAppBar from '../../shared/HomeAppBar'
import Container from '@material-ui/core/Container'
import DrawerHome from '../shared/drawer-home/DrawerHome'
import {useSelector} from 'react-redux'
import {isAuthenticatedSelector, passwordResetNeededSelector} from '../../core/profile/redux/profile.reducer'
import {useHomeContainerHook} from './custom-hook/useHomeContainerHook'
import BankAccountContainer from '../dashboard/components/bank-account/BankAccountContainer'
import KryptoWalletsContainer from '../dashboard/components/krypto-wallets/KryptoWalletsContainer'
import KryptoMarketplaceContainer from '../dashboard/components/krypto-marketplace/KryptoMarketplaceContainer'
import ProfileInfoContainer from '../dashboard/components/profile-info/ProfileContainer'
import ChangePasswordContainer from '../dashboard/components/change-password/ChangePasswordContainer'
import {Skeleton} from '@material-ui/lab'

interface IHomeContainer {
}

const HomeContainer: React.FC<IHomeContainer> = () => {

    // Selector
    const isAuthenticated = useSelector(isAuthenticatedSelector)
    const isPasswordChangeNeeded = useSelector(passwordResetNeededSelector)

    // Call config services
    const {isRequestsPending} = useHomeContainerHook()

    return <>

        {/* Home page app bar */}
        <HomeAppBar/>

        <div className="d-flex">

            {/* Home drawer */}
            <DrawerHome/>

            <Container maxWidth="lg" className="flex-grow-1 overflow-auto">
                {isPasswordChangeNeeded ?
                    <ChangePasswordContainer/>
                    : isRequestsPending ?
                        /*Spinner skeleton*/
                        <div className="col flex-grow-1 overflow-auto mt-3">
                            <div className="row justify-content-between">
                                <Skeleton className="col-5 mr-4" height={50} variant="text"/>
                                <Skeleton className="col-5 mr-4" variant="text"/>
                                <Skeleton className="col-5 mr-4" height={50} variant="text"/>
                                <Skeleton className="col-5 mr-4" variant="text"/>
                            </div>
                        </div>
                        : <Switch>

                            {/* Bank account -> /home/bank */}
                            <Route
                                exact
                                path={`${HOME_PAGE_PATH}${BANK_ACCOUNT_PATH}`}
                                render={(props: any) => {
                                    return <BankAccountContainer {...props}/>
                                }}
                            />

                            {/* Krypto Marketplace -> /home/wallets */}
                            <Route
                                exact
                                path={`${HOME_PAGE_PATH}${WALLETS_PATH}`}
                                render={(props: any) => {
                                    return <KryptoWalletsContainer {...props}/>
                                }}
                            />

                            {/* Krypto Wallets -> /home/marketplace */}
                            <Route
                                exact
                                path={`${HOME_PAGE_PATH}${KRYPTO_MARKETPLACE_PATH}`}
                                render={(props: any) => {
                                    return <KryptoMarketplaceContainer {...props}/>
                                }}
                            />

                            {/* Profile info -> /home/profile */}
                            <Route
                                exact
                                path={`${HOME_PAGE_PATH}${PROFILE_INFO_PATH}`}
                                render={(props: any) => {
                                    return <ProfileInfoContainer {...props}/>
                                }}
                            />

                            <Redirect to={isAuthenticated ? `${HOME_PAGE_PATH}${BANK_ACCOUNT_PATH}` : LOGIN_PATH}/>

                        </Switch>}

            </Container>
        </div>
    </>
}

export default HomeContainer
