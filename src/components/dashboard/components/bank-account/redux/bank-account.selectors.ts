import {createSelector} from 'reselect'
import {IBaseBankState} from './bank-account.types'
import {baseBankSelector} from './bank-account.reducers'
import moment from 'moment'
import {DATE_TIME_CUSTOM_FORMAT, MAP_CURRENCY, toCapitalizeString} from '../../../../../core/components/utils'

export const bankAccountSelector = createSelector(baseBankSelector,
    (bankAccount: IBaseBankState) => bankAccount?.accountInfo
)

export const allBankTransactionSelector = createSelector(baseBankSelector,
    (bankAccount: IBaseBankState) => bankAccount?.bankTransaction ?
        bankAccount?.bankTransaction?.map(transaction => ({
            ...transaction,
            amount: transaction?.amount?.amount?.toLocaleString() + ' ' + MAP_CURRENCY[transaction.amount.currency],
            timestamp: moment(transaction.timestamp).format(DATE_TIME_CUSTOM_FORMAT),
            status: toCapitalizeString(transaction.status),
            type: toCapitalizeString(transaction.type.replace('_', ' '))
        })) : []
)

