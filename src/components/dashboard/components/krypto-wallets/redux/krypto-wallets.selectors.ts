import {IRootState} from '../../../../../reducer'
import {IBaseWalletsState} from './krypto-wallets.types'
import {createSelector} from 'reselect'
import {ILabelValueOption, IWallet} from '../../../dashboard.types'
import {createChartData, DATE_TIME_CUSTOM_FORMAT} from '../../../../../core/components/utils'
import moment from 'moment'
import {IWalletEntities} from '../krypto-wallets.types'

// Base wallet selector
export const baseWalletsSelector = (state: IRootState): IBaseWalletsState => state?.wallets

export const selectedWalletSymbolSelector = createSelector(baseWalletsSelector,
    (wallets: IBaseWalletsState) => wallets?.currentWalletSymbol || null
)

export const walletListSelector = createSelector(baseWalletsSelector,
    (wallets: IBaseWalletsState): IWalletEntities | null => wallets?.walletList || null
)

export const currentWalletDetailSelector = createSelector(baseWalletsSelector,
    (wallets: IBaseWalletsState) => {
        const currentSymbol = wallets?.currentWalletSymbol
        return currentSymbol ? wallets?.walletList?.[currentSymbol] : null
    }
)

// return only wallet symbol list
export const kryptoSymbolWalletListSelector = createSelector(baseWalletsSelector,
    (wallets: IBaseWalletsState): string[] => wallets?.walletList ?
        Object.keys(wallets?.walletList) : []
)

export const kryptoWalletOptionsSelector = createSelector(baseWalletsSelector,
    (wallets: IBaseWalletsState): ILabelValueOption<IWallet>[] => wallets?.walletList ?
        Object.entries(wallets?.walletList)?.map(([key, wallet]) => ({
            label: wallet.symbol + ' ' + wallet.availableAmount,
            value: wallet
        })) : []
)

export const chartWalletHistoryKryptoSelector = createSelector(currentWalletDetailSelector,
    (currentWalletSymbol: IWallet | null | undefined) => currentWalletSymbol ? currentWalletSymbol?.balanceHistory?.map(
        (historicalData) => createChartData(moment(historicalData.instant).format(DATE_TIME_CUSTOM_FORMAT), Number(historicalData.amount))) : []
)
