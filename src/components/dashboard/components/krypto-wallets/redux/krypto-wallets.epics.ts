import {combineEpics, ofType} from 'redux-observable'
import {Observable} from 'rxjs'
import {ISuccessFetchAction} from 'fetch-with-redux-observable/dist/fetch/fetch.types'
import {mergeMap} from 'rxjs/operators'
import {addError, addSuccess} from 'fetch-with-redux-observable/dist/user-message/user-message.actions'
import {
    clearCurrentWalletSymbolAction,
    FETCH_DELETE_WALLET_ID,
    FETCH_TRANSFER_KRYPTO_ID,
    FETCH_WALLETS_LIST_ID,
    fetchCreateWalletAction,
    fetchDeleteWalletAction,
    fetchTransferKryptoAction,
    fetchValidateAddressAction,
    fetchWalletsListInfoAction
} from './krypto-wallets.fetch'
import {closeDialogAction} from '../../../../shared/dialog/redux/dialog.actions'
import Cookies from 'js-cookie'
import {USER_TOKEN} from '../../../../../core/components/utils'

/**
 * On fetchDeleteWalletAction or fetchTransferKryptoAction success, update wallet list
 * */
export const deleteWalletOrTransferKryptoSuccessEpic = (action$: Observable<ISuccessFetchAction<any>>) =>
    action$.pipe(
        ofType(fetchDeleteWalletAction.successActionType,
            fetchTransferKryptoAction.successActionType,
            fetchCreateWalletAction.successActionType),
        mergeMap((action) => {

            // dispatch update list
            const token = Cookies.get(USER_TOKEN)

            const message = action?.meta?.meta?.requestId === FETCH_TRANSFER_KRYPTO_ID ?
                'Amount transferred successfully' :
                action?.meta?.meta?.requestId === FETCH_DELETE_WALLET_ID ?
                    'Wallet deleted successfully' : 'Wallet created successfully'

            return [
                fetchWalletsListInfoAction.build(null, FETCH_WALLETS_LIST_ID,
                    undefined, undefined, undefined, {
                        headers: {
                            'Authorization': `Bearer ${token}`,
                            'Content-Type': 'application/json'
                        }
                    }),
                closeDialogAction(),
                addSuccess({userMessage: message}),
                // remove selected symbol, to prevent request with incorrect data
                clearCurrentWalletSymbolAction()
            ]
        })
    )

/**
 * fetch transfer after destination wallet address check
 * */
export const validateWalletAddressSuccessEpic = (action$: Observable<ISuccessFetchAction<any>>) =>
    action$.pipe(
        ofType(fetchValidateAddressAction.successActionType),
        mergeMap((action) => {

            const payload = action?.payload

            if (payload) {

                // dispatch transfer
                const token = Cookies.get(USER_TOKEN)

                const {
                    amount,
                    source
                } = action?.meta?.meta as any

                const {
                    address,
                    symbol
                } = action?.meta?.payload as any

                return [(fetchTransferKryptoAction.build({
                    destination: address,
                    amount,
                    source,
                    symbol,
                    currencyCode: 'EUR'
                }, FETCH_TRANSFER_KRYPTO_ID, undefined, undefined, undefined, {
                    headers: {
                        'Authorization': `Bearer ${token}`,
                        'Content-Type': 'application/json'
                    }
                })),
                    addSuccess({userMessage: 'Destination wallet address found, transfer in progress'})]
            } else {
                return [addError({userMessage: 'Destination wallet address not found or with different krypto. Please enter valid data'})]
            }

        })
    )

/**
 * on fetchTransferKryptoAction success close dialog
 * */
export const walletTransferSuccessEpic = (action$: Observable<ISuccessFetchAction<any>>) =>
    action$.pipe(
        ofType(fetchTransferKryptoAction.successActionType),
        mergeMap(() => [addSuccess({userMessage: 'Successful crypto transfer'}), closeDialogAction()]))


export const kryptoWalletsEpics = combineEpics(
    deleteWalletOrTransferKryptoSuccessEpic,
    validateWalletAddressSuccessEpic,
    walletTransferSuccessEpic
)
