import * as React from 'react'
import NumberFormat, {NumberFormatProps} from 'react-number-format'
import TextField from '@material-ui/core/TextField'
import {ErrorMessage, FieldProps, useFormikContext} from 'formik'
import FormHelperText from '@material-ui/core/FormHelperText'
import FormControl from '@material-ui/core/FormControl'

interface IFormikNumberFormatProps extends FieldProps {
    numberFormatProps?: NumberFormatProps
    shouldValidateOnChange?: boolean
    formControlClassName?: string
}

const FormikNumberFormatFC: React.FC<IFormikNumberFormatProps> = props => {

    const {field, form, shouldValidateOnChange = true, numberFormatProps = {}, formControlClassName} = props
    const {errors, touched} = useFormikContext<any>()


    return (
        <FormControl className={formControlClassName}>
            <NumberFormat
                value={field.value}
                onValueChange={(values) => {
                    form.setFieldValue(field.name, values.floatValue ?? '', shouldValidateOnChange)
                }}
                customInput={TextField}
                displayType={'input'}
                decimalSeparator=","
                thousandSeparator="."
                decimalScale={2}
                fixedDecimalScale={true}
                allowNegative={false}
                error={!!errors?.[field.name] && !!touched?.[field.name]}
                {...numberFormatProps}
            />
            <ErrorMessage name={field.name}
                          render={(message) => <FormHelperText
                              error={true}>
                              {message}
                          </FormHelperText>}/>

        </FormControl>
    )
}

export const FormikNumberFormat = FormikNumberFormatFC
