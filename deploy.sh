docker stop bok-fe
docker rm bok-fe
docker build -t bok-fe .
docker run -d --name bok-fe -p 90:80 --env "VIRTUAL_HOST=bok.faraone.ovh" --env "VIRTUAL_PORT=90" --env "LETSENCRYPT_HOST=bok.faraone.ovh" --env "LETSENCRYPT_EMAIL=bok@faraone.ovh" bok-fe
